var express = require('express');
var router = express.Router();
var pool = require('./db_connection');
const jwt = require('jsonwebtoken');
var mysql = require('mysql');
var cors = require('cors');
var nodemailer = require('nodemailer');
var moment = require('moment');
var myobj = {};
//values.push({material_number});


router.get('/get/:server/:type', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    req.setTimeout(0);
    var server = req.params.server;
    var type = req.params.type;
    var username = process.env.SAP_GET_USER;
    var password = process.env.SAP_GET_PASSWORD;
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
    var request = require('request');

    var date = moment().subtract(7,'days').format('YYYYMMDD');
    var param = "&change_date="+date;

    if(type=='full'){
        param = "";
    }else if(type=='part'){
        param = "&change_date="+date;;
    }

    //GEMS
    if(server=='GEMS'){
        var url = process.env.SAP_GET_GEMS+param;
    
    }else if(server=='BC'){
        var url = process.env.SAP_GET_BC+param;
    
    }else if(server=='BKES'){
        var url = process.env.SAP_GET_BKES+param;
    }
    
    console.log(url);

    request.get( {
        url : url,
        headers : {
            "Authorization" : auth
        }
    }, function(error, response, body) {
        var values = [];
        var json = JSON.parse(body);
        for(var i=0; i< parseInt(json.length); i++){
            material_number = json[i].MATERIAL_NUMBER;
            server_code = json[i].SERVER_CODE;
            description = json[i].DESCRIPTION;
            company_id  = json[i].COMPANY_ID;
            plant = json[i].PLANT;
            base_uom = json[i].BASE_UOM;
            material_group = json[i].MATERIAL_GROUP;
            external_group = json[i].EXTERNAL_GROUP ? json[i].EXTERNAL_GROUP : "";
            company_name = json[i].COMPANY_NAME;
            storage_location_id = json[i].STORAGE_LOCATION_ID;
            
            //Initial not element
            part_no  = json[i].PART_NO ? json[i].PART_NO : "";
            inc_code = json[i].INC_CODE ? json[i].INC_CODE : "";
            inc_name = json[i].INC_NAME ? json[i].INC_NAME : "";
            basic_text = json[i].BASIC_TEXT ? json[i].BASIC_TEXT : "";
            colloquial_name = json[i].COLLOQUIAL_NAME ? json[i].COLLOQUIAL_NAME : "";
            brand = json[i].BRAND ? json[i].BRAND : "";
            

            values.push([
                        material_number,
                        server_code,
                        description,
                        company_id,
                        plant,
                        base_uom,
                        material_group,
                        external_group,
                        company_name,
                        storage_location_id,
                        part_no,
                        inc_code,
                        inc_name,
                        basic_text,
                        colloquial_name,
                        brand
                    ]);
        }

        var sql = "INSERT INTO tm_initial_temp \
                                        (\
                                            MATERIAL_NUMBER,\
                                            SERVER_CODE,\
                                            DESCRIPTION,\
                                            COMPANY_ID,\
                                            PLANT,\
                                            BASE_UOM,\
                                            MATERIAL_GROUP,\
                                            EXTERNAL_GROUP,\
                                            COMPANY_NAME,\
                                            STORAGE_LOCATION_ID,\
                                            PART_NO,\
                                            INC_CODE,\
                                            INC_NAME,\
                                            ADDITIONAL_INFORMATION,\
                                            COLLOQUIAL_NAME,\
                                            BRAND\
                                        ) VALUES ? ";

            pool.getConnection(function(err,connection){
                
                pool.query(sql,[values],function(err,rows,fields){
                    if(err){
                        console.log("Error set Initial")
                    }else{
                        console.log("Initial item successfully processed");
                    }
                });

                //connection.end();
            });
        console.log(values);
    });
});


router.get('/proses', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    req.setTimeout(0);
    //var server = req.params.server;
    //var date = req.params.date;

    var sql = "SELECT \
                (SELECT COUNT(MATERIAL_NUMBER) AS TOTAL FROM tm_initial_item \
                WHERE MATERIAL_NUMBER=a.MATERIAL_NUMBER AND SERVER_CODE=a.SERVER_CODE AND COMPANY_ID=a.COMPANY_ID \
                AND PLANT=a.PLANT) \
                AS total,a.* \
                FROM tm_initial_temp a";
   
    pool.getConnection(function(err,connection){
                
        pool.query(sql,function(err,rows,fields){
            if(err){
                console.log("Error set Initial");
                res.send(false);
            }else{
                var total_update = 0;
                var total_insert = 0;

                for(var i=0; i<rows.length; i++){
                    //console.log(rows[i].total);
                    if(rows[i].total > 0){
                        total_update++;
                        var sql_update = "UPDATE tm_initial_item SET \
                                            MATERIAL_NUMBER='"+rows[i].MATERIAL_NUMBER+"',\
                                            SERVER_CODE='"+rows[i].SERVER_CODE+"',\
                                            DESCRIPTION='"+rows[i].DESCRIPTION+"',\
                                            COMPANY_ID='"+rows[i].COMPANY_ID+"',\
                                            PLANT='"+rows[i].PLANT+"',\
                                            BASE_UOM='"+rows[i].BASE_UOM+"',\
                                            MATERIAL_GROUP='"+rows[i].MATERIAL_GROUP+"',\
                                            EXTERNAL_GROUP='"+rows[i].EXTERNAL_GROUP+"',\
                                            COMPANY_NAME='"+rows[i].COMPANY_NAME+"',\
                                            STORAGE_LOCATION_ID='"+rows[i].STORAGE_LOCATION_ID+"',\
                                            PART_NO='"+rows[i].PART_NO+"',\
                                            INC_CODE='"+rows[i].INC_CODE+"',\
                                            INC_NAME='"+rows[i].INC_NAME+"',\
                                            ADDITIONAL_INFORMATION='"+rows[i].ADDITIONAL_INFORMATION+"',\
                                            COLLAQUAL_NAME='"+rows[i].COLLOQUIAL_NAME+"',\
                                            BRAND='"+rows[i].BRAND+"' \
                                            WHERE MATERIAL_NUMBER='"+rows[i].MATERIAL_NUMBER+"' \
                                            AND SERVER_CODE='"+rows[i].SERVER_CODE+"' \
                                            AND COMPANY_ID='"+rows[i].COMPANY_ID+"' \
                                            AND PLANT='"+rows[i].PLANT+"' \
                                        ";

                        pool.query(sql_update,function(err,rows2,fields){
                            if(err){
                                console.log('Update Error');
                            }else{
                                console.log('Update Sukses');
                            }
                        });

                    }else{
                        total_insert++;
                        var sql_insert = "INSERT INTO tm_initial_item \
                                                        (\
                                                            MATERIAL_NUMBER,\
                                                            SERVER_CODE,\
                                                            DESCRIPTION,\
                                                            COMPANY_ID,\
                                                            PLANT,\
                                                            BASE_UOM,\
                                                            MATERIAL_GROUP,\
                                                            EXTERNAL_GROUP,\
                                                            COMPANY_NAME,\
                                                            STORAGE_LOCATION_ID,\
                                                            PART_NO,\
                                                            INC_CODE,\
                                                            INC_NAME,\
                                                            ADDITIONAL_INFORMATION,\
                                                            COLLAQUAL_NAME,\
                                                            BRAND\
                                                        ) VALUES \
                                                        (\
                                                            '"+rows[i].MATERIAL_NUMBER+"',\
                                                            '"+rows[i].SERVER_CODE+"',\
                                                            '"+rows[i].DESCRIPTION+"',\
                                                            '"+rows[i].COMPANY_ID+"',\
                                                            '"+rows[i].PLANT+"',\
                                                            '"+rows[i].BASE_UOM+"',\
                                                            '"+rows[i].MATERIAL_GROUP+"',\
                                                            '"+rows[i].EXTERNAL_GROUP+"',\
                                                            '"+rows[i].COMPANY_NAME+"',\
                                                            '"+rows[i].STORAGE_LOCATION_ID+"',\
                                                            '"+rows[i].PART_NO+"',\
                                                            '"+rows[i].INC_CODE+"',\
                                                            '"+rows[i].INC_NAME+"',\
                                                            '"+rows[i].ADDITIONAL_INFORMATION+"',\
                                                            '"+rows[i].COLLOQUIAL_NAME+"',\
                                                            '"+rows[i].BRAND+"'\
                                                        )";

                        pool.query(sql_insert,function(err,rows2,fields){
                            if(err){
                                console.log('Insert Error');
                            }else{
                                console.log('Insert Successfully');
                            }
                        });
                    }                   
                }

                //res.send('total insert : '+total_insert+' total update : '+total_update);
                res.send(true);
            }
        });

        connection.end();
    });
});


router.get('/clear_temp', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

        var sql = "TRUNCATE tm_initial_temp";
        pool.getConnection(function(err, connection){
            if(err){
                res.send("Error Connection");
            }else{
                pool.query(sql,function(err,rows,fields){
                    if(err){
                        res.send(false);
                    }else{
                        res.send(true);
                    }                 
                }) 
            }
            connection.end();
        })
});


router.get('/get_param/:server/:date', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    req.setTimeout(0);
    var username = process.env.SAP_GET_USER;
    var password = process.env.SAP_GET_PASSWORD;
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
    var request = require('request');

    var server = req.params.server;
    var date = req.params.date;
    date = date.replace('-','');
    var param = "&change_date="+date;


    //GEMS
    if(server=='GEMS'){
        var url = process.env.SAP_GET_GEMS+param;
    
    }else if(server=='BC'){
        var url = process.env.SAP_GET_BC+param;
    
    }else if(server=='BKES'){
        var url = process.env.SAP_GET_BKES+param;
    }
    
    //console.log(url);

    request.get( {
        url : url,
        headers : {
            "Authorization" : auth
        }
    }, function(error, response, body) {
        var values = [];
        var json = JSON.parse(body);
        for(var i=0; i< parseInt(json.length); i++){
            material_number = json[i].MATERIAL_NUMBER;
            server_code = json[i].SERVER_CODE;
            description = json[i].DESCRIPTION;
            company_id  = json[i].COMPANY_ID;
            plant = json[i].PLANT;
            base_uom = json[i].BASE_UOM;
            material_group = json[i].MATERIAL_GROUP;
            external_group = json[i].EXTERNAL_GROUP ? json[i].EXTERNAL_GROUP : "";
            company_name = json[i].COMPANY_NAME;
            storage_location_id = json[i].STORAGE_LOCATION_ID;
            
            //Initial not element
            part_no  = json[i].PART_NO ? json[i].PART_NO : "";
            inc_code = json[i].INC_CODE ? json[i].INC_CODE : "";
            inc_name = json[i].INC_NAME ? json[i].INC_NAME : "";
            basic_text = json[i].BASIC_TEXT ? json[i].BASIC_TEXT : "";
            colloquial_name = json[i].COLLOQUIAL_NAME ? json[i].COLLOQUIAL_NAME : "";
            brand = json[i].BRAND ? json[i].BRAND : "";
            

            values.push([
                        material_number,
                        server_code,
                        description,
                        company_id,
                        plant,
                        base_uom,
                        material_group,
                        external_group,
                        company_name,
                        storage_location_id,
                        part_no,
                        inc_code,
                        inc_name,
                        basic_text,
                        colloquial_name,
                        brand
                    ]);
        }

        var sql = "INSERT INTO tm_initial_temp \
                                        (\
                                            MATERIAL_NUMBER,\
                                            SERVER_CODE,\
                                            DESCRIPTION,\
                                            COMPANY_ID,\
                                            PLANT,\
                                            BASE_UOM,\
                                            MATERIAL_GROUP,\
                                            EXTERNAL_GROUP,\
                                            COMPANY_NAME,\
                                            STORAGE_LOCATION_ID,\
                                            PART_NO,\
                                            INC_CODE,\
                                            INC_NAME,\
                                            ADDITIONAL_INFORMATION,\
                                            COLLOQUIAL_NAME,\
                                            BRAND\
                                        ) VALUES ? ";

            pool.getConnection(function(err,connection){
                
                pool.query(sql,[values],function(err,rows,fields){
                    if(err){
                        res.send("false")
                    }else{
                        console.log("Initial item successfully processed");
                        res.send("true");
                    }
                });

                connection.end();
            });

            //console.log(values);
    });
});



router.use('/post', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);
    

    var username = process.env.SAP_POST_USER;
    var password = process.env.SAP_POST_PASSWORD;
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
    var request = require('request');

    var server = req.body.SERVER_CODE;

    //GEMS
    if(server=='GEMS'){
        var url = process.env.SAP_POST_GEMS;
    
    }else if(server=='BC'){
        var url = process.env.SAP_POST_BC;
    
    }else if(server=='BKES'){
        var url = process.env.SAP_POST_BKES;
    }
    
    var material_type = req.body.MATERIAL_TYPE;
    var material_number = req.body.MATERIAL_NUMBER;
    var description = req.body.DESCRIPTION;
    var base_uom = req.body.BASE_UOM;
    var material_group = req.body.MATERIAL_GROUP;
    var external_group = req.body.EXTERNAL_GROUP;
    var lab_office = req.body.LAB_OFFICE;
    var part_number = req.body.PART_NUMBER;
    var plant = req.body.PLANT;
    var inc_code = req.body.INC_CODE;
    var inc_name = req.body.INC_NAME;
    var colloquial_name = req.body.COLLOQUIAL_NAME;
    var valuation_class = req.body.VALUATION_CLASS;
    var price_control = req.body.PRICE_CONTROL;
    var additional_Info = req.body.ADDITIONAL_INFO;
    var brand = req.body.BRAND;

    request.post( {
        url : url,
        json :  {
                    "MATERIAL_TYPE":material_type,
                    "MATERIAL_NUMBER":material_number,
                    "DESCRIPTION":description,
                    "BASE_UOM":base_uom,
                    "MATERIAL_GROUP":material_group,
                    "EXT_MATERIAL_GROUP":external_group,
                    "LAB_OFFICE":lab_office,
                    "PART_NUMBER":part_number,
                    "PLANT":plant,
                    "INC_CODE":inc_code,
                    "INC_NAME":inc_name,
                    "COLLOQUIAL":colloquial_name,
                    "BRAND":brand,
                    "VALUATION_CLASS":valuation_class,
                    "PRICE_CONTROL":price_control,
                    "LONG_DESCRIPTION":additional_Info
                },
        headers : {
            "Authorization" : auth
        }
    }, function(error, response, row) {
        res.send(row);
    });
});

router.use('/post_update', async (req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin','*');
        res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
        res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
        res.setHeader('Access-Control-Allow-Credentials', true); 
    
        var request_no = req.body.REQUEST_NO;
        var material_number = req.body.MATERIAL_NUMBER;
        var item_status = req.body.ITEM_STATUS;
        var user_id = req.body.USER_INPUT;
        var name = req.body.NAME;
        var email = req.body.EMAIL;
        var inc_code = req.body.INC_CODE;
        var colloquial_name = req.body.COLLOQUIAL_NAME;

        if(req.method=='OPTIONS'){
            res.send(200);
        }else{

            var sql = "UPDATE tm_item_registration \
                        SET MATERIAL_NUMBER='"+material_number+"', \
                        REGISTER_SAP_DATE=NOW(), \
                        ITEM_STATUS='"+item_status+"'\
                        WHERE REQUEST_NO='"+request_no+"'";

            pool.getConnection(function(err,connection){
            
                pool.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Error Send to SAP");
                    }else{
                        
                        if(item_status=="REG"){

                            var sqlemail = "SELECT * FROM tm_item_registration WHERE MATERIAL_NUMBER='"+material_number+"'";
                            
                            pool.query(sqlemail,function(err,rows,fields){
                                if(err){
                                    console.log('error query mail');
                                }
                                
                                for(var i=0; i<rows.length; i++){
                                    var transporter = nodemailer.createTransport({
                                        host: process.env.MAIL_HOST,
                                        port: process.env.MAIL_PORT,
                                        secure: false,
                                        auth: {
                                            user: process.env.MAIL_USER,
                                            pass: process.env.MAIL_PASSWORD
                                        }
                                    });
                                    
                                    var mailOptions = {
                                        from: '"IMaster Sinarmas Mining" <noreply@example.com>',
                                        to: email,
                                        subject: 'IMaster Notification Success Send to SAP',
                                        html: '<div align="center" style="font-size:18px;font-weight:600;padding:20px">Notifikasi Pemberitahuan Sukses</div>\
                                                        <div style="padding:5px 20px;background-color:#ffff;margin-top:20px" align="center">\
                                                            <div style="padding:0px;margin-top:5px;background-color:#ffff">\
                                                            <table cellpadding="2" width="100%">\
                                                                <tr>\
                                                                    <td colspan="2">\
                                                                    <b>Hai '+name+'</b> \
                                                                    </td>\
                                                                </tr> \
                                                                <tr>\
                                                                    <td colspan="2">\
                                                                        Item anda berhasil dikirim ke SAP dengan Material Number <b>'+material_number+'</b>.\
                                                                    </td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td colspan="2"><hr></td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td colspan="2"><b>Detail Item</b></td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td width="150">INC Code</td>\
                                                                    <td>'+ rows[i].INC_CODE +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>INC Name</td>\
                                                                    <td>'+ rows[i].INC +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Description</td>\
                                                                    <td>'+ rows[i].DESCRIPTION +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Part Number</td>\
                                                                    <td>'+ rows[i].PART_NO +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Brand</td>\
                                                                    <td>'+ rows[i].BRAND +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Company</td>\
                                                                    <td>'+ rows[i].COMPANY_NAME +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Plant</td>\
                                                                    <td>'+ rows[i].PLANT +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Storage Location</td>\
                                                                    <td>'+ rows[i].STORAGE_LOCATION +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Material Type</td>\
                                                                    <td>'+ rows[i].MATERIAL_TYPE +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Material Group</td>\
                                                                    <td>'+ rows[i].MATERIAL_GROUP +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>External Group</td>\
                                                                    <td>'+ rows[i].EXTERNAL_GROUP +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td colspan="2" style="padding-top:15px">\
                                                                        Terimakasih atas perhatiannya,<br>\
                                                                        IMaster System - Sinarmas Mining\
                                                                    </td>\
                                                                </tr>\
                                                            </table> \
                                                        </div>'
                                    };
                                    
                                    transporter.sendMail(mailOptions, (err, info) => {
                                        if (err) throw err;
                                    });
                                }
                            });

                            //Update SQL Colloquial Name
                            var select_inc = "SELECT * FROM tm_inc WHERE INC_CODE='"+inc_code+"'";
                            pool.getConnection(function(err, connection){

                                pool.query(select_inc,function(err,rows,fields){
                                    var inc_colloquial_name = rows.COLLOQUIAL_NAME +","+ colloquial_name;
                                    
                                    if(!rows.COLLOQUIAL_NAME){
                                        inc_colloquial_name = colloquial_name;
                                    }

                                    var update_inc = "UPDATE tm_inc SET COLLOQUIAL_NAME='"+inc_colloquial_name+"' WHERE INC_CODE='"+inc_code+"'";
                                    pool.query(update_inc,function(err,rows,fields){
                                        console.log('success');   
                                    })
                                })                               
                                connection.end();
                            });

                        }

                        res.send(true);
                    }
                });
                
                connection.end();
            });
        }
});




router.get('/get_initial_full/:server', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    req.setTimeout(0);
    var server = req.params.server;
    
    var username = process.env.SAP_GET_USER;
    var password = process.env.SAP_GET_PASSWORD;
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
    var request = require('request');

    var date = moment().subtract(7,'days').format('YYYYMMDD');
    var param = ""; //"&change_date="+date;


    //GEMS
    if(server=='GEMS'){
        var url = process.env.SAP_GET_GEMS+param;
    
    }else if(server=='BC'){
        var url = process.env.SAP_GET_BC+param;
    
    }else if(server=='BKES'){
        var url = process.env.SAP_GET_BKES+param;
    }
    
    console.log(url);

    request.get( {
        url : url,
        headers : {
            "Authorization" : auth
        }
    }, function(error, response, body) {
        var values = [];
        var json = JSON.parse(body);
        for(var i=0; i< parseInt(json.length); i++){
            material_number = json[i].MATERIAL_NUMBER;
            server_code = json[i].SERVER_CODE;
            description = json[i].DESCRIPTION;
            company_id  = json[i].COMPANY_ID;
            plant = json[i].PLANT;
            base_uom = json[i].BASE_UOM;
            material_group = json[i].MATERIAL_GROUP;
            external_group = json[i].EXTERNAL_GROUP ? json[i].EXTERNAL_GROUP : "";
            company_name = json[i].COMPANY_NAME;
            storage_location_id = json[i].STORAGE_LOCATION_ID;
            
            //Initial not element
            part_no  = json[i].PART_NO ? json[i].PART_NO : "";
            inc_code = json[i].INC_CODE ? json[i].INC_CODE : "";
            inc_name = json[i].INC_NAME ? json[i].INC_NAME : "";
            basic_text = json[i].BASIC_TEXT ? json[i].BASIC_TEXT : "";
            collaqual_name = json[i].COLLOQUIAL_NAME ? json[i].COLLOQUIAL_NAME : "";
            brand = json[i].BRAND ? json[i].BRAND : "";
            

            values.push([
                        material_number,
                        server_code,
                        description,
                        company_id,
                        plant,
                        base_uom,
                        material_group,
                        external_group,
                        company_name,
                        storage_location_id,
                        part_no,
                        inc_code,
                        inc_name,
                        basic_text,
                        collaqual_name,
                        brand,
                        '1'
                    ]);
        }

        var sql = "INSERT INTO tm_initial_item \
                                        (\
                                            MATERIAL_NUMBER,\
                                            SERVER_CODE,\
                                            DESCRIPTION,\
                                            COMPANY_ID,\
                                            PLANT,\
                                            BASE_UOM,\
                                            MATERIAL_GROUP,\
                                            EXTERNAL_GROUP,\
                                            COMPANY_NAME,\
                                            STORAGE_LOCATION_ID,\
                                            PART_NO,\
                                            INC_CODE,\
                                            INC_NAME,\
                                            ADDITIONAL_INFORMATION,\
                                            COLLAQUAL_NAME,\
                                            BRAND,\
                                            STATUS\
                                        ) VALUES ? ";

            pool.getConnection(function(err,connection){
                var sql_update = "UPDATE tm_initial_item SET STATUS='2' WHERE SERVER_CODE='"+server+"'";
                var sql_delete = "DELETE FROM tm_initial_item WHERE SERVER_CODE='"+server+"' AND STATUS='2'";

                //Update Initial Process
                pool.query(sql_update,function(err,rows,fields){
                    if(err){
                        console.log("Console Log, Error Update Set Initial Item");
                        res.send("Error Update Set Initial Item");
                    
                    }else{
                        console.log("Console Log, Initial Item Successfully Update Processed");
                        
                        //Insert Initial Process
                        pool.query(sql,[values],function(err,rows,fields){
                            if(err){
                                console.log("Console Log, Error Insert Set Initial Item");
                                res.send("Error Insert Set Initial Item");

                            }else{
                                //Delete Initial Process
                                console.log("Initial item successfully Insert Processed");
                                pool.query(sql_delete,function(err,rows,fields){
                                    if(err){
                                        console.log("Console Log, Error Delete Set Initial Item");
                                        res.send("Error Delete Set Initial Item");
                                    }else{
                                        console.log("Initial item Successfully Processed Completed");
                                        res.send("Initial item Successfully Processed Completed");
                                    }
                                });
                            }
                        });
                    }
                });

                connection.end();
            });
            console.log(values);
    });
});



router.get('/get_initial/:server', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    req.setTimeout(0);
    var server = req.params.server;
    
    var username = process.env.SAP_GET_USER;
    var password = process.env.SAP_GET_PASSWORD;
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
    var request = require('request');

    var date = moment().subtract(7,'days').format('YYYYMMDD');
    var param = "&change_date="+date;


    //GEMS
    if(server=='GEMS'){
        var url = process.env.SAP_GET_GEMS+param;
    
    }else if(server=='BC'){
        var url = process.env.SAP_GET_BC+param;
    
    }else if(server=='BKES'){
        var url = process.env.SAP_GET_BKES+param;
    }
    
    console.log(url);

    request.get( {
        url : url,
        headers : {
            "Authorization" : auth
        }
    }, function(error, response, body) {
        var values = [];
        var json = JSON.parse(body);
        for(var i=0; i< parseInt(json.length); i++){
            material_number = json[i].MATERIAL_NUMBER;
            server_code = json[i].SERVER_CODE;
            description = json[i].DESCRIPTION;
            company_id  = json[i].COMPANY_ID;
            plant = json[i].PLANT;
            base_uom = json[i].BASE_UOM;
            material_group = json[i].MATERIAL_GROUP;
            external_group = json[i].EXTERNAL_GROUP ? json[i].EXTERNAL_GROUP : "";
            company_name = json[i].COMPANY_NAME;
            storage_location_id = json[i].STORAGE_LOCATION_ID;
            
            //Initial not element
            part_no  = json[i].PART_NO ? json[i].PART_NO : "";
            inc_code = json[i].INC_CODE ? json[i].INC_CODE : "";
            inc_name = json[i].INC_NAME ? json[i].INC_NAME : "";
            basic_text = json[i].BASIC_TEXT ? json[i].BASIC_TEXT : "";
            collaqual_name = json[i].COLLOQUIAL_NAME ? json[i].COLLOQUIAL_NAME : "";
            brand = json[i].BRAND ? json[i].BRAND : "";
            

            values.push([
                        material_number,
                        server_code,
                        description,
                        company_id,
                        plant,
                        base_uom,
                        material_group,
                        external_group,
                        company_name,
                        storage_location_id,
                        part_no,
                        inc_code,
                        inc_name,
                        basic_text,
                        collaqual_name,
                        brand
                    ]);
        }

        var sql = "INSERT INTO tm_initial_item \
                                        (\
                                            MATERIAL_NUMBER,\
                                            SERVER_CODE,\
                                            DESCRIPTION,\
                                            COMPANY_ID,\
                                            PLANT,\
                                            BASE_UOM,\
                                            MATERIAL_GROUP,\
                                            EXTERNAL_GROUP,\
                                            COMPANY_NAME,\
                                            STORAGE_LOCATION_ID,\
                                            PART_NO,\
                                            INC_CODE,\
                                            INC_NAME,\
                                            ADDITIONAL_INFORMATION,\
                                            COLLAQUAL_NAME,\
                                            BRAND\
                                        ) VALUES ? ";

            pool.getConnection(function(err,connection){
                
              
                pool.query(sql,[values],function(err,rows,fields){
                    if(err){
                        console.log("Error set Initial");
                        res.send(false);
                    }else{
                        console.log("Initial item successfully processed");
                        res.send(true)
                    }
                });

                connection.end();
            });
            console.log(values);
    });
});




router.get('/show_initial/:server/:date', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    req.setTimeout(0);
    var username = process.env.SAP_GET_USER;
    var password = process.env.SAP_GET_PASSWORD;
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
    var request = require('request');

    var server = req.params.server;
    var date = req.params.date;
    date = date.replace('-','');
    var param = "&change_date="+date;


    //GEMS
    if(server=='GEMS'){
        var url = process.env.SAP_GET_GEMS+param;
    
    }else if(server=='BC'){
        var url = process.env.SAP_GET_BC+param;
    
    }else if(server=='BKES'){
        var url = process.env.SAP_GET_BKES+param;
    }
    
    //console.log(url);

    request.get( { 
        url : url,
        headers : {
            "Authorization" : auth
        }
    }, function(error, response, body) {
        var values = [];
        var json = JSON.parse(body);
        for(var i=0; i< parseInt(json.length); i++){
            material_number = json[i].MATERIAL_NUMBER;
            server_code = json[i].SERVER_CODE;
            description = json[i].DESCRIPTION;
            company_id  = json[i].COMPANY_ID;
            plant = json[i].PLANT;
            base_uom = json[i].BASE_UOM;
            material_group = json[i].MATERIAL_GROUP;
            external_group = json[i].EXTERNAL_GROUP ? json[i].EXTERNAL_GROUP : "";
            company_name = json[i].COMPANY_NAME;
            storage_location_id = json[i].STORAGE_LOCATION_ID;
            
            //Initial not element
            part_no  = json[i].PART_NO ? json[i].PART_NO : "";
            inc_code = json[i].INC_CODE ? json[i].INC_CODE : "";
            inc_name = json[i].INC_NAME ? json[i].INC_NAME : "";
            basic_text = json[i].BASIC_TEXT ? json[i].BASIC_TEXT : "";
            colloquial_name = json[i].COLLOQUIAL_NAME ? json[i].COLLOQUIAL_NAME : "";
            brand = json[i].BRAND ? json[i].BRAND : "";
            

            values.push([
                        material_number,
                        server_code,
                        description,
                        company_id,
                        plant,
                        base_uom,
                        material_group,
                        external_group,
                        company_name,
                        storage_location_id,
                        part_no,
                        inc_code,
                        inc_name,
                        basic_text,
                        colloquial_name,
                        brand
                    ]);
        }

        console.log(values);
        res.send(values);
    });
});


module.exports = router;

