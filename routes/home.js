var express = require('express');
var router = express.Router();
var pool = require('./db_connection');
var mysql = require('mysql');
var cors = require('cors')
var myobj = {};

var con = mysql.createConnection({
    host : "localhost",
    user: "root",
    password : "",
    database : "db_ematerial"
})


router.get('/',function(req,res,next){
    con.connect(function(err){
        if(err) throw err;      
        res.send("Connected");
    });
});

router.get('/pool',function(req,res,next){
    pool.getConnection(function(err , connection){
        if(err){
            res.send('Error Coonection');
        }else{
            res.send('Connection Success');
        }       
    })
});

router.get('/basic_code',function(req,res,next){
    var sql = "SELECT * FROM basic_code";

    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
    })
});


router.get('/basic_code_param/:id/:val',function(req,res,next){
    var id  = req.params.id;
    var val = req.params.val;
    
    var query = "SELECT * FROM basic_code WHERE codd_flnm='"+ id + "' AND codd_valu='" + val + "'";
    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
        }else{
            connection.query(query,function(err,rows,fields){
                res.send(rows);
            })
        }
    });
});


router.post('/basic_code_post',function(req,res,next){
    var values = [];
    var codd_flnm = req.body.codd_flnm;
    var codd_valu = req.body.codd_valu;
    var codd_desc = req.body.codd_desc;
    var crtx_date = req.body.crtx_date;
    var crtx_byxx = req.body.crtx_byxx;
    
    values.push([codd_flnm,codd_valu,codd_desc,crtx_date,crtx_byxx]);
    var query = "INSERT INTO basic_code (codd_flnm,codd_valu,codd_desc,crtx_date,crtx_byxx) VALUES ?";

    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
        }else{
            connection.query(query,[values],function(err,rows,fields){
                if(err){
                    res.send("Gagal Memasukan Data");
                }else{
                    res.send("Data Berhasil Diinput");
                }
            })
        }
    })
});

router.post('/basic_multiple',function(req,res,next){
    var values = [];
    var jsondata = req.body.rows;
    for(var i=0; i<jsondata.length; i++){
        codd_flnm = jsondata[i].codd_flnm;
        codd_valu = jsondata[i].codd_valu;
        codd_desc = jsondata[i].codd_desc;
        crtx_date = jsondata[i].crtx_date;
        crtx_byxx = jsondata[i].crtx_byxx;    
        values.push([codd_flnm,codd_valu,codd_desc,crtx_date,crtx_byxx]);
    }

    var query = "INSERT INTO basic_code (codd_flnm,codd_valu,codd_desc,crtx_date,crtx_byxx) VALUES ?"; 
    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
        }else{
            connection.query(query,[values],function(err,rows,fields){
                if(err){
                    res.send("Gagal Memasukan Data");
                }else{
                    res.send("Data Berhasil Diinput");
                }
            })
        }
    })
});

router.post('/transaction',function(req,res,next){
    var values = [];
    var jsondata = req.body.rows;

    id_toko       = jsondata.id_toko;
    nama_toko     = jsondata.nama_toko;
    subtotal      = jsondata.subtotal;
    ppn           = jsondata.ppn;
    total         = jsondata.total;
    tgl_transaksi = jsondata.tgl_transaksi;
    values.push([id_toko,nama_toko,subtotal,ppn,total,tgl_transaksi]);
    
    var query = "INSERT INTO sales (id_toko,nama_toko,subtotal,ppn,total,tgl_transaksi) VALUES ?";
    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection")
        }else{
            connection.query(query,[values],function(err,rows,fields){
                if(err){
                    res.send("Gagal Memasukan Data");
                }else{
                    var detail = [];
                    var jsondetail = jsondata.detail;
                    for(var i=0; i<jsondetail.length; i++){
                        item     = jsondetail[i].item;
                        price    = jsondetail[i].price;
                        qty      = jsondetail[i].qty;
                        total    = jsondetail[i].total;
                        id_sales = rows.insertId;
                        detail.push([item,price,qty,total,id_sales]);                   
                    }
                    var queryDetail = "INSERT INTO detail_sales (item,price,qty,total,id_sales) VALUES ?";
                    connection.query(queryDetail,[detail],function(err,rows){
                        if(err){
                            res.send("Gagal Menginputkan Data");
                        }else{
                            res.send(i + " Data Berhasil Diiinputkan");
                        }
                    })
                }
            })
        }
    })
})


router.get("/Promise",function(req,res,next){
    function getPromise(){
        return new Promise(function(resolve,reject){
            resolve("Tes Promis");
        })
    }

    async function main(){
        myobj['data1'] = await getPromise();
        console.log();
    }
})



router.get('/getItem',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var WHERE   = "";
    var LIMIT   = "";
    var ORDER   = "";
    var search  = req.query.search;
    var start   = req.query.start;
    var limit   = req.query.limit;

    if(search){
        WHERE += " AND tipe LIKE '%"+ search +"%'";
    }

    if(start && limit){
        LIMIT += " LIMIT " + start + "," + limit;
    }

    ORDER = " ORDER BY stok";

    var sql = "SELECT * FROM barang WHERE id is not null" + WHERE + ORDER + LIMIT;
    pool.getConnection(function(err,connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            })
        }
    })

})


router.use('/addItem',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true); 
    
    var values = [];
    var tipe    = req.body.tipe;
    var merk    = req.body.merk;
    var spec    = req.body.spec;
    var negara  = req.body.negara;
    var harga   = req.body.harga;
    var stok    = req.body.stok;
    var logo    = "logo1.png";

    if(req.method=='OPTIONS'){
        res.send(200);
    }else{    
        values.push([tipe,merk,spec,negara,harga,stok,logo]);
        var query = "INSERT INTO barang (tipe,merk,spec,negara,harga,stok,logo) VALUES ?";

        pool.getConnection(function(err,connection){
            if(err){
                res.send("Error Connection");
            }else{
                connection.query(query,[values],function(err,rows,fields){
                    if(err){
                        res.send("false");
                    }else{
                        res.send("true");
                    }
                })
            }
        })
    }
})


router.get('/deleteItem',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true)
    
    var id = req.query.id;
    var query = 'DELETE FROM barang WHERE id="'+ id +'"';
    
    pool.getConnection(function(err,connection){
        if(err){
            res.send('Connection Error')
        }else{
            connection.query(query,function(err,rows,fields){
                if(err){
                    res.send(true);
                }else{
                    res.send(false);
                }
            })
        }
    })
})


router.get('/getData',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true)
    
    var id = req.query.id;
    var query = 'SELECT * FROM barang WHERE id="'+ id +'"';
    
    pool.getConnection(function(err,connection){
        if(err){
            res.send('Connection Error')
        }else{
            connection.query(query,function(err,rows,fields){
                if(err){
                    res.send(rows);
                }else{
                    res.send('Connection Error');
                }
            })
        }
    })
})



















router.get('/item',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);
    
    var WHERE   = "";
    var LIMIT   = "";
    var ORDER   = "";
    var search  = req.query.search;
    var start   = req.query.start ;
    var limit   = req.query.limit; 

    if(search){
        WHERE += " AND tipe like '%"+ search + "%'"; 
    }
    
    if(start && limit){
        LIMIT += " LIMIT "+start+","+limit;
    }
    
    ORDER = " ORDER BY id DESC";

    var sql   = "SELECT * FROM barang WHERE id is not null" + WHERE + ORDER + LIMIT;

    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
    })
});


router.use('/inputItem', function(req,res,next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

    var values  = [];
    var tipe    = req.body.tipe;
    var merk    = req.body.merk;
    var spec    = req.body.spec;
    var negara  = req.body.negara;
    var harga   = req.body.harga;
    var stok    = req.body.stok;

    
    if ('OPTIONS'===req.method) {
        res.send(200);

    }else{
         values.push([tipe,merk,spec,negara,harga,stok]);
        var query = "INSERT INTO barang (tipe,merk,spec,negara,harga,stok) VALUES ?";
        pool.getConnection(function(err,connection){
            if(err){
                res.send("Error Connection");
            }else{
                connection.query(query,[values],function(err,rows,fields){
                    if(err){
                        res.send("false");
                    }else{
                        res.send("true");
                    }
                })
            }
        })
    }
});


router.get('/getData2',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);
    
    var id  = req.query.id;
    var sql = "SELECT * FROM barang WHERE id='"+ id +"'";

    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows); 
            }) 
        }
    })
})





module.exports = router;

