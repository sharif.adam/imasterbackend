var express = require('express');
var router = express.Router();
var pool = require('./db_connection');
const jwt = require('jsonwebtoken');
var mysql = require('mysql');
var cors = require('cors')
var myobj = {};
var nodemailer = require('nodemailer');
const fileUpload = require('express-fileupload');

router.get('/mailApprove',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true)

    var transporter = nodemailer.createTransport({
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        secure: false,
        auth: {
            user: process.env.MAIL_USER,
            pass: process.env.MAIL_PASSWORD
        }
    });
    
    var mailOptions = {
        from: '"IMaster Sinarmas Mining" <noreply@example.com>',
        to: 'syarief.hidayatullah@edi-indonesia.co.id',
        subject: 'Sending Email using Nodejs',
        html: '<div align="center" style="font-size:15px;font-weight:600">Notifikasi Pemberitahuan Approve</div>\
                <div>Tes</div>\
               '
    };
    
    transporter.sendMail(mailOptions, (err, info) => {
        if (err) throw err;
        console.log('Email sent: ' + info.response);
    });
});

router.use('/mail_approval', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

        var departement  = req.body.departement;
        var role = req.body.role;

        var sql = "SELECT a.*,b.* FROM tx_departement_role a \
                    LEFT JOIN tm_user b ON a.USER_ID=b.USER_ID \
                    WHERE a.STATUS='Y' and a.DEPARTEMENT_ID='"+ departement +"' LIMIT 1";
        
        pool.getConnection(function(err, connection){
            if(err){
                
                res.send("Error Connection");
            }else{
                pool.query(sql,function(err,rows,fields){
                    res.send(rows);
                }) 
            }
            connection.end();
        })
});


router.use('/mail_approval2', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

        var server = req.body.server;

        var sql = "SELECT a.*,b.* FROM tx_approval_server a \
                    LEFT JOIN tm_user b ON a.APPROVAL2=b.USER_ID \
                    WHERE a.STATUS='Y' and a.SERVER='"+ server +"' LIMIT 1";
        
        pool.getConnection(function(err, connection){
            if(err){
                
                res.send("Error Connection");
            }else{
                pool.query(sql,function(err,rows,fields){
                    res.send(rows);
                }) 
            }
            connection.end();
        })
});


router.use('/send_message',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var message = req.body.message;
    var request_no = req.body.request_no;
    var from = req.body.from;
    var receiver = req.body.receiver;
    var type = req.body.type;
    
    var query = "INSERT INTO tx_message (MESSAGE,REQUEST_NO,SENDER,RECEIVER,MESSAGE_DATE,TYPE) \
                VALUES ('"+message+"','"+request_no+"','"+from+"','"+receiver+"',NOW(),'"+type+"')";

    if(req.method=='OPTIONS'){
        res.send(200);
    }else{
        pool.getConnection(function(err,connection){
            if(err){
                res.send("Error Connection");
            }else{
                pool.query(query,function(err,rows,fields){
                    if(err){
                        res.send("Error send message");
                    }else{

                            //---------------------------------------------------------------------------------
                            var sqlemail = "SELECT *, \
                                            (SELECT CONCAT(a.NAME,' | ',b.ROLE_NAME) FROM tm_user a \
                                            LEFT JOIN tm_role b ON a.ROLE_ID=b.ROLE_ID WHERE USER_ID='"+from+"') AS SENDER, \
                                            (SELECT EMAIL FROM tm_user WHERE USER_ID='"+receiver+"' ) as EMAIL, \
                                            (SELECT NAME FROM tm_user WHERE USER_ID='"+receiver+"' ) as RECEIVER_NAME \
                                            FROM tm_item_registration WHERE REQUEST_NO='"+request_no+"'";

                            pool.query(sqlemail,function(err,rows,fields){
                                if(err){
                                    console.log(sqlemail);
                                }
                                
                                for(var i=0; i<rows.length; i++){
                                    email   = rows[i].EMAIL;
                                    from    = rows[i].SENDER;
                                    receiver_name = rows[i].RECEIVER_NAME;
                                    
                                    var transporter = nodemailer.createTransport({
                                        host: process.env.MAIL_HOST,
                                        port: process.env.MAIL_PORT,
                                        secure: false,
                                        auth: {
                                            user: process.env.MAIL_USER,
                                            pass: process.env.MAIL_PASSWORD
                                        }
                                    });

                                    var mailOptions = {
                                        from: '"IMaster Sinarmas Mining" <noreply@example.com>',
                                        to: email,
                                        subject: 'Notes Item Registration IMaster',
                                        html: '<div align="center" style="font-size:18px;font-weight:600;padding:20px">Notifikasi Pemberitahuan Notes</div>\
                                                        <div style="padding:5px 20px;background-color:#ffff;margin-top:5px" align="center">\
                                                            <div style="padding:20px;background-color:#ffff">\
                                                            <table cellpadding="2" width="100%">\
                                                            <tr>\
                                                                <td colspan="2">\
                                                                <b>Hi '+receiver_name+'</b> \
                                                                </td>\
                                                            </tr> \
                                                            <tr>\
                                                                <td colspan="2">\
                                                                    Anda memiliki notes dari '+ from +'. \
                                                                </td>\
                                                            </tr>\
                                                            <tr>\
                                                                <td colspan="2"><b>Notes</b> : <br>'+ message +'</td>\
                                                            </tr>\
                                                            <tr>\
                                                                <td colspan="2"><hr></td>\
                                                            </tr>\
                                                            <tr>\
                                                                <td colspan="2"><b>Detail Item</b></td>\
                                                            </tr>\
                                                            <tr>\
                                                                <td width="150">INC Code</td>\
                                                                <td>'+ rows[i].INC_CODE +'</td>\
                                                            </tr>\
                                                            <tr>\
                                                                <td>INC Name</td>\
                                                                <td>'+ rows[i].INC +'</td>\
                                                            </tr>\
                                                            <tr>\
                                                                <td>Description</td>\
                                                                <td>'+ rows[i].DESCRIPTION +'</td>\
                                                            </tr>\
                                                            <tr>\
                                                                <td>Company</td>\
                                                                <td>'+ rows[i].COMPANY_NAME +'</td>\
                                                            </tr>\
                                                            <tr>\
                                                                <td>Material Type</td>\
                                                                <td>'+ rows[i].MATERIAL_TYPE +'</td>\
                                                            </tr>\
                                                        </tr>\
                                                        <tr>\
                                                            <td colspan="2" style="padding-top:15px">\
                                                                Buka aplikasi IMaster anda untuk membalas notes tersebut.<br><br>\
                                                                Terimakasih atas perhatiannya,<br>\
                                                                IMaster System - Sinarmas Mining\
                                                            </td>\
                                                        </tr>\
                                                            </table> \
                                                            </div>\
                                                            '
                                    };                          
                                    transporter.sendMail(mailOptions, (err, info) => {
                                        if (err) throw err;
                                    });
                                }
                                                    
                            });
                        //---------------------------------------------------------------------------------

                        res.send("true");
                    }
                })
            }
            connection.end();
        })
    }
});


router.use('/list_message',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var request_no = req.body.request_no;
    var type = req.body.type;
    
    var query = "SELECT a.*,DATE_FORMAT(a.MESSAGE_DATE,'%d-%m-%Y %H:%i:%s') as MESSAGE_DATE,b.NAME, \
                b.ROLE_ID,c.DEPARTEMENT_DESC,d.ROLE_NAME,e.NAME as USER_RECEIVER,e.ROLE_ID as ROLE_RECEIVER \
                from tx_message a \
                LEFT JOIN tm_user b ON a.SENDER=b.USER_ID \
                LEFT JOIN tm_departement c ON b.DEPARTEMENT_ID=c.DEPARTEMENT_ID \
                LEFT JOIN tm_role d ON b.ROLE_ID=d.ROLE_ID \
                LEFT JOIN tm_user e ON e.USER_ID=a.RECEIVER \
                where a.REQUEST_NO='"+request_no+"' and a.TYPE='"+type+"' order by a.MESSAGE_DATE ASC";

    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
        }else{
            pool.query(query,function(err,rows,fields){
                if(err){
                    res.send("Error list message");
                }else{
                    res.send(rows);
                }
            })
        }
        connection.end();
    })
});


router.use('/getApprovalMessage',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server = req.body.server;
    var departement = req.body.departement;
    
    var query = "SELECT a.USER_ID,CONCAT(b.NAME,' - ',b.ROLE_ID) as USER_NAME FROM tx_departement_role a \
                 LEFT JOIN tm_user b ON a.USER_ID=b.USER_ID WHERE a.DEPARTEMENT_ID='"+departement+"' \
                 UNION \
                 SELECT a.APPROVAL2 as USER_ID,CONCAT(b.NAME,' - ',b.ROLE_ID) as USER_NAME FROM tx_approval_server a \
                 LEFT JOIN tm_user b ON a.APPROVAL2=b.USER_ID WHERE a.SERVER='"+server+"'";

    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
        }else{
            pool.query(query,function(err,rows,fields){
                if(err){
                    res.send("Error approval message");
                }else{
                    res.send(rows);
                }
            })
        }
        connection.end();
    })
});

router.use('/getCataloguerMessage',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server = req.body.server;
    
    var query = "SELECT a.CATALOGUER,b.ROLE_ID,b.NAME,b.USER_ID FROM tx_approval_server a \
                 LEFT JOIN tm_user b ON a.CATALOGUER=b.USER_ID WHERE a.SERVER='"+server+"' LIMIT 1";

    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
        }else{
            pool.query(query,function(err,rows,fields){
                if(err){
                    res.send("Error approval message");
                }else{
                    res.send(rows);
                }
            })
        }
        connection.end();
    })
});



router.use('/update_password',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var password = req.body.password;
    var new_password = req.body.new_password;
    var user_id = req.body.user_id;

    var query = "UPDATE tm_user SET PASSWORD=MD5('"+new_password+"') WHERE USER_ID='"+user_id+"'";
    var checkQuery = "SELECT * FROM tm_user WHERE PASSWORD=MD5('"+password+"') and USER_ID='"+user_id+"'";

    if(req.method=='OPTIONS'){
        res.send(200);
    }else{
        pool.getConnection(function(err,connection){
            if(err){
                res.send("Error Connection");
            }else{
                pool.query(checkQuery,function(err,rows,fields){
                    if(err){
                        res.send("Error Check User");
                    }else{
                        if(rows==''){
                            res.send("Incorect password");
                        
                        }else{

                            pool.query(query,function(err,rows,fields){
                                if(err){
                                    res.send("Error update password");
                                }else{
                                    res.send(true)
                                }
                            });

                        }
                    }
                });
                
            }
            connection.end();
        })
    }
});

router.use(fileUpload());
router.use('/upload',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var file = req.files.file;
    var filename = req.body.filename;

    file.mv('./public/dir_upload/'+filename, function(err) {
        if (err){
            res.send("gagal mengupload data");
        }

        res.send(true);
    });
});

module.exports = router;

