var express = require('express');
var router = express.Router();
var pool = require('./db_connection');
const jwt = require('jsonwebtoken');
var mysql = require('mysql');
var cors = require('cors')
var myobj = {};


router.post('/getUser', verifyToken ,async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    try {
        jwt.verify(req.token, 'SuperSecRetKey', (err, authData)=>{

            if(err){  
                res.sendStatus(403);
            }else{
    
                var sql = "SELECT * FROM tm_user";
                pool.getConnection(function(err, connection){
                    if(err){
                        connection.release();
                        res.send("Error Connection");
                    }else{
                        connection.query(sql,function(err,rows,fields){
                            res.send(rows);
                        }) 
                    }
                })
            }
        });
    } catch (error) {
        res.status(500).send(error)
    }
});


/** verifyToken method - this method verifies token */
function verifyToken(req, res, next){
    
    const bearerHeader = req.headers['authorization'];
    
    if(typeof bearerHeader !== 'undefined'){

        const bearer = bearerHeader.split(' ');
        
        const bearerToken = bearer[1];

        req.token = bearerToken;

        next();

    }else{

        res.sendStatus(403);

    }
}


router.post('/api/getToken', (req, res) => {
    const user = {
        id: 1,
        username: "johndoe",
        email: "john.doe@test.com"
    }
    jwt.sign({user},'SuperSecRetKey', { expiresIn: 60 * 60 }, (err, token) => {
        res.json({token});
    });
});

function jsonShow(req, res, next){
    var tes = req;
    return tes;
}

router.post('/api/posts', verifyToken, (req, res) => {
    jwt.verify(req.token, 'SuperSecRetKey', (err, authData)=>{
        if(err){
            res.sendStatus(403);
        }else{
            var tes = jsonShow('123');
            res.send(tes);
        }
    });
});
module.exports = router;

