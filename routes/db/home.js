var express = require('express');
var router = express.Router();
var pool = require('./db_connection');
var mysql = require('mysql');

var con = mysql.createConnection({
    host : "localhost",
    user: "root",
    password : "",
    database : "db_stars"
})


router.get('/',function(req,res,next){
    con.connect(function(err){
        if(err) throw err;      
        res.send("Connected");
    });
});

router.get('/pool',function(req,res,next){
    pool.getConnection(function(err , connection){
        if(err){
            res.send('Error Coonection');
        }else{
            res.send('Connection Success');
        }       
    })
});

router.get('/basic_code',function(req,res,next){
    var sql = "SELECT * FROM basic_code";

    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
    })
});


router.get('/basic_code_param/:id/:val',function(req,res,next){
    var id    = req.params.id;
    var val   = req.params.val;
    var query = "SELECT * FROM basic_code WHERE codd_flnm='"+ id +"' AND codd_valu='"+ val +"'";

    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
            return
        }else{
            connection.query(query,function(err,rows,fields){
                res.send(rows);
            })
        }
    })
})


router.post('/basic_code_post',function(request,res){
    var values = [];
    var codd_flnm = request.body.codd_flnm;
    var codd_valu = request.body.codd_valu;
    var codd_desc = request.body.codd_desc;
    var crtx_date = request.body.crtx_date;
    var crtx_byxx = request.body.crtx_byxx;

    values.push([codd_flnm,codd_valu,codd_desc,crtx_date,crtx_byxx]);
    var query = "INSERT INTO basic_code (codd_flnm,codd_valu,codd_desc,crtx_date,crtx_byxx) VALUES ?";

    pool.getConnection(function(err,connection){
        if(err){
            res.send("Koneksi ke Database Gagal");
        }else{
            connection.query(query,[values],function(err,rows,fields){
                if(err){
                    res.send('Gagal Memasukan Data');
                }else{
                    res.send('Data Berhasil Dimasukan');
                }
            })
        }
    })
})


module.exports = router;

