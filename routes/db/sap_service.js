var express = require('express');
var router = express.Router();
var pool = require('./db_connection');
const jwt = require('jsonwebtoken');
var mysql = require('mysql');
var cors = require('cors');
var nodemailer = require('nodemailer');
var myobj = {};
//values.push({material_number});

router.get('/get/:server', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server = req.params.server;
    var username = "SAP_ISUP";
    var password = "123456";
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
    var request = require('request');

    var date = "";

    //GEMS
    if(server=='GEMS'){
        var url = "http://sapwdp.sinarmasmining.com:8009/sap/bc/zse_mm_imat_g?sap-client=100";
    
    }else if(server=='BC'){
        var url = "http://sapwdp.sinarmasmining.com:8010/sap/bc/zse_mm_imat_g?sap-client=100";
    
    }else if(server=='BKES'){
        var url = "http://sapwdp.sinarmasmining.com:8011/sap/bc/zse_mm_imat_g?sap-client=200";
    }
    
    request.get( {
        url : url,
        headers : {
            "Authorization" : auth
        }
    }, function(error, response, body) {
        var values = [];
        var json = JSON.parse(body);
        for(var i=0; i< parseInt(json.length); i++){
            material_number = json[i].MATERIAL_NUMBER;
            server_code = json[i].SERVER_CODE;
            description = json[i].DESCRIPTION;
            company_id  = json[i].COMPANY_ID;
            plant = json[i].PLANT;
            base_uom = json[i].BASE_UOM;
            material_group = json[i].MATERIAL_GROUP;
            external_group = json[i].EXTERNAL_GROUP ? json[i].EXTERNAL_GROUP : "";
            company_name = json[i].COMPANY_NAME;
            storage_location_id = json[i].STORAGE_LOCATION_ID;
            
            //Initial not element
            part_no  = json[i].PART_NO ? json[i].PART_NO : "";
            inc_code = json[i].INC_CODE ? json[i].INC_CODE : "";
            inc_name = json[i].INC_NAME ? json[i].INC_NAME : "";
            values.push([
                        material_number,
                        server_code,
                        description,
                        company_id,
                        plant,
                        base_uom,
                        material_group,
                        external_group,
                        company_name,
                        storage_location_id,
                        part_no,
                        inc_code,
                        inc_name
                    ]);
        }

        var sql = "INSERT INTO tm_initial_temp \
                                        (\
                                            MATERIAL_NUMBER,\
                                            SERVER_CODE,\
                                            DESCRIPTION,\
                                            COMPANY_ID,\
                                            PLANT,\
                                            BASE_UOM,\
                                            MATERIAL_GROUP,\
                                            EXTERNAL_GROUP,\
                                            COMPANY_NAME,\
                                            STORAGE_LOCATION_ID,\
                                            PART_NO,\
                                            INC_CODE,\
                                            INC_NAME\
                                        ) VALUES ? ";

            pool.getConnection(function(err,connection){
                
                pool.query(sql,[values],function(err,rows,fields){
                    if(err){
                        console.log("Error set Initial")
                    }else{
                        console.log("Initial item successfully processed");
                    }
                });

                //connection.end();
            });
        console.log(values);
    });
});


router.get('/proses', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    //var server = req.params.server;
    //var date = req.params.date;

    var sql = "SELECT * FROM tm_initial_temp";
   
    pool.getConnection(function(err,connection){
                
        pool.query(sql,function(err,rows,fields){
            if(err){
                console.log("Error set Initial")
            }else{
                let a = 0;
                for(var i=0; i<rows.length; i++){
                    a++;
                    var sql_item = "SELECT COUNT(MATERIAL_NUMBER) as TOTAL FROM tm_initial_item WHERE \
                                    SERVER_CODE='"+rows[i].SERVER_CODE+"' AND \
                                    MATERIAL_NUMBER='"+rows[i].MATERIAL_NUMBER+"' AND \
                                    PLANT='"+rows[i].PLANT+"' AND \
                                    STORAGE_LOCATION_ID='"+rows[i].STORAGE_LOCATION_ID+"' AND\
                                    COMPANY_ID='"+rows[i].COMPANY_ID+"'\
                                    ORDER BY MATERIAL_NUMBER ASC";
                        
                        pool.query(sql_item,function(err,rows2,fields){
                            console.log(a)
                            if(err){
                                console.log('Error');
                            }else{
                                if(rows2[0].TOTAL > 0){
                                    // var sql_insert = "INSERT INTO tm_initial_temp \
                                    //                     (\
                                    //                         MATERIAL_NUMBER,\
                                    //                         SERVER_CODE,\
                                    //                         DESCRIPTION,\
                                    //                         COMPANY_ID,\
                                    //                         PLANT,\
                                    //                         BASE_UOM,\
                                    //                         MATERIAL_GROUP,\
                                    //                         EXTERNAL_GROUP,\
                                    //                         COMPANY_NAME,\
                                    //                         STORAGE_LOCATION_ID,\
                                    //                         PART_NO,\
                                    //                         INC_CODE,\
                                    //                         INC_NAME\
                                    //                     ) VALUES \
                                    //                     (\
                                    //                         '"+rows[i].MATERIAL_NUMBER+"',\
                                    //                         '"+rows[i].SERVER_CODE+"',\
                                    //                         '"+rows[i].DESCRIPTION+"',\
                                    //                         '"+rows[i].COMPANY_ID+"',\
                                    //                         '"+rows[i].PLANT+"',\
                                    //                         '"+rows[i].BASE_UOM+"',\
                                    //                         '"+rows[i].MATERIAL_GROUP+"',\
                                    //                         '"+rows[i].EXTERNAL_GROUP+"',\
                                    //                         '"+rows[i].COMPANY_NAME+"',\
                                    //                         '"+rows[i].STORAGE_LOCATION_ID+"',\
                                    //                         '"+rows[i].PART_NO+"',\
                                    //                         '"+rows[i].INC_CODE+"',\
                                    //                         '"+rows[i].INC_NAME+"'\
                                    //                     )";
                                    
                                    // pool.query(sql_insert,function(err,rows,fields){
                                    //     if(err){
                                    //         console.log('Error Update');    
                                    //     }
                                    //     console.log('Success Insert');                                 
                                        
                                    // });
                                    console.log('tes');
                                }else{
                                //     var sql_insert = "INSERT INTO tm_initial_temp \
                                //                         (\
                                //                             MATERIAL_NUMBER,\
                                //                             SERVER_CODE,\
                                //                             DESCRIPTION,\
                                //                             COMPANY_ID,\
                                //                             PLANT,\
                                //                             BASE_UOM,\
                                //                             MATERIAL_GROUP,\
                                //                             EXTERNAL_GROUP,\
                                //                             COMPANY_NAME,\
                                //                             STORAGE_LOCATION_ID,\
                                //                             PART_NO,\
                                //                             INC_CODE,\
                                //                             INC_NAME\
                                //                         ) VALUES \
                                //                         (\
                                //                             '"+rows[i].MATERIAL_NUMBER+"',\
                                //                             '"+rows[i].SERVER_CODE+"',\
                                //                             '"+rows[i].DESCRIPTION+"',\
                                //                             '"+rows[i].COMPANY_ID+"',\
                                //                             '"+rows[i].PLANT+"',\
                                //                             '"+rows[i].BASE_UOM+"',\
                                //                             '"+rows[i].MATERIAL_GROUP+"',\
                                //                             '"+rows[i].EXTERNAL_GROUP+"',\
                                //                             '"+rows[i].COMPANY_NAME+"',\
                                //                             '"+rows[i].STORAGE_LOCATION_ID+"',\
                                //                             '"+rows[i].PART_NO+"',\
                                //                             '"+rows[i].INC_CODE+"',\
                                //                             '"+rows[i].INC_NAME+"'\
                                //                         )";
                                    
                                //     pool.query(sql_insert,function(err,rows,fields){
                                //         if(err){
                                //             console.log('Error Update');    
                                //         }
                                //         console.log('Success Insert');                                 
                                        
                                //     });
                                }
                            }
                         });
                    
                }
            }
        });

        connection.end();
    });
    
});



router.use('/post', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);
    

    var username = "SAP_ISUP";
    var password = "123456";
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
    var request = require('request');

    var server = req.body.SERVER_CODE;

    //GEMS
    if(server=='GEMS'){
        var url = "http://sapwdp.sinarmasmining.com:8009/sap/bc/zse_mm_imat_p?sap-client=100";
    
    }else if(server=='BC'){
        var url = "http://sapwdp.sinarmasmining.com:8010/sap/bc/zse_mm_imat_p?sap-client=100";
    
    }else if(server=='BKES'){
        var url = "http://sapwdp.sinarmasmining.com:8011/sap/bc/zse_mm_imat_p?sap-client=100";
        //var url = "http://sapwdp.sinarmasmining.com:8009/sap/bc/zse_mm_imat_p?sap-client=100";
    }
    
    var material_type = req.body.MATERIAL_TYPE;
    var material_number = req.body.MATERIAL_NUMBER;
    var description = req.body.DESCRIPTION;
    var base_uom = req.body.BASE_UOM;
    var material_group = req.body.MATERIAL_GROUP;
    var external_group = req.body.EXTERNAL_GROUP;
    var lab_office = req.body.LAB_OFFICE;
    var part_number = req.body.PART_NUMBER;
    var plant = req.body.PLANT;
    var inc_code = req.body.INC_CODE;
    var inc_name = req.body.INC_NAME;
    var colloquial_name = req.body.COLLOQUIAL_NAME;
    var valuation_class = req.body.VALUATION_CLASS;
    var price_control = req.body.PRICE_CONTROL;
    var additional_Info = req.body.ADDITIONAL_INFO;
    var brand = req.body.BRAND;

    request.post( {
        url : url,
        json :  {
                    "MATERIAL_TYPE":material_type,
                    "MATERIAL_NUMBER":material_number,
                    "DESCRIPTION":description,
                    "BASE_UOM":base_uom,
                    "MATERIAL_GROUP":material_group,
                    "EXT_MATERIAL_GROUP":external_group,
                    "LAB_OFFICE":lab_office,
                    "PART_NUMBER":part_number,
                    "PLANT":plant,
                    "INC_CODE":inc_code,
                    "INC_NAME":inc_name,
                    "COLLOQUIAL":colloquial_name,
                    "BRAND":brand,
                    "VALUATION_CLASS":valuation_class,
                    "PRICE_CONTROL":price_control,
                    "LONG_DESCRIPTION":additional_Info
                },
        headers : {
            "Authorization" : auth
        }
    }, function(error, response, row) {
        res.send(row);
    });
});

router.use('/post_update', async (req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin','*');
        res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
        res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
        res.setHeader('Access-Control-Allow-Credentials', true); 
    
        var request_no = req.body.REQUEST_NO;
        var material_number = req.body.MATERIAL_NUMBER;
        var item_status = req.body.ITEM_STATUS;
        var user_id = req.body.USER_INPUT;
        var name = req.body.NAME;
        var email = req.body.EMAIL;
        
        if(req.method=='OPTIONS'){
            res.send(200);
        }else{

            var sql = "UPDATE tm_item_registration \
                        SET MATERIAL_NUMBER='"+material_number+"', \
                        REGISTER_SAP_DATE=NOW(), \
                        ITEM_STATUS='"+item_status+"'\
                        WHERE REQUEST_NO='"+request_no+"'";

            pool.getConnection(function(err,connection){
            
                pool.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Error Send to SAP");
                    }else{
                        
                        if(item_status=="REG"){

                            var sqlemail = "SELECT * FROM tm_item_registration WHERE MATERIAL_NUMBER='"+material_number+"'";
                            
                            pool.query(sqlemail,function(err,rows,fields){
                                if(err){
                                    console.log('error query mail');
                                }
                                
                                for(var i=0; i<rows.length; i++){
                                    var transporter = nodemailer.createTransport({
                                        service: 'gmail',
                                        auth: {
                                            user: 'imasterdevelope@gmail.com',
                                            pass: 'pass123ABC'
                                        }
                                    });
                                    
                                    var mailOptions = {
                                        from: '"IMaster Sinarmas Mining" <noreply@example.com>',
                                        to: email,
                                        subject: 'IMaster Notification Success Send to SAP',
                                        html: '<div align="center" style="font-size:18px;font-weight:600;padding:20px">Notifikasi Pemberitahuan Sukses</div>\
                                                        <div style="padding:5px 20px;background-color:#ffff;margin-top:20px" align="center">\
                                                            <div style="padding:0px;margin-top:5px;background-color:#ffff">\
                                                            <table cellpadding="2" width="100%">\
                                                                <tr>\
                                                                    <td colspan="2">\
                                                                    <b>Hai '+name+'</b> \
                                                                    </td>\
                                                                </tr> \
                                                                <tr>\
                                                                    <td colspan="2">\
                                                                        Item anda berhasil dikirim ke SAP dengan Material Number <b>'+material_number+'</b>.\
                                                                    </td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td colspan="2"><hr></td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td colspan="2"><b>Detail Item</b></td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td width="150">INC Code</td>\
                                                                    <td>'+ rows[i].INC_CODE +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>INC Name</td>\
                                                                    <td>'+ rows[i].INC +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Description</td>\
                                                                    <td>'+ rows[i].DESCRIPTION +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Part Number</td>\
                                                                    <td>'+ rows[i].PART_NO +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Brand</td>\
                                                                    <td>'+ rows[i].BRAND +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Company</td>\
                                                                    <td>'+ rows[i].COMPANY_NAME +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Plant</td>\
                                                                    <td>'+ rows[i].PLANT +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Storage Location</td>\
                                                                    <td>'+ rows[i].STORAGE_LOCATION +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Material Type</td>\
                                                                    <td>'+ rows[i].MATERIAL_TYPE +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Material Group</td>\
                                                                    <td>'+ rows[i].MATERIAL_GROUP +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>External Group</td>\
                                                                    <td>'+ rows[i].EXTERNAL_GROUP +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td colspan="2" style="padding-top:15px">\
                                                                        Terimakasih atas perhatiannya,<br>\
                                                                        IMaster System - Sinarmas Mining\
                                                                    </td>\
                                                                </tr>\
                                                            </table> \
                                                        </div>'
                                    };
                                    
                                    transporter.sendMail(mailOptions, (err, info) => {
                                        if (err) throw err;
                                    });
                                }
                            });
                        }

                        res.send(true);
                    }
                });
                
                connection.end();
            });
        }
});


module.exports = router;

