var mysql =require('mysql');
var pool = mysql.createPool({
    connectionLimit:999999999999999999999,
    queueLimit: 1000,
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'db_ematerial',
    multipleStatements : true,
    waitForConnections: true    
});

module.exports = pool;

