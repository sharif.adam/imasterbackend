var express = require('express');
var router = express.Router();
var pool = require('./db_connection');
var mysql = require('mysql');
var cors = require('cors');
var myobj = {};
var nodemailer = require('nodemailer');
var btoa = require('btoa');
var moment = require('moment');


//mail_vps   = process.env.EMAIL_TOKEN;
//mail_local = "http://localhost:8080/#/Apps/";

//mail_server = "http://172.20.22.70:8082/#/Apps/";

router.use('/add',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true); 
    
    var values = [];
    //var request_no = "000002";
    var request_by = req.body.request_by;
    var request_date = req.body.request_date;
    var inc_code = req.body.inc_code;
    var inc = req.body.inc;
    var description = req.body.description;
    var additional_information = req.body.additional_information;
    var colloquial_name = req.body.colloquial_name;
    var brand = req.body.brand;
    var part_no = req.body.part_no;
    var base_uom = req.body.base_uom;
    var material_group = req.body.material_group;
    var external_group = req.body.external_group;
    var company_id = req.body.company_id;
    var company_name = req.body.company_name;
    var plant = req.body.plant;
    var storage_location = req.body.storage_location;
    var average_usage = req.body.average_usage;
    var type_stock = req.body.type_stock;
    var safety_stock = req.body.safety_stock;
    var criticallity = req.body.criticallity;
    var server_code = req.body.server_code;
    var item_status = req.body.item_status;
    var type_input = req.body.type_input;
    var message = req.body.message;
    var user_id = req.body.user_id;
    var material_type = req.body.material_type;
    var json_inc_attribute = req.body.json_inc_attribute;
    var attachment = req.body.attachment;

    var departement_id = req.body.departement_id;
    var request_code =  moment().format("DDMMYYHHmmss");

    var submit_date = 'NOW()';
    if(item_status=='APP1' || item_status=='CAT'){
        submit_date = 'NOW()';
    }

    //email element
    var email_approval = req.body.email_approval;
    var user_approval = req.body.user_approval;
    var kode_token = Math.floor(Math.random() * 100000);
    var token = btoa(user_id+'&'+departement_id+'&'+request_by+'&'+user_approval+'&'+email_approval+'&'+kode_token+'&'+item_status);
    var url_token = process.env.EMAIL_TOKEN+token;

    if(req.method=='OPTIONS'){
        res.send(200);
    }else{    
        values.push([
                        request_by,
                        request_date,
                        submit_date,
                        inc_code,
                        inc,
                        description,
                        additional_information,
                        colloquial_name,
                        brand,
                        part_no,
                        base_uom,
                        material_group,
                        external_group,
                        company_id,
                        company_name,
                        plant,
                        storage_location,
                        average_usage,
                        type_stock,
                        safety_stock,
                        criticallity,
                        server_code,
                        type_input,
                        item_status,
                        message,
                        user_id,
                        material_type,
                        departement_id,
                        request_code,
                        'NOW()'
                    ]);
        
                    var query = "INSERT INTO tm_item_registration (\
                                    REQUEST_BY, \
                                    REQUEST_DATE, \
                                    SUBMIT_DATE, \
                                    INC_CODE, \
                                    INC, \
                                    DESCRIPTION, \
                                    ADDITIONAL_INFORMATION, \
                                    COLLOQUIAL_NAME, \
                                    BRAND, \
                                    PART_NO, \
                                    BASE_UOM, \
                                    MATERIAL_GROUP, \
                                    EXTERNAL_GROUP, \
                                    COMPANY_ID, \
                                    COMPANY_NAME, \
                                    PLANT, \
                                    STORAGE_LOCATION, \
                                    AVERAGE_USAGE, \
                                    TYPE_STOCK, \
                                    SAFETY_STOCK, \
                                    CRITICALLITY, \
                                    SERVER_CODE, \
                                    TYPE_INPUT, \
                                    ITEM_STATUS, \
                                    MESSAGE, \
                                    USER_INPUT,\
                                    MATERIAL_TYPE,\
                                    DEPARTEMENT_ID,\
                                    REQUEST_CODE,\
                                    INC_ATTRIBUTE,\
                                    ATTACHMENT,\
                                    UPDATE_DATE) VALUES \
                                    (\
                                        '"+request_by+ "',\
                                        '"+request_date+"',\
                                        "+submit_date+",\
                                        '"+inc_code+"',\
                                        '"+inc+"',\
                                        '"+description+"',\
                                        '"+additional_information+"',\
                                        '"+colloquial_name+"',\
                                        '"+brand+"',\
                                        '"+part_no+"',\
                                        '"+base_uom+"',\
                                        '"+material_group+"',\
                                        '"+external_group+"',\
                                        '"+company_id+"',\
                                        '"+company_name+"',\
                                        '"+plant+"',\
                                        '"+storage_location+"',\
                                        '"+average_usage+"',\
                                        '"+type_stock+"',\
                                        '"+safety_stock+"',\
                                        '"+criticallity+"',\
                                        '"+server_code+"',\
                                        '"+type_input+"',\
                                        '"+item_status+"',\
                                        '"+message+"',\
                                        '"+user_id+"',\
                                        '"+material_type+"',\
                                        '"+departement_id+"',\
                                        '"+request_code+"',\
                                        '"+json_inc_attribute+"',\
                                        '"+attachment+"',\
                                        NOW()\
                                    )";
        
        
        if(type_input=='NEW'){
            checkInitial = "select PART_NO from tm_initial_item where PART_NO='"+part_no+"'";
            checkItemReg = "select PART_NO from tm_item_registration where PART_NO='"+part_no+"'";
        }else{
            checkInitial = "select PART_NO from tm_initial_item where SERVER_CODE='"+server_code+"' and PART_NO='"+part_no+"'";
            checkItemReg = "select PART_NO from tm_item_registration where SERVER_CODE='"+server_code+"' and PART_NO='"+part_no+"'";
        }              
        
        pool.getConnection(function(err,connection){
            pool.query(checkInitial,function(err,rows,fields){
                if(err){
                    res.send("error initial search item");
                }else{
                    
                    //pengecekan di initial item
                    if(rows==''){
                        pool.query(checkItemReg,function(err,rows,fields){
                            if(err){
                                res.send("error search item registration");
                            }else{
                                
                                //pengecekan di item registration
                                if(rows==''){
                                    pool.query(query,function(err,rows,fields){
                                        if(err){
                                            res.send(err);
                                        }else{
                                            if(item_status=='APP1'){
                                                var transporter = nodemailer.createTransport({
                                                    host: process.env.MAIL_HOST,
                                                    port: process.env.MAIL_PORT,
                                                    secure: false,
                                                    auth: {
                                                        user: process.env.MAIL_USER,
                                                        pass: process.env.MAIL_PASSWORD
                                                    }
                                                });
                                                
                                                var mailOptions = {
                                                    from: '"IMaster Sinarmas Mining" <noreply@example.com>',
                                                    to: email_approval,
                                                    subject: 'Need Approval IMaster',
                                                    html: '<div align="center" style="font-size:18px;font-weight:600;padding:20px">Notifikasi Pemberitahuan Approve</div>\
                                                                    <div style="padding:5px 20px;background-color:#ffff;margin-top:20px" align="center">\
                                                                        <div style="padding:0px;margin-top:5px;background-color:#ffff">\
                                                                        <table cellpadding="2" width="100%">\
                                                                            <tr>\
                                                                                <td colspan="2">\
                                                                                <b></b> \
                                                                                </td>\
                                                                            </tr> \
                                                                            <tr>\
                                                                                <td colspan="2"><a href="'+url_token+'">[Klik Disini]</a> Untuk melakukan approval dan melihat dari request tersebut.\
                                                                                Anda memiliki permohonan persetujuan untuk Create Material Master Request. \
                                                                            </td>\
                                                                            </tr>\
                                                                           <tr>\
                                                                             <td width="120">Requester</td>\
                                                                             <td>: <b>'+request_by+'</b></td>\
                                                                           </tr> \
                                                                           <tr>\
                                                                             <td width="120">Departement</td>\
                                                                             <td>: <b>'+departement_id+'</b></td>\
                                                                           </tr> \
                                                                           <tr>\
                                                                                <td colspan="2" style="padding-top:15px">\
                                                                                    <a href="'+url_token+'">[Klik Disini]</a> untuk melakukan approval dan juga melihat detail dari request tersebut. \
                                                                                </td>\
                                                                            </tr>\
                                                                            <tr>\
                                                                                <td colspan="2" style="padding-top:15px">\
                                                                                    Terimakasih atas perhatiannya,<br>\
                                                                                    IMaster System - Sinarmas Mining\
                                                                                </td>\
                                                                            </tr>\
                                                                        </table> \
                                                                        </div>\
                                                                        '
                                                };
                                                
                                                transporter.sendMail(mailOptions, (err, info) => {
                                                    if (err) throw err;
                                                });
                                            }
                                            res.send("true");
                                        }
                                    });    
                                }else{
                                    res.send("This item has been registered in Registration List");
                                }                           
                            }
                        })

                    }else{
                        res.send("This item has been registered in SAP");
                    }                   
                }
            })
            connection.end();
        })
    }
});



router.use('/add_new',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true); 
    
    var values = [];
    //var request_no = "000002";
    var request_by = req.body.request_by;
    var request_date = req.body.request_date;
    var inc_code = req.body.inc_code;
    var inc = req.body.inc;
    var description = req.body.description;
    var additional_information = req.body.additional_information;
    var colloquial_name = req.body.colloquial_name;
    var brand = req.body.brand;
    var part_no = req.body.part_no;
    var base_uom = req.body.base_uom;
    var material_group = req.body.material_group;
    var external_group = req.body.external_group;
    var company_id = req.body.company_id;
    var company_name = req.body.company_name;
    var plant = req.body.plant;
    var storage_location = req.body.storage_location;
    var average_usage = req.body.average_usage;
    var type_stock = req.body.type_stock;
    var safety_stock = req.body.safety_stock;
    var criticallity = req.body.criticallity;
    var server_code = req.body.server_code;
    var item_status = req.body.item_status;
    var type_input = req.body.type_input;
    var message = req.body.message;
    var user_id = req.body.user_id;
    var material_type = req.body.material_type;
    var json_inc_attribute = req.body.json_inc_attribute;
    var requester_description = req.body.requester_description;
    var requester_additional_info = req.body.requester_additional_info;
    var requester_json_inc = req.body.requester_json_inc;
    var attachment = req.body.attachment;

    var departement_id = req.body.departement_id;
    var request_code = moment().format("DDMMYYHHmmss");

    var submit_date = 'NOW()';
    if(item_status=='APP1' || item_status=='CAT'){
        submit_date = 'NOW()';
    }

    //email element
    var email_approval = req.body.email_approval;
    var user_approval = req.body.user_approval;
    var kode_token = Math.floor(Math.random() * 100000);
    var token = btoa(user_id+'&'+departement_id+'&'+request_by+'&'+user_approval+'&'+email_approval+'&'+kode_token+'&'+item_status);
    var url_token = process.env.EMAIL_TOKEN+token;

    if(req.method=='OPTIONS'){
        res.send(200);
    }else{    
        values.push([
                        request_by,
                        request_date,
                        inc_code,
                        inc,
                        description,
                        additional_information,
                        colloquial_name,
                        brand,
                        part_no,
                        base_uom,
                        material_group,
                        external_group,
                        company_id,
                        company_name,
                        plant,
                        storage_location,
                        average_usage,
                        type_stock,
                        safety_stock,
                        criticallity,
                        server_code,
                        type_input,
                        item_status,
                        message,
                        user_id,
                        material_type,
                        departement_id,
                        request_code,
                        'NOW()'
                    ]);
        
                    var query = "INSERT INTO tm_item_registration (\
                                    REQUEST_BY, \
                                    REQUEST_DATE, \
                                    SUBMIT_DATE, \
                                    INC_CODE, \
                                    INC, \
                                    DESCRIPTION, \
                                    ADDITIONAL_INFORMATION, \
                                    COLLOQUIAL_NAME, \
                                    BRAND, \
                                    PART_NO, \
                                    BASE_UOM, \
                                    MATERIAL_GROUP, \
                                    EXTERNAL_GROUP, \
                                    COMPANY_ID, \
                                    COMPANY_NAME, \
                                    PLANT, \
                                    STORAGE_LOCATION, \
                                    AVERAGE_USAGE, \
                                    TYPE_STOCK, \
                                    SAFETY_STOCK, \
                                    CRITICALLITY, \
                                    SERVER_CODE, \
                                    TYPE_INPUT, \
                                    ITEM_STATUS, \
                                    MESSAGE, \
                                    USER_INPUT,\
                                    MATERIAL_TYPE,\
                                    DEPARTEMENT_ID,\
                                    REQUEST_CODE,\
                                    INC_ATTRIBUTE,\
                                    REQUESTER_DESCRIPTION,\
                                    REQUESTER_ADDITIONAL_INFO,\
                                    REQUESTER_JSON_INC,\
                                    ATTACHMENT,\
                                    UPDATE_DATE) VALUES \
                                    (\
                                        '"+request_by+ "',\
                                        '"+request_date+"',\
                                        "+submit_date+",\
                                        '"+inc_code+"',\
                                        '"+inc+"',\
                                        '"+description+"',\
                                        '"+additional_information+"',\
                                        '"+colloquial_name+"',\
                                        '"+brand+"',\
                                        '"+part_no+"',\
                                        '"+base_uom+"',\
                                        '"+material_group+"',\
                                        '"+external_group+"',\
                                        '"+company_id+"',\
                                        '"+company_name+"',\
                                        '"+plant+"',\
                                        '"+storage_location+"',\
                                        '"+average_usage+"',\
                                        '"+type_stock+"',\
                                        '"+safety_stock+"',\
                                        '"+criticallity+"',\
                                        '"+server_code+"',\
                                        '"+type_input+"',\
                                        '"+item_status+"',\
                                        '"+message+"',\
                                        '"+user_id+"',\
                                        '"+material_type+"',\
                                        '"+departement_id+"',\
                                        '"+request_code+"',\
                                        '"+json_inc_attribute+"',\
                                        '"+requester_description+"',\
                                        '"+requester_additional_info+"',\
                                        '"+requester_json_inc+"',\
                                        '"+attachment+"',\
                                        NOW()\
                                    )";              
        
        pool.getConnection(function(err,connection){
    
            pool.query(query,function(err,rows,fields){
                if(err){
                    res.send(err);
                }else{
                    if(item_status=='APP1'){
                        var transporter = nodemailer.createTransport({
                            host: process.env.MAIL_HOST,
                            port: process.env.MAIL_PORT,
                            secure: false,
                            auth: {
                                user: process.env.MAIL_USER,
                                pass: process.env.MAIL_PASSWORD
                            }
                        });
                        
                        var mailOptions = {
                            from: '"IMaster Sinarmas Mining" <noreply@example.com>',
                            to: email_approval,
                            subject: 'Need Approval IMaster',
                            html: '<div align="center" style="font-size:18px;font-weight:600;padding:20px">Notifikasi Pemberitahuan Approve</div>\
                                            <div style="padding:5px 20px;background-color:#ffff;margin-top:20px" align="center">\
                                                <div style="padding:0px;margin-top:5px;background-color:#ffff">\
                                                <table cellpadding="2" width="100%">\
                                                    <tr>\
                                                        <td colspan="2">\
                                                        <b></b> \
                                                        </td>\
                                                    </tr> \
                                                    <tr>\
                                                        <td colspan="2"><a href="'+url_token+'">[Klik Disini]</a> Untuk melakukan approval dan melihat dari request tersebut.\
                                                        Anda memiliki permohonan persetujuan untuk Create Material Master Request. \
                                                    </td>\
                                                    </tr>\
                                                    <tr>\
                                                        <td width="120">Requester</td>\
                                                        <td>: <b>'+request_by+'</b></td>\
                                                    </tr> \
                                                    <tr>\
                                                        <td width="120">Departement</td>\
                                                        <td>: <b>'+departement_id+'</b></td>\
                                                    </tr> \
                                                    <tr>\
                                                        <td colspan="2" style="padding-top:15px">\
                                                            <a href="'+url_token+'">[Klik Disini]</a> untuk melakukan approval dan juga melihat detail dari request tersebut. \
                                                        </td>\
                                                    </tr>\
                                                    <tr>\
                                                        <td colspan="2" style="padding-top:15px">\
                                                            Terimakasih atas perhatiannya,<br>\
                                                            IMaster System - Sinarmas Mining\
                                                        </td>\
                                                    </tr>\
                                                </table> \
                                                </div>\
                                                '
                        };
                        
                        transporter.sendMail(mailOptions, (err, info) => {
                            if (err) throw err;
                        });
                    }
                    res.send("true");
                }
            });                            
            connection.end();
        })
    }
});




router.use('/update',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true); 

    var request_no = req.body.request_no;
    var inc_code = req.body.inc_code;
    var inc = req.body.inc;
    var description = req.body.description;
    var additional_information = req.body.additional_information;
    var colloquial_name = req.body.colloquial_name;
    var brand = req.body.brand;
    var part_no = req.body.part_no;
    var base_uom = req.body.base_uom;
    var material_group = req.body.material_group;
    var external_group = req.body.external_group;
    var company_id = req.body.company_id;
    var company_name = req.body.company_name;
    var plant = req.body.plant;
    var storage_location = req.body.storage_location;
    var average_usage = req.body.average_usage;
    var safety_stock = req.body.safety_stock;
    var criticallity = req.body.criticallity;
    var server_code = req.body.server_code;
    var item_status = req.body.item_status;
    var message = req.body.message;
    var type_input = req.body.type_input;
    var material_type = req.body.material_type;
    var departement_id = req.body.departement_id;
    var json_inc_attribute = req.body.json_inc_attribute;
    var requester_description = req.body.requester_description;
    var requester_additional_info = req.body.requester_additional_info;
    var requester_json_inc = req.body.requester_json_inc;
    var type_stock = req.body.type_stock;
    var attachment = req.body.attachment;

    var user_id = req.body.user_id;
    var request_by = req.body.request_by;

    //email element
    var email_approval = req.body.email_approval;
    var user_approval = req.body.user_approval;
    var kode_token = Math.floor(Math.random() * 100000);
    var token = btoa(user_id+'&'+departement_id+'&'+request_by+'&'+user_approval+'&'+email_approval+'&'+kode_token+'&'+item_status);
    var url_token = process.env.EMAIL_TOKEN+token;


    if(req.method=='OPTIONS'){
        res.send(200);
    }else{ 
        var query = "UPDATE tm_item_registration SET \
                        INC_CODE = '"+ inc_code +"', \
                        INC='"+ inc +"', \
                        DESCRIPTION='"+ description +"', \
                        ADDITIONAL_INFORMATION='"+ additional_information +"', \
                        COLLOQUIAL_NAME='"+ colloquial_name +"', \
                        BRAND='"+ brand +"', \
                        BASE_UOM='"+ base_uom +"', \
                        TYPE_STOCK='"+type_stock +"', \
                        MATERIAL_GROUP='"+ material_group +"', \
                        EXTERNAL_GROUP='"+ external_group +"', \
                        COMPANY_ID='"+ company_id +"', \
                        COMPANY_NAME='"+ company_name +"', \
                        PLANT='"+ plant +"', \
                        STORAGE_LOCATION='"+ storage_location +"', \
                        AVERAGE_USAGE='"+ average_usage +"', \
                        SAFETY_STOCK='"+ safety_stock +"', \
                        CRITICALLITY='"+ criticallity +"', \
                        SERVER_CODE='"+ server_code +"', \
                        MESSAGE='"+ message +"', \
                        ITEM_STATUS='"+ item_status +"', \
                        MATERIAL_TYPE='"+ material_type +"', \
                        PART_NO='"+ part_no +"', \
                        INC_ATTRIBUTE='"+ json_inc_attribute +"', \
                        REQUESTER_DESCRIPTION='"+ requester_description +"', \
                        REQUESTER_ADDITIONAL_INFO='"+ requester_additional_info +"', \
                        REQUESTER_JSON_INC='"+ requester_json_inc +"', \
                        ATTACHMENT='"+ attachment +"', \
                        UPDATE_DATE=NOW(), \
                        SUBMIT_DATE=NOW() \
                        WHERE REQUEST_NO='"+ request_no +"'";

            pool.getConnection(function(err,connection){
                if(err){
                    res.send("Error Connection");
                }else{
                    pool.query(query,function(err,rows,fields){
                        if(err){
                            res.send("error");
                        }else{
                            if(item_status=='APP1'){
                                    var transporter = nodemailer.createTransport({
                                        host: process.env.MAIL_HOST,
                                        port: process.env.MAIL_PORT,
                                        secure: false,
                                        auth: {
                                            user: process.env.MAIL_USER,
                                            pass: process.env.MAIL_PASSWORD
                                        }
                                    });
                                    
                                    var mailOptions = {
                                        from: '"IMaster Sinarmas Mining" <noreply@example.com>',
                                        to: email_approval,
                                        subject: 'Need Approval IMaster',
                                        html: '<div align="center" style="font-size:18px;font-weight:600;padding:20px">Notifikasi Pemberitahuan Approve</div>\
                                                        <div style="padding:5px 20px;background-color:#ffff;margin-top:5px" align="center">\
                                                            <div style="padding:20px;background-color:#ffff">\
                                                            <table cellpadding="2" width="100%">\
                                                                <tr>\
                                                                    <td colspan="2">\
                                                                    <b></b> \
                                                                    </td>\
                                                                </tr> \
                                                                <tr>\
                                                                    <td colspan="2"><a href="'+url_token+'">[Klik Disini]</a> Untuk melakukan approval dan melihat dari request tersebut.\
                                                                    Anda memiliki permohonan persetujuan untuk Create Material Master Request. \
                                                                </td>\
                                                                </tr>\
                                                               <tr>\
                                                                 <td width="120">Requester</td>\
                                                                 <td>: <b>'+request_by+'</b></td>\
                                                               </tr> \
                                                               <tr>\
                                                                 <td width="120">Departement</td>\
                                                                 <td>: <b>'+departement_id+'</b></td>\
                                                               </tr> \
                                                               <tr>\
                                                                    <td colspan="2" style="padding-top:15px">\
                                                                        <a href="'+url_token+'">[Klik Disini]</a> untuk melakukan approval dan juga melihat detail dari request tersebut. \
                                                                    </td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td colspan="2" style="padding-top:15px">\
                                                                        Terimakasih atas perhatiannya,<br>\
                                                                        IMaster System - Sinarmas Mining\
                                                                    </td>\
                                                                </tr>\
                                                            </table> \
                                                            </div>\
                                                            '
                                    };
                                    
                                    transporter.sendMail(mailOptions, (err, info) => {
                                        if (err) throw err;
                                    });
                                }

                            res.send("true");
                        }
                    })
                }
                connection.end();
            });
            
        }
});



router.use('/update_new',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true); 

    var request_no = req.body.request_no;
    var inc_code = req.body.inc_code;
    var inc = req.body.inc;
    var description = req.body.description;
    var additional_information = req.body.additional_information;
    var colloquial_name = req.body.colloquial_name;
    var brand = req.body.brand;
    var part_no = req.body.part_no;
    var base_uom = req.body.base_uom;
    var material_group = req.body.material_group;
    var external_group = req.body.external_group;
    var company_id = req.body.company_id;
    var company_name = req.body.company_name;
    var plant = req.body.plant;
    var type_stock = req.body.type_stock;
    var storage_location = req.body.storage_location;
    var average_usage = req.body.average_usage;
    var safety_stock = req.body.safety_stock;
    var criticallity = req.body.criticallity;
    var server_code = req.body.server_code;
    var item_status = req.body.item_status;
    var message = req.body.message;
    var type_input = req.body.type_input;
    var material_type = req.body.material_type;
    var departement_id = req.body.departement_id;
    var json_inc_attribute = req.body.json_inc_attribute;
    var requester_description = req.body.requester_description;
    var requester_additional_info = req.body.requester_additional_info;
    var requester_json_inc = req.body.requester_json_inc;
    var attachment = req.body.attachment;
    var part_no_save = req.body.part_no_save;

    var user_id = req.body.user_id;
    var request_by = req.body.request_by;

    //email element
    var email_approval = req.body.email_approval;
    var user_approval = req.body.user_approval;
    var kode_token = Math.floor(Math.random() * 100000);
    var token = btoa(user_id+'&'+departement_id+'&'+request_by+'&'+user_approval+'&'+email_approval+'&'+kode_token+'&'+item_status);
    var url_token = process.env.EMAIL_TOKEN+token;
    

    if(req.method=='OPTIONS'){
        res.send(200);
    }else{ 
        var query = "UPDATE tm_item_registration SET \
                        INC_CODE = '"+ inc_code +"', \
                        INC='"+ inc +"', \
                        DESCRIPTION='"+ description +"', \
                        ADDITIONAL_INFORMATION='"+ additional_information +"', \
                        COLLOQUIAL_NAME='"+ colloquial_name +"', \
                        BRAND='"+ brand +"', \
                        BASE_UOM='"+ base_uom +"', \
                        MATERIAL_GROUP='"+ material_group +"', \
                        EXTERNAL_GROUP='"+ external_group +"', \
                        COMPANY_ID='"+ company_id +"', \
                        COMPANY_NAME='"+ company_name +"', \
                        PLANT='"+ plant +"', \
                        STORAGE_LOCATION='"+ storage_location +"', \
                        AVERAGE_USAGE='"+ average_usage +"', \
                        SAFETY_STOCK='"+ safety_stock +"', \
                        CRITICALLITY='"+ criticallity +"', \
                        SERVER_CODE='"+ server_code +"', \
                        MESSAGE='"+ message +"', \
                        ITEM_STATUS='"+ item_status +"', \
                        MATERIAL_TYPE='"+ material_type +"', \
                        PART_NO='"+ part_no +"', \
                        TYPE_STOCK='"+type_stock +"', \
                        INC_ATTRIBUTE='"+ json_inc_attribute +"', \
                        REQUESTER_DESCRIPTION='"+ requester_description +"', \
                        REQUESTER_ADDITIONAL_INFO='"+ requester_additional_info +"', \
                        REQUESTER_JSON_INC='"+ requester_json_inc +"', \
                        ATTACHMENT='"+ attachment +"', \
                        UPDATE_DATE=NOW(), \
                        SUBMIT_DATE=NOW() \
                        WHERE REQUEST_NO='"+ request_no +"'";


            if(type_input=='NEW'){
                checkInitial = "select PART_NO from tm_initial_item where PART_NO='"+part_no+"' AND PART_NO !='"+part_no_save+"'";
                checkItemReg = "select PART_NO from tm_item_registration where PART_NO='"+part_no+"' AND PART_NO !='"+part_no_save+"'";
            }else{
                checkInitial = "select PART_NO from tm_initial_item where SERVER_CODE='"+server_code+"' and PART_NO='"+part_no+"' AND PART_NO !='"+part_no_save+"'";
                checkItemReg = "select PART_NO from tm_item_registration where SERVER_CODE='"+server_code+"' and PART_NO='"+part_no+"' AND PART_NO !='"+part_no_save+"'";
            }

            pool.getConnection(function(err,connection){
                pool.query(checkInitial,function(err,rows,fields){
                    if(err){
                        res.send("error initial search item");
                    }else{
                        
                        //pengecekan di initial item
                        if(rows==''){
                            pool.query(checkItemReg,function(err,rows,fields){
                                if(err){
                                    res.send("error search item registration");
                                }else{
                                    
                                    //pengecekan di item registration
                                    if(rows==''){
                                        pool.query(query,function(err,rows,fields){
                                            if(err){
                                                res.send("error");
                                            }else{
                                                if(item_status=='APP1'){
                                                        var transporter = nodemailer.createTransport({
                                                            host: process.env.MAIL_HOST,
                                                            port: process.env.MAIL_PORT,
                                                            secure: false,
                                                            auth: {
                                                                user: process.env.MAIL_USER,
                                                                pass: process.env.MAIL_PASSWORD
                                                            }
                                                        });
                                                        
                                                        var mailOptions = {
                                                            from: '"IMaster Sinarmas Mining" <noreply@example.com>',
                                                            to: email_approval,
                                                            subject: 'Need Approval IMaster',
                                                            html: '<div align="center" style="font-size:18px;font-weight:600;padding:20px">Notifikasi Pemberitahuan Approve</div>\
                                                                            <div style="padding:5px 20px;background-color:#ffff;margin-top:5px" align="center">\
                                                                                <div style="padding:20px;background-color:#ffff">\
                                                                                <table cellpadding="2" width="100%">\
                                                                                    <tr>\
                                                                                        <td colspan="2">\
                                                                                        <b></b> \
                                                                                        </td>\
                                                                                    </tr> \
                                                                                    <tr>\
                                                                                        <td colspan="2"><a href="'+url_token+'">[Klik Disini]</a> Untuk melakukan approval dan melihat dari request tersebut.\
                                                                                        Anda memiliki permohonan persetujuan untuk Create Material Master Request. \
                                                                                    </td>\
                                                                                    </tr>\
                                                                                   <tr>\
                                                                                     <td width="120">Requester</td>\
                                                                                     <td>: <b>'+request_by+'</b></td>\
                                                                                   </tr> \
                                                                                   <tr>\
                                                                                     <td width="120">Departement</td>\
                                                                                     <td>: <b>'+departement_id+'</b></td>\
                                                                                   </tr> \
                                                                                   <tr>\
                                                                                        <td colspan="2" style="padding-top:15px">\
                                                                                            <a href="'+url_token+'">[Klik Disini]</a> untuk melakukan approval dan juga melihat detail dari request tersebut. \
                                                                                        </td>\
                                                                                    </tr>\
                                                                                    <tr>\
                                                                                        <td colspan="2" style="padding-top:15px">\
                                                                                            Terimakasih atas perhatiannya,<br>\
                                                                                            IMaster System - Sinarmas Mining\
                                                                                        </td>\
                                                                                    </tr>\
                                                                                </table> \
                                                                                </div>\
                                                                                '
                                                        };
                                                        
                                                        transporter.sendMail(mailOptions, (err, info) => {
                                                            if (err) throw err;
                                                        });
                                                    }
                    
                                                res.send("true");
                                            }
                                        });    
                                    }else{
                                        res.send("This item has been registered in Registration List");
                                    }                           
                                }
                            })

                        }else{
                            res.send("This item has been registered in SAP");
                        }                   
                    }
                })
                connection.end();
            })
        }
});



router.use('/update_cataloguer',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true); 

    var request_no = req.body.request_no;
    var inc_code = req.body.inc_code;
    var inc = req.body.inc;
    var description = req.body.description;
    var additional_information = req.body.additional_information;
    var colloquial_name = req.body.colloquial_name;
    var brand = req.body.brand;
    var part_no = req.body.part_no;
    var base_uom = req.body.base_uom;
    var material_group = req.body.material_group;
    var external_group = req.body.external_group;
    var company_id = req.body.company_id;
    var company_name = req.body.company_name;
    var plant = req.body.plant;
    var storage_location = req.body.storage_location;
    var average_usage = req.body.average_usage;
    var safety_stock = req.body.safety_stock;
    var criticallity = req.body.criticallity;
    var server_code = req.body.server_code;
    var item_status = req.body.item_status;
    var message = req.body.message;
    var type_input = req.body.type_input;
    var material_type = req.body.material_type;
    var departement_id = req.body.departement_id;
    var valuation_class = req.body.valuation_class;
    var price_control = req.body.price_control;
    var profit_center = req.body.profit_center;
    var lab_office = req.body.lab_office;
    var material_number = req.body.material_number;
    var json_inc_attribute = req.body.json_inc_attribute;
    var attachment = req.body.attachment;

    var user_id = req.body.user_id;
    var request_by = req.body.request_by;


    if(req.method=='OPTIONS'){
        res.send(200);
    }else{ 
        var query = "UPDATE tm_item_registration SET \
                        INC_CODE = '"+ inc_code +"', \
                        INC='"+ inc +"', \
                        DESCRIPTION='"+ description +"', \
                        ADDITIONAL_INFORMATION='"+ additional_information +"', \
                        COLLOQUIAL_NAME='"+ colloquial_name +"', \
                        BRAND='"+ brand +"', \
                        BASE_UOM='"+ base_uom +"', \
                        PART_NO='"+part_no +"', \
                        MATERIAL_GROUP='"+ material_group +"', \
                        EXTERNAL_GROUP='"+ external_group +"', \
                        COMPANY_ID='"+ company_id +"', \
                        COMPANY_NAME='"+ company_name +"', \
                        PLANT='"+ plant +"', \
                        STORAGE_LOCATION='"+ storage_location +"', \
                        AVERAGE_USAGE='"+ average_usage +"', \
                        SAFETY_STOCK='"+ safety_stock +"', \
                        CRITICALLITY='"+ criticallity +"', \
                        SERVER_CODE='"+ server_code +"', \
                        MESSAGE='"+ message +"', \
                        ITEM_STATUS='"+ item_status +"', \
                        MATERIAL_TYPE='"+ material_type +"', \
                        PRICE_CONTROL='"+ price_control +"', \
                        VALUATION_CLASS ='"+ valuation_class +"', \
                        PROFIT_CENTER='"+ profit_center +"', \
                        LAB_OFFICE='"+ lab_office +"', \
                        MATERIAL_NUMBER='"+ material_number +"', \
                        CATALOGUER_DATE=NOW() , \
                        USER_CATALOGUER='"+user_id+"',\
                        INC_ATTRIBUTE='"+ json_inc_attribute +"', \
                        ATTACHMENT='"+ attachment +"', \
                        UPDATE_DATE=NOW() \
                        WHERE REQUEST_NO='"+ request_no +"'";

            pool.getConnection(function(err,connection){
                if(err){
                    res.send("Error Connection");
                }else{
                    pool.query(query,function(err,rows,fields){
                        if(err){
                            res.send("error");
                        }else{
                            res.send("true");
                        }
                    })
                }
                connection.end();
            })
        }
});


router.use('/update_cataloguer_new',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true); 

    var request_no = req.body.request_no;
    var inc_code = req.body.inc_code;
    var inc = req.body.inc;
    var description = req.body.description;
    var additional_information = req.body.additional_information;
    var colloquial_name = req.body.colloquial_name;
    var brand = req.body.brand;
    var part_no = req.body.part_no;
    var base_uom = req.body.base_uom;
    var material_group = req.body.material_group;
    var external_group = req.body.external_group;
    var company_id = req.body.company_id;
    var company_name = req.body.company_name;
    var plant = req.body.plant;
    var storage_location = req.body.storage_location;
    var average_usage = req.body.average_usage;
    var safety_stock = req.body.safety_stock;
    var criticallity = req.body.criticallity;
    var server_code = req.body.server_code;
    var item_status = req.body.item_status;
    var message = req.body.message;
    var type_input = req.body.type_input;
    var material_type = req.body.material_type;
    var departement_id = req.body.departement_id;
    var valuation_class = req.body.valuation_class;
    var price_control = req.body.price_control;
    var profit_center = req.body.profit_center;
    var lab_office = req.body.lab_office;
    var material_number = req.body.material_number;
    var part_no_save = req.body.part_no_save;
    var user_id = req.body.user_id;
    var request_by = req.body.request_by;
    var json_inc_attribute = req.body.json_inc_attribute;
    var attachment = req.body. attachment;

    if(req.method=='OPTIONS'){
        res.send(200);
    }else{ 
        var query = "UPDATE tm_item_registration SET \
                        INC_CODE = '"+ inc_code +"', \
                        INC='"+ inc +"', \
                        DESCRIPTION='"+ description +"', \
                        ADDITIONAL_INFORMATION='"+ additional_information +"', \
                        COLLOQUIAL_NAME='"+ colloquial_name +"', \
                        BRAND='"+ brand +"', \
                        PART_NO='"+ part_no +"', \
                        BASE_UOM='"+ base_uom +"', \
                        MATERIAL_GROUP='"+ material_group +"', \
                        EXTERNAL_GROUP='"+ external_group +"', \
                        COMPANY_ID='"+ company_id +"', \
                        COMPANY_NAME='"+ company_name +"', \
                        PLANT='"+ plant +"', \
                        STORAGE_LOCATION='"+ storage_location +"', \
                        AVERAGE_USAGE='"+ average_usage +"', \
                        SAFETY_STOCK='"+ safety_stock +"', \
                        CRITICALLITY='"+ criticallity +"', \
                        SERVER_CODE='"+ server_code +"', \
                        MESSAGE='"+ message +"', \
                        ITEM_STATUS='"+ item_status +"', \
                        MATERIAL_TYPE='"+ material_type +"', \
                        PRICE_CONTROL='"+ price_control +"', \
                        VALUATION_CLASS ='"+ valuation_class +"', \
                        PROFIT_CENTER='"+ profit_center +"', \
                        LAB_OFFICE='"+ lab_office +"', \
                        MATERIAL_NUMBER='"+ material_number +"', \
                        CATALOGUER_DATE=NOW() , \
                        USER_CATALOGUER='"+user_id+"',\
                        INC_ATTRIBUTE='"+ json_inc_attribute +"', \
                        ATTACHMENT='"+ attachment +"', \
                        UPDATE_DATE=NOW() \
                        WHERE REQUEST_NO='"+ request_no +"'";


            if(type_input=='NEW'){
                checkInitial = "select PART_NO from tm_initial_item where PART_NO='"+part_no+"' AND PART_NO !='"+part_no_save+"'";
                checkItemReg = "select PART_NO from tm_item_registration where PART_NO='"+part_no+"' AND PART_NO !='"+part_no_save+"'";
            }else{
                checkInitial = "select PART_NO from tm_initial_item where SERVER_CODE='"+server_code+"' and PART_NO='"+part_no+"' AND PART_NO !='"+part_no_save+"'";
                checkItemReg = "select PART_NO from tm_item_registration where SERVER_CODE='"+server_code+"' and PART_NO='"+part_no+"' AND PART_NO !='"+part_no_save+"'";
            }

            checkMaterialNumber = "select MATERIAL_NUMBER from tm_item_registration where MATERIAL_NUMBER='"+material_number+"' and REQUEST_NO !='"+request_no+"'";

            pool.getConnection(function(err,connection){
                pool.query(checkInitial,function(err,rows,fields){
                    if(err){
                        res.send("error initial search item");
                    }else{
                        
                        //pengecekan di initial item
                        if(rows==''){
                            pool.query(checkItemReg,function(err,rows,fields){
                                if(err){
                                    res.send("error search item registration");
                                }else{
                                    
                                    //pengecekan di item registration
                                    if(rows==''){
                                        if(server_code=='BC'){
                                            
                                            pool.query(checkMaterialNumber,function(err,rows,fields){
                                                if(err){
                                                    res.send(err);
                                                }else{
                                                    if(rows==''){
                                                        pool.query(query,function(err,rows,fields){
                                                            if(err){
                                                                res.send(err);
                                                            }else{
                                                                res.send("true");
                                                            }
                                                        });
                                                    }else{
                                                        res.send("This item material number has been registered in Registration List");
                                                    }                                               
                                                }
                                            });

                                        }else{
                                            pool.query(query,function(err,rows,fields){
                                                if(err){
                                                    res.send(err);
                                                }else{
                                                    res.send("true");
                                                }
                                            });
                                        }
                                            
                                    }else{
                                        res.send("This item has been registered in Registration List");
                                    }                           
                                }
                            })

                        }else{
                            res.send("This item has been registered in SAP");
                        }                   
                    }
                })
                connection.end();
            })
        }
});



router.use('/delete',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true); 
    
    var request_no = req.body.request_no;

    if(req.method=='OPTIONS'){
        res.send(200);
    }else{    

        var query = "DELETE FROM tm_item_registration WHERE REQUEST_NO='"+ request_no +"'";

        pool.getConnection(function(err,connection){
            if(err){
                res.send("Error Connection");
            }else{
                pool.query(query,function(err,rows,fields){
                    if(err){
                        res.send("false");
                    }else{
                        res.send("true");
                    }
                })
            }
            connection.end();
        })
    }
})


router.use('/submit',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true); 
    
    var values = [];
    var item = req.body.item;
    var item_status = req.body.item_status;
    var type_stock = req.body.type_stock;

    //email element
    var user_id = req.body.user_id;
    var username = req.body.username;
    var departement_id = req.body.departement_id;
    var email_approval = req.body.email_approval;
    var user_approval = req.body.user_approval;
    var kode_token = Math.floor(Math.random() * 100000);
    var token = btoa(user_id+'&'+departement_id+'&'+username+'&'+user_approval+'&'+email_approval+'&'+kode_token+'&'+item_status);
    var url_token = process.env.EMAIL_TOKEN+token;
    var update_date = "";

    if(item_status=='APP1'){
        update_date = ",SUBMIT_DATE=NOW()";
    }else if(item_status=='APP2'){
        update_date = ",APPROVAL1_DATE=NOW(),USER_APPROVAL1='"+user_id+"'";
    }else if(item_status=='CAT'){
        update_date = ",APPROVAL2_DATE=NOW(),USER_APPROVAL2='"+user_id+"'";
    }else if(item_status=='NI'){
        update_date = ",NEED_INFO_DATE=NOW(),NEED_INFO_USER='"+user_id+"'";
    }else if(item_status=='RJT'){
        update_date = ",REJECT_DATE=NOW(),REJECT_USER='"+user_id+"'";
    }

    if(req.method=='OPTIONS'){
        res.send(200);
    }else{    
        
        if(item_status=='APP1' || item_status=='APP2'){
            var query = "UPDATE tm_item_registration \
                        SET ITEM_STATUS = CASE \
                        WHEN TYPE_STOCK = 'Y' THEN 'APP1' \
                        WHEN TYPE_STOCK = 'N' THEN  'CAT' \
                        ELSE ITEM_STATUS \
                        END\
                        "+ update_date +",UPDATE_DATE=NOW() WHERE REQUEST_NO IN ("+ item +")";
        
        }else{
            var query = "UPDATE tm_item_registration SET ITEM_STATUS='"+ item_status +"' "+ update_date +",UPDATE_DATE=NOW() WHERE REQUEST_NO IN ("+ item +")";
        }

        pool.getConnection(function(err,connection){
            if(err){
                res.send("Error Connection");
            }else{

                if(item !=''){
                    pool.query(query,function(err,rows,fields){
                        if(err){
                            res.send("false");
                        }else{
                            
                            if((item_status=='APP1' || item_status=='APP2') && type_stock=='Y'){
                                var transporter = nodemailer.createTransport({
                                    host: process.env.MAIL_HOST,
                                    port: process.env.MAIL_PORT,
                                    secure: false,
                                    auth: {
                                        user: process.env.MAIL_USER,
                                        pass: process.env.MAIL_PASSWORD
                                    }
                                });
                                
                                var mailOptions = {
                                    from: '"IMaster Sinarmas Mining" <noreply@example.com>',
                                    to: email_approval,
                                    subject: 'Need Approval IMaster',
                                    html: '<div align="center" style="font-size:18px;font-weight:600;padding:20px">Notifikasi Pemberitahuan Approve</div>\
                                                    <div style="padding:5px 20px;background-color:#ffff;margin-top:5px" align="center">\
                                                        <div style="padding:20px;background-color:#ffff">\
                                                        <table cellpadding="2" width="100%">\
                                                           <tr>\
                                                              <td colspan="2">\
                                                                <b></b> \
                                                              </td>\
                                                           </tr> \
                                                           <tr>\
                                                              <td colspan="2"><a href="'+url_token+'">[Klik Disini]</a> Untuk melakukan approval dan melihat dari request tersebut.\
                                                              Anda memiliki permohonan persetujuan untuk Create Material Master Request. \
                                                            </td>\
                                                           </tr>\
                                                           <tr>\
                                                             <td width="120">Requester</td>\
                                                             <td>: <b>'+username+'</b></td>\
                                                           </tr> \
                                                           <tr>\
                                                             <td width="120">Departement</td>\
                                                             <td>: <b>'+departement_id+'</b></td>\
                                                           </tr> \
                                                           <tr>\
                                                                <td colspan="2" style="padding-top:15px">\
                                                                    <a href="'+url_token+'">[Klik Disini]</a> untuk melakukan approval dan juga melihat detail dari request tersebut. \
                                                                </td>\
                                                            </tr>\
                                                            <tr>\
                                                                <td colspan="2" style="padding-top:15px">\
                                                                    Terimakasih atas perhatiannya,<br>\
                                                                    IMaster System - Sinarmas Mining\
                                                                </td>\
                                                            </tr>\
                                                        </table> \
                                                        </div>\
                                                        '
                                };
                                
                                transporter.sendMail(mailOptions, (err, info) => {
                                    if (err) throw err;
                                });
                            }
    
                            if(item_status=='NI'){
                                var sqlneedinfo = "SELECT a.*,b.EMAIL \
                                                FROM tm_item_registration a LEFT JOIN tm_user b ON a.USER_INPUT=b.USER_ID \
                                                WHERE REQUEST_NO IN ("+ item +") LIMIT 1";
    
                                pool.query(sqlneedinfo,function(err,rows,fields){
                                    if(err){
                                        console.log(sqlneedinfo);
                                    }
    
                                    for(var i=0; i<rows.length; i++){
                                        var email_user = rows[i].EMAIL;
    
                                        var transporter = nodemailer.createTransport({
                                            host: process.env.MAIL_HOST,
                                            port: process.env.MAIL_PORT,
                                            secure: false,
                                            auth: {
                                                user: process.env.MAIL_USER,
                                                pass: process.env.MAIL_PASSWORD
                                            }
                                        });
                                        
                                        var mailOptions = {
                                                from: '"IMaster Sinarmas Mining" <noreply@example.com>',
                                                to: email_user,
                                                subject: 'Need More Info Requester IMaster',
                                                html: '<div align="center" style="font-size:18px;font-weight:600;padding:20px">Notifikasi Pemberitahuan Need More Info</div>\
                                                                <div style="padding:5px 20px;background-color:#ffff;margin-top:5px" align="center">\
                                                                    <div style="padding:20px;background-color:#ffff">\
                                                                    <table cellpadding="2" width="100%">\
                                                                    <tr style="display:none">\
                                                                        <td colspan="2">\
                                                                            <b>Hi '+username+'</b> \
                                                                        </td>\
                                                                    </tr> \
                                                                    <tr>\
                                                                        <td colspan="2">Item anda telah dikambalikan oleh Approval 1, mohon agar lengkapi data item tersebut,\
                                                                        agar dapat diproses. \
                                                                        </td>\
                                                                    </tr>\
                                                                    <tr>\
                                                                        <td width="120">Requester</td>\
                                                                        <td>: <b>'+username+'</b></td>\
                                                                    </tr> \
                                                                    <tr>\
                                                                        <td width="120">Departement</td>\
                                                                        <td>: <b>'+departement_id+'</b></td>\
                                                                    </tr> \
                                                                    <tr>\
                                                                        <td colspan="2"><hr></td>\
                                                                    </tr>\
                                                                    <tr>\
                                                                        <td colspan="2"><b>Detail Item</b></td>\
                                                                    </tr>\
                                                                    <tr>\
                                                                        <td width="150">INC Code</td>\
                                                                        <td>'+ rows[i].INC_CODE +'</td>\
                                                                    </tr>\
                                                                    <tr>\
                                                                        <td>INC Name</td>\
                                                                        <td>'+ rows[i].INC +'</td>\
                                                                    </tr>\
                                                                    <tr>\
                                                                        <td>Description</td>\
                                                                        <td>'+ rows[i].DESCRIPTION +'</td>\
                                                                    </tr>\
                                                                    <tr>\
                                                                        <td>Company</td>\
                                                                        <td>'+ rows[i].COMPANY_NAME +'</td>\
                                                                    </tr>\
                                                                    <tr>\
                                                                        <td>Material Type</td>\
                                                                        <td>'+ rows[i].MATERIAL_TYPE +'</td>\
                                                                    </tr>\
                                                                    <tr>\
                                                                        <td colspan="2" style="padding-top:15px">\
                                                                            Terimakasih atas perhatiannya,<br>\
                                                                            IMaster System - Sinarmas Mining\
                                                                        </td>\
                                                                    </tr>\
                                                                    </table> \
                                                                    </div>\
                                                                    '
                                            };
                                            
                                            transporter.sendMail(mailOptions, (err, info) => {
                                                if (err) throw err;
                                            }); 
                                    }
                                });                                 
                            }
                            
                            res.send("true");
                        }
                    });
                }
                
            }
            connection.end();
        })
    }
});



router.use('/submitapp',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true); 
    
    var values = [];
    var item = req.body.item;
    var server = req.body.server;
    var item_status = req.body.item_status;

    //email element
    var user_id = req.body.user_id;
    var username = req.body.username;
    var departement_id = req.body.departement_id;
    var email_approval = req.body.email_approval;
    var user_approval = req.body.user_approval;
    var update_date = "";

    if(item_status=='APP1'){
        update_date = ",SUBMIT_DATE=NOW()";
    }else if(item_status=='APP2'){
        update_date = ",APPROVAL1_DATE=NOW(),USER_APPROVAL1='"+user_id+"'";
    }else if(item_status=='CAT'){
        update_date = ",APPROVAL2_DATE=NOW(),USER_APPROVAL2='"+user_id+"'";
    }else if(item_status=='NI'){
        update_date = ",NEED_INFO_DATE=NOW(),NEED_INFO_USER='"+user_id+"'";
    }else if(item_status=='RJT'){
        update_date = ",REJECT_DATE=NOW(),REJECT_USER='"+user_id+"'";
    }

    if(req.method=='OPTIONS'){
        res.send(200);
    }else{    
         
        var query = "UPDATE tm_item_registration SET ITEM_STATUS='"+ item_status +"' "+ update_date +",UPDATE_DATE=NOW() WHERE REQUEST_NO IN ("+ item +")";
        pool.getConnection(function(err,connection){
            if(err){
                res.send("Error Connection");
            }else{
                pool.query(query,function(err,rows,fields){
                    if(err){
                        res.send("false");
                    }else{

                        if(item_status=='APP2'){
                            var sqlemail = "SELECT a.*,b.* FROM tx_approval_server a \
                                            LEFT JOIN tm_user b ON a.APPROVAL2=b.USER_ID \
                                            WHERE a.STATUS='Y' and a.SERVER IN ("+ server +") LIMIT 1";

                            pool.query(sqlemail,function(err,rows,fields){
                                for(var i=0; i<rows.length; i++){
                                    email_approval   = rows[i].EMAIL;
                                    user_approval    = rows[i].USER_NAME;
                                    server           = rows[i].SERVER;
                                    //departement_id   = rows[i].DEPARTEMENT_ID;
                                    //res.send(rows[i].EMAIL);
                                
                                    var kode_token = Math.floor(Math.random() * 100000);
                                    var token = btoa(user_id+'&'+departement_id+'&'+username+'&'+user_approval+'&'+email_approval+'&'+kode_token+'&'+item_status+'&'+server);
                                    var url_token = process.env.EMAIL_TOKEN+token;

                                    var transporter = nodemailer.createTransport({
                                        host: process.env.MAIL_HOST,
                                        port: process.env.MAIL_PORT,
                                        secure: false,
                                        auth: {
                                            user: process.env.MAIL_USER,
                                            pass: process.env.MAIL_PASSWORD
                                        }
                                    });

                                    var mailOptions = {
                                        from: '"IMaster Sinarmas Mining" <noreply@example.com>',
                                        to: email_approval,
                                        subject: 'Need Approval IMaster',
                                        html: '<div align="center" style="font-size:18px;font-weight:600;padding:20px">Notifikasi Pemberitahuan Approve</div>\
                                                        <div style="padding:5px 20px;background-color:#ffff;margin-top:5px" align="center">\
                                                            <div style="padding:20px;background-color:#ffff">\
                                                            <table cellpadding="2" width="100%">\
                                                            <tr>\
                                                                <td colspan="2">\
                                                                <b></b> \
                                                                </td>\
                                                            </tr> \
                                                            <tr>\
                                                                <td colspan="2"><a href="'+url_token+'">[Klik Disini]</a> Untuk melakukan approval dan melihat dari request tersebut.\
                                                                Anda memiliki permohonan persetujuan untuk Create Material Master Request. \
                                                            </td>\
                                                            </tr>\
                                                            <tr>\
                                                            <td colspan="2" style="padding-top:15px">\
                                                                <a href="'+url_token+'">[Klik Disini]</a> untuk melakukan approval dan juga melihat detail dari request tersebut. \
                                                            </td>\
                                                        </tr>\
                                                        <tr>\
                                                            <td colspan="2" style="padding-top:15px">\
                                                                Terimakasih atas perhatiannya,<br>\
                                                                IMaster System - Sinarmas Mining\
                                                            </td>\
                                                        </tr>\
                                                            </table> \
                                                            </div>\
                                                            '
                                    };                          
                                    transporter.sendMail(mailOptions, (err, info) => {
                                        if (err) throw err;
                                    });
                                }
                                                    
                            });
                        }
                        
                        
                        if(item_status=='NI'){
                            var sqlneedinfo = "SELECT a.*,b.EMAIL \
                                            FROM tm_item_registration a LEFT JOIN tm_user b ON a.USER_INPUT=b.USER_ID \
                                            WHERE REQUEST_NO IN ("+ item +") LIMIT 1";

                            pool.query(sqlneedinfo,function(err,rows,fields){
                                if(err){
                                    console.log(sqlneedinfo);
                                }

                                for(var i=0; i<rows.length; i++){
                                    var email_user = rows[i].EMAIL;

                                    var transporter = nodemailer.createTransport({
                                        host: process.env.MAIL_HOST,
                                        port: process.env.MAIL_PORT,
                                        secure: false,
                                        auth: {
                                            user: process.env.MAIL_USER,
                                            pass: process.env.MAIL_PASSWORD
                                        }
                                    });
                                    
                                    var mailOptions = {
                                            from: '"IMaster Sinarmas Mining" <noreply@example.com>',
                                            to: email_user,
                                            subject: 'Need More Info Requester IMaster',
                                            html: '<div align="center" style="font-size:18px;font-weight:600;padding:20px">Notifikasi Pemberitahuan Need More Info</div>\
                                                            <div style="padding:5px 20px;background-color:#ffff;margin-top:5px" align="center">\
                                                                <div style="padding:20px;background-color:#ffff">\
                                                                <table cellpadding="2" width="100%">\
                                                                <tr style="display:none">\
                                                                    <td colspan="2">\
                                                                        <b>Hi '+username+'</b> \
                                                                    </td>\
                                                                </tr> \
                                                                <tr>\
                                                                    <td colspan="2">Item anda telah dikambalikan oleh Approval 1, mohon agar lengkapi data item tersebut,\
                                                                    agar dapat diproses. \
                                                                    </td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td width="120">Requester</td>\
                                                                    <td>: <b>'+username+'</b></td>\
                                                                </tr> \
                                                                <tr>\
                                                                    <td width="120">Departement</td>\
                                                                    <td>: <b>'+departement_id+'</b></td>\
                                                                </tr> \
                                                                <tr>\
                                                                    <td colspan="2"><hr></td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td colspan="2"><b>Detail Item</b></td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td width="150">INC Code</td>\
                                                                    <td>'+ rows[i].INC_CODE +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>INC Name</td>\
                                                                    <td>'+ rows[i].INC +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Description</td>\
                                                                    <td>'+ rows[i].DESCRIPTION +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Company</td>\
                                                                    <td>'+ rows[i].COMPANY_NAME +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td>Material Type</td>\
                                                                    <td>'+ rows[i].MATERIAL_TYPE +'</td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td colspan="2" style="padding-top:15px">\
                                                                        Terimakasih atas perhatiannya,<br>\
                                                                        IMaster System - Sinarmas Mining\
                                                                    </td>\
                                                                </tr>\
                                                                </table> \
                                                                </div>\
                                                                '
                                        };
                                        
                                        transporter.sendMail(mailOptions, (err, info) => {
                                            if (err) throw err;
                                        }); 
                                }
                            });

                               
                        }
                        
                        res.send("true");
                    }
                })
            }
            connection.end();
        })
    }
});


router.use('/count',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true); 
    
    var values = [];
    var user_id = req.body.user_id;
    var status = req.body.status;

    if(req.method=='OPTIONS'){
        res.send(200);
    }else{    
         
        var query = "SELECT COUNT(REQUEST_NO) as total FROM tm_item_registration WHERE USER_INPUT='"+ user_id +"' AND ITEM_STATUS='"+ status +"'";

        pool.getConnection(function(err,connection){
            if(err){
                res.send("Error Connection");
            }else{
                pool.query(query,function(err,rows,fields){
                    if(err){
                        res.send("false");
                    }else{
                        res.send(rows);
                    }
                })
            }
            connection.end();
        })
    }
});


module.exports = router;

