var express = require("express");
var router = express.Router();
var pool = require('./db_connection');
const jwt = require("jsonwebtoken");

var mysql = require("mysql");
var cors = require("cors");
var myobj = {};
//var md5 = require('md5');

router.use("/getUser", async (req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET,POST,OPTIONS,PUT,PATCH,DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);

  var username = req.body.username;
  var password = req.body.password;

  var sql =
    "SELECT a.*,b.DEPARTEMENT_DESC FROM tm_user a \
               LEFT JOIN tm_departement b ON a.DEPARTEMENT_ID=b.DEPARTEMENT_ID \
               WHERE a.EMAIL='" +
    username +
    "' and a.STATUS='Y' and a.PASSWORD=MD5('" +
    password +
    "') \
               ";

  pool.getConnection(function (err, connection) {
    
    if (err) {
      connection.release();
      console.log("Error Koneksi Login");
      res.send(false);
    } else {
      pool.query(sql, function (err, rows, fields) {
        if (err) {
          console.log("Error Query Login");
          res.send(false);
        } else {
          res.send(rows);
        }
      });
    }
    connection.end();
  });
});

/** verifyToken method - this method verifies token */
function verifyToken(req, res, next) {
  const bearerHeader = req.headers["authorization"];

  if (typeof bearerHeader !== "undefined") {
    const bearer = bearerHeader.split(" ");

    const bearerToken = bearer[1];

    req.token = bearerToken;

    next();
  } else {
    res.sendStatus(403);
  }
}

router.post("/api/getToken", (req, res) => {
  const user = {
    id: 1,
    username: "johndoe",
    email: "john.doe@test.com",
  };
  jwt.sign({ user }, "SuperSecRetKey", { expiresIn: 60 * 60 }, (err, token) => {
    res.json({ token });
  });
});

function jsonShow(req, res, next) {
  var tes = req;
  return tes;
}

router.post("/api/posts", verifyToken, (req, res) => {
  jwt.verify(req.token, "SuperSecRetKey", (err, authData) => {
    if (err) {
      res.sendStatus(403);
    } else {
      var tes = jsonShow("123");
      res.send(tes);
    }
  });
});
module.exports = router;
