var express = require('express');
var router = express.Router();
var pool = require('./db_connection');
const jwt = require('jsonwebtoken');
var mysql = require('mysql');
var cors = require('cors');
//var md5 = require('md5');
var myobj = {};


router.use('/server', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select * from tm_server limit 20";
    }else{
        var sql = "select * from tm_server where SERVER_CODE like '%"+param+"%' \
                   or SERVER_NAME like '%"+param+"%' \
                   limit 20";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            })
        }
        connection.end();
    })
});


router.use('/company', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select * from tm_company ORDER BY ID DESC limit 250";
    }else{
        var sql = "select * from tm_company where COMPANY_ID like '%"+param+"%' \
                   or SHORTNAME like '%"+param+"%' or COMPANY_NAME like '%"+param+"%' or CITY like '%"+param+"%' or SERVER_CODE like '%"+param+"%' \
                   ORDER BY ID DESC limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/plant', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select a.*,b.COMPANY_NAME from tm_plant a LEFT JOIN \
                   tm_company b ON a.COMPANY_ID=b.COMPANY_ID ORDER BY a.ID DESC limit 250";
    }else{
        var sql = "select a.*,b.COMPANY_NAME from tm_plant a\
                   LEFT JOIN tm_company b ON a.COMPANY_ID=b.COMPANY_ID where a.PLANT_CODE like '%"+param+"%' \
                   or b.COMPANY_NAME like '%"+param+"%' or a.PLANT_NAME like '%"+param+"%' \
                   ORDER BY a.ID DESC limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/storage_location', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select a.*,b.PLANT_NAME from tm_storage_location a LEFT JOIN \
                   tm_plant b ON a.PLANT_CODE=b.PLANT_CODE ORDER BY a.ID DESC limit 250";
    }else{
        var sql = "select a.*,b.PLANT_NAME from tm_storage_location a\
                   LEFT JOIN tm_plant b ON a.PLANT_CODE=b.PLANT_CODE where a.STORAGE_LOCATION_NAME like '%"+param+"%' \
                   or a.STORAGE_LOCATION_ID like '%"+param+"%' or b.PLANT_NAME like '%"+param+"%'\
                   ORDER BY a.ID DESC limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});

router.use('/uom', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select * from tm_uom limit 250";
    }else{
        var sql = "select * from tm_uom WHERE UOM_CODE LIKE '%"+param+"%' OR UOM_NAME LIKE '%"+param+"%' limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/profit_center', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select * from tm_profit_center limit 250";
    }else{
        var sql = "select * from tm_profit_center WHERE PROFIT_CENTER_CODE LIKE '%"+param+"%' OR \
                    PROFIT_CENTER_DESC LIKE '%"+param+"%' OR MATERIAL_TYPE LIKE '%"+param+"%' \
                    OR SERVER_CODE LIKE '%"+param+"%' \
                    limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/valuation_class', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select * from tm_valuation_class ORDER BY ID DESC limit 250";
    }else{
        var sql = "select * from tm_valuation_class WHERE VALUATION_CLASS_ID LIKE '%"+param+"%' OR \
                    VALUATION_CLASS_NAME LIKE '%"+param+"%' OR MATERIAL_TYPE_CODE LIKE '%"+param+"%' \
                    OR SERVER_CODE LIKE '%"+param+"%' \
                    ORDER BY ID DESC limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/price_control', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select * from tm_price_control GROUP BY PRICE_CONTROL_ID,MATERIAL_TYPE_CODE,SERVER_CODE ORDER BY ID DESC limit 250";
    }else{
        var sql = "select * from tm_price_control WHERE PRICE_CONTROL_ID LIKE '%"+param+"%' OR \
                    PRICE_CONTROL_DESC LIKE '%"+param+"%' OR MATERIAL_TYPE_CODE LIKE '%"+param+"%' \
                    OR SERVER_CODE LIKE '%"+param+"%' \
                    GROUP BY PRICE_CONTROL_ID,MATERIAL_TYPE_CODE,SERVER_CODE ORDER BY ID DESC limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/lab_office', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select a.*,b.EXTERNAL_GROUP_NAME from tm_lab_office a \
                    LEFT JOIN tm_external_group b ON a.MATERIAL_GROUP=b.EXTERNAL_GROUP_CODE \
                    ORDER BY a.ID DESC limit 250";
    }else{
        var sql = "select a.*,b.EXTERNAL_GROUP_NAME from tm_lab_office a \
                    LEFT JOIN tm_external_group b ON a.MATERIAL_GROUP=b.EXTERNAL_GROUP_CODE \
                    WHERE b.EXTERNAL_GROUP_CODE LIKE '%"+param+"%' OR \
                    b.EXTERNAL_GROUP_NAME LIKE '%"+param+"%' OR a.LAB_OFFICE LIKE '%"+param+"%' \
                    ORDER BY a.ID,a.LAB_OFFICE DESC limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});



router.use('/material_type', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select * from tm_material_type GROUP BY MATERIAL_TYPE_CODE,SERVER_CODE ORDER BY ID DESC limit 250";
    }else{
        var sql = "select * from tm_material_type WHERE MATERIAL_TYPE_CODE LIKE '%"+param+"%' OR \
                    MATERIAL_TYPE_NAME LIKE '%"+param+"%' OR SERVER_CODE LIKE '%"+param+"%' \
                    GROUP BY MATERIAL_TYPE_CODE,SERVER_CODE ORDER BY ID DESC limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/material_group', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select * from tm_material_group ORDER BY ID DESC limit 250";
    }else{
        var sql = "select * from tm_material_group WHERE MATERIAL_GROUP_ID LIKE '%"+param+"%' OR \
                    MATERIAL_GROUP_NAME LIKE '%"+param+"%' OR \
                    MATERIAL_GROUP_DESC LIKE '%"+param+"%' OR SERVER_CODE LIKE '%"+param+"%' \
                    ORDER BY ID DESC limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/external_group', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select a.*,b.MATERIAL_GROUP_ID,MATERIAL_GROUP_NAME from tm_external_group a LEFT JOIN \
                   tm_material_group b ON a.MATERIAL_GROUP_CODE=b.MATERIAL_GROUP_ID \
                   GROUP BY a.MATERIAL_GROUP_CODE,a.EXTERNAL_GROUP_CODE ORDER BY ID DESC limit 250";
    }else{
        var sql = "select a.*,b.MATERIAL_GROUP_ID,MATERIAL_GROUP_NAME from tm_external_group a LEFT JOIN \
                    tm_material_group b ON a.MATERIAL_GROUP_CODE=b.MATERIAL_GROUP_ID \
                    WHERE a.EXTERNAL_GROUP_CODE LIKE '%"+param+"%' OR \
                    a.EXTERNAL_GROUP_NAME LIKE '%"+param+"%' OR \
                    b.MATERIAL_GROUP_NAME LIKE '%"+param+"%' OR b.MATERIAL_GROUP_ID LIKE '%"+param+"%' \
                    GROUP BY a.MATERIAL_GROUP_CODE,a.EXTERNAL_GROUP_CODE ORDER BY ID DESC limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/inc', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select * from tm_inc ORDER BY ID DESC limit 250";
    }else{
        var sql = "select * from tm_inc WHERE INC_CODE LIKE '%"+param+"%' OR \
                    INC_NAME LIKE '%"+param+"%' OR \
                    SHORT_NAME LIKE '%"+param+"%' OR COLLOQUIAL_NAME LIKE '%"+param+"%' \
                    ORDER BY ID DESC limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/inc_attribute', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select * from tm_inc_attribute ORDER BY ATTRIBUTE_ID DESC limit 500";
    }else{
        var sql = "select * from tm_inc_attribute WHERE ATTRIBUTE_CODE LIKE '%"+param+"%' OR \
                    EXPLANATION LIKE '%"+param+"%' OR INC_CODE LIKE '%"+param+"%' OR ATTRIBUTE_DESC LIKE '%"+param+"%' \
                    ORDER BY ATTRIBUTE_ID DESC limit 500";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/initial_item', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select * from tm_initial_item limit 250";
    }else{
        var sql = "select * from tm_initial_item WHERE SERVER_CODE LIKE '%"+param+"%' OR \
                    MATERIAL_NUMBER LIKE '%"+param+"%' OR DESCRIPTION LIKE '%"+param+"%' \
                    limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/departement', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select * from tm_departement ORDER BY ID DESC limit 250";
    }else{
        var sql = "select * from tm_departement WHERE DEPARTEMENT_ID LIKE '%"+param+"%' OR \
                    DEPARTEMENT_DESC LIKE '%"+param+"%' ORDER BY ID DESC limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/user', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select a.*,b.ROLE_NAME from tm_user a \
                    LEFT JOIN tm_role b ON a.ROLE_ID=b.ROLE_ID\
                    ORDER BY a.DEPARTEMENT_ID ASC\
                    limit 250";
    }else{
        var sql = "select a.*,b.ROLE_NAME from tm_user a \
                    LEFT JOIN tm_role b ON a.ROLE_ID=b.ROLE_ID \
                    WHERE USER_NAME LIKE '%"+param+"%' OR \
                    NAME LIKE '%"+param+"%' OR DEPARTEMENT_ID LIKE '%"+param+"%' \
                    OR EMAIL LIKE '%"+param+"%' \
                    OR SERVER_CODE LIKE '%"+param+"%' \
                    ORDER BY a.DEPARTEMENT_ID ASC\
                    limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/role', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select a.*,b.*,c.* from tm_user a \
                    LEFT JOIN tm_departement b ON a.DEPARTEMENT_ID=b.DEPARTEMENT_ID\
                    LEFT JOIN tm_role c ON a.ROLE_ID=c.ROLE_ID\
                    WHERE a.ROLE_ID !='REQ' limit 250";
    }else{
        var sql = "select a.*,b.*,c.* from tm_user a \
                    LEFT JOIN tm_departement b ON a.DEPARTEMENT_ID=b.DEPARTEMENT_ID\
                    LEFT JOIN tm_role c ON a.ROLE_ID=c.ROLE_ID\
                    WHERE a.ROLE_ID !='REQ' and (USER_NAME LIKE '%"+param+"%' OR \
                    NAME LIKE '%"+param+"%' OR DEPARTEMENT_ID LIKE '%"+param+"%' \
                    OR SERVER_CODE LIKE '%"+param+"%') \
                    limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/departement_role', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select a.*,b.DEPARTEMENT_DESC,c.NAME from tx_departement_role a \
                    LEFT JOIN tm_departement b ON a.DEPARTEMENT_ID=b.DEPARTEMENT_ID \
                    LEFT JOIN tm_user c ON a.USER_ID=c.USER_ID \
                    GROUP BY a.DEPARTEMENT_ID,a.USER_ID \
                    limit 250";
    }else{
        var sql = "select a.*,b.DEPARTEMENT_DESC,c.NAME from tx_departement_role a \
                    LEFT JOIN tm_departement b ON a.DEPARTEMENT_ID=b.DEPARTEMENT_ID \
                    LEFT JOIN tm_user c ON a.USER_ID=c.USER_ID \
                    WHERE b.DEPARTEMENT_DESC LIKE '%"+param+"%' OR \
                    c.NAME LIKE '%"+param+"%' \
                    GROUP BY a.DEPARTEMENT_ID,a.USER_ID \
                    limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/server_role', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;

    if(param==''){
        var sql = "select a.*,b.NAME as 'APPROVAL2_NAME',c.NAME as 'CATALOGUER_NAME',d.DEPARTEMENT_DESC from tx_approval_server a \
                    LEFT JOIN tm_user b ON a.APPROVAL2=b.USER_ID \
                    LEFT JOIN tm_user c ON a.CATALOGUER=c.USER_ID \
                    LEFT JOIN tm_departement d ON b.DEPARTEMENT_ID=d.DEPARTEMENT_ID \
                    GROUP BY a.SERVER,a.APPROVAL2 \
                    limit 250";
    }else{
        var sql = "select a.*,b.NAME as 'APPROVAL2_NAME',c.NAME as 'CATALOGUER_NAME',d.DEPARTEMENT_DESC from tx_approval_server a \
                    LEFT JOIN tm_user b ON a.APPROVAL2=b.USER_ID \
                    LEFT JOIN tm_user c ON a.CATALOGUER=c.USER_ID \
                    LEFT JOIN tm_departement d ON b.DEPARTEMENT_ID=d.DEPARTEMENT_ID \
                    WHERE b.NAME LIKE '%"+param+"%' OR c.NAME LIKE '%"+param+"%' \
                    GROUP BY a.SERVER,a.APPROVAL2 \
                    limit 200";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});





router.use('/add_departement_role', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var departement_id = req.body.departement_id;
    var user_id = req.body.user_id;
    var id = req.body.id;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{
        if(id==""){
            var sql = "INSERT INTO tx_departement_role (DEPARTEMENT_ID,USER_ID) VALUES ('"+departement_id+"','"+user_id+"')";
        
        }else{
            var sql = "UPDATE tx_departement_role SET DEPARTEMENT_ID='"+departement_id+"',USER_ID='"+user_id+"' \
                        WHERE ID='"+id+"'";
        }
        
        var check_query= "SELECT * FROM tx_departement_role WHERE DEPARTEMENT_ID='"+departement_id+"' and USER_ID='"+user_id+"'";

        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                
                if(id==""){
                    connection.query(check_query,function(err,rows,fields){
                        if(err){
                            res.send("Error check departement");
                        }else{
                            if(rows==''){
                                connection.query(sql,function(err,rows,fields){
                                    if(err){
                                        res.send("Error Query Departement");
                                    }else{
                                        res.send(true);
                                    }
                                })
                            }else{
                                res.send("Data Available");
                            }
                        }
                    });
                }else{

                    connection.query(sql,function(err,rows,fields){
                        if(err){
                            res.send("Error Query Departement");
                        }else{
                            res.send(true);
                        }
                    });

                }
            }
            connection.end();
        })
    }
});


router.use('/add_server_role', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server = req.body.server;
    var approval2 = req.body.approval2;
    var cataloguer = req.body.cataloguer;
    var id = req.body.id;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{
        if(id==""){
            var sql = "INSERT INTO tx_approval_server (SERVER,APPROVAL2,CATALOGUER) \
                        VALUES ('"+server+"','"+approval2+"','"+cataloguer+"')";
        
        }else{
            var sql = "UPDATE tx_approval_server SET \
                        SERVER='"+server+"',APPROVAL2='"+approval2+"',CATALOGUER='"+cataloguer+"' \
                        WHERE ID='"+id+"'";
        }
        
        var check_query= "SELECT * FROM tx_approval_server WHERE SERVER='"+server+"' and APPROVAL2='"+approval2+"'";

        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{

                if(id==""){
                    connection.query(check_query,function(err,rows,fields){
                        if(err){
                            res.send("Error check server");
                        }else{
                            if(rows==''){
                                connection.query(sql,function(err,rows,fields){
                                    if(err){
                                        res.send("Error Query Server Role");
                                    }else{
                                        
                                        var update_server = "UPDATE tm_user SET SERVER_CODE='"+server+"' WHERE USER_ID='"+approval2+"'";
                                        connection.query(update_server,function(err,rows,fields){
                                           console.log("true");                                      
                                        });
    
                                        res.send(true);
                                    }
                                })
                            }else{
                                res.send("Data Available");
                            }
                        }
                    })
                }else{
                    connection.query(sql,function(err,rows,fields){
                        if(err){
                            res.send("Error Query Server Role");
                        }else{
                            
                            var update_server = "UPDATE tm_user SET SERVER_CODE='"+server+"' WHERE USER_ID='"+approval2+"'";
                            connection.query(update_server,function(err,rows,fields){
                               console.log("true");                                      
                            });

                            res.send(true);
                        }
                    })
                }
                                 
            }
            connection.end();
        })
    }
});


router.use('/add_user', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var username = req.body.username;
    var fullname = req.body.fullname;
    var email = req.body.email;
    var departement_id = req.body.departement_id;
    var role_id = req.body.role;
    var server = req.body.server;
    var password = 'pass123ABC';
    var id = req.body.id;
    var status = 'Y';

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{
        if(id==""){
            var sql = "INSERT INTO tm_user (USER_NAME,NAME,EMAIL,DEPARTEMENT_ID,ROLE_ID,SERVER_ROLE,PASSWORD,STATUS) \
                        VALUES ('"+username+"','"+fullname+"','"+email+"','"+departement_id+"','"+role_id+"','"+server+"',MD5('"+password+"'),'"+status+"')";
        
        }else{
            var sql = "UPDATE tm_user SET \
                        USER_NAME='"+username+"',NAME='"+fullname+"',EMAIL='"+email+"', \
                        DEPARTEMENT_ID='"+departement_id+"',ROLE_ID='"+role_id+"',SERVER_ROLE='"+server+"' \
                        WHERE USER_ID='"+id+"'";
        }
        
        var check_query= "SELECT * FROM tm_user WHERE ROLE_ID='NNN'";

        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(check_query,function(err,rows,fields){
                    if(err){
                        res.send("Error check user");
                    }else{
                        if(rows==''){
                            connection.query(sql,function(err,rows,fields){
                                if(err){
                                    res.send("Error Query User");
                                }else{
                                    res.send(true);
                                }
                            })
                        }else{
                            res.send("Data Available");
                        }
                    }
                })                 
            }
            connection.end();
        })
    }
});



router.use('/reset_password', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var password = 'pass123ABC';
    var id = req.body.id;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{
        
        var sql = "UPDATE tm_user SET PASSWORD=MD5('"+password+"') WHERE USER_ID='"+id+"'";
        
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                    
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Error Query User");
                    }else{
                        res.send("Password succesfully reset");
                    }
                });
                            
            }
            connection.end();
        })
    }
});


router.use('/log', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var request_no = req.body.request_no;

    var sql = "select \
               DATE_FORMAT(a.SUBMIT_DATE,'%d %M %Y, %H:%i') AS SUBMIT_DATE, \
               DATE_FORMAT(a.APPROVAL1_DATE,'%d %M %Y, %H:%i') AS APPROVAL1_DATE, \
               DATE_FORMAT(a.APPROVAL2_DATE,'%d %M %Y, %H:%i') AS APPROVAL2_DATE, \
               DATE_FORMAT(a.CATALOGUER_DATE,'%d %M %Y, %H:%i') AS CATALOGUER_DATE, \
               DATE_FORMAT(a.REGISTER_SAP_DATE,'%d %M %Y, %H:%i') AS REGISTER_SAP_DATE, \
               b.NAME AS 'USER_APPROVAL1',b.DEPARTEMENT_ID AS 'DEPARTEMENT_APPROVAL1',b.ROLE_ID AS 'ROLE_APPROVAL1',\
               c.NAME AS 'USER_APPROVAL2',c.DEPARTEMENT_ID AS 'DEPARTEMENT_APPROVAL2',c.ROLE_ID AS 'ROLE_APPROVAL2',\
               d.NAME AS 'USER_CATALOGUER',d.DEPARTEMENT_ID AS 'DEPARTEMENT_CATALOGUER',d.ROLE_ID AS 'ROLE_CATALOGUER' \
               FROM tm_item_registration a \
               LEFT JOIN tm_user b ON a.USER_APPROVAL1=b.USER_ID \
               LEFT JOIN tm_user c ON a.USER_APPROVAL2=c.USER_ID \
               LEFT JOIN tm_user d ON a.USER_CATALOGUER=d.USER_ID \
               WHERE a.REQUEST_NO='"+request_no+"'";
    
    pool.getConnection(function(err, connection){
        if(err){
            connection.release();
            res.send("Error Connection");
        }else{
            connection.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/add_server', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server_code = req.body.server_code;
    var server_name = req.body.server_name;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "INSERT INTO tm_server VALUES ('"+server_code+"','"+server_name+"')";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/update_server', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server_code = req.body.server_code;
    var server_name = req.body.server_name;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "UPDATE tm_server SET SERVER_NAME='"+server_name+"' WHERE SERVER_CODE='"+server_code+"'";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/add_company', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var company_id = req.body.company_id;
    var company_name = req.body.company_name;
    var shortname = req.body.shortname;
    var city = req.body.city;
    var server_code = req.body.server;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "INSERT INTO tm_company (COMPANY_ID,COMPANY_NAME,SHORTNAME,CITY,SERVER_CODE) VALUES ('"+company_id+"','"+company_name+"','"+shortname+"','"+city+"','"+server_code+"')";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});



router.use('/update_company', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var company_id = req.body.company_id;
    var company_name = req.body.company_name;
    var city = req.body.city;
    var shortname = req.body.shortname;
    var server_code = req.body.server;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "UPDATE tm_company SET COMPANY_NAME='"+company_name+"',SHORTNAME='"+shortname+"',CITY='"+city+"',SERVER_CODE='"+server_code+"' WHERE COMPANY_ID='"+company_id+"'";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/add_plant', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var plant_code = req.body.plant_id;
    var plant_name = req.body.plant_name;
    var company_id = req.body.company_id;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "INSERT INTO tm_plant (PLANT_CODE,PLANT_NAME,COMPANY_ID) VALUES ('"+plant_code+"','"+plant_name+"','"+company_id+"')";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/update_plant', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var plant_code = req.body.plant_id;
    var plant_name = req.body.plant_name;
    var company_id = req.body.company_id;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "UPDATE tm_plant SET PLANT_NAME='"+plant_name+"',COMPANY_ID='"+company_id+"' WHERE PLANT_CODE='"+plant_code+"'";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/add_storage_location', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var storage_location_id = req.body.storage_location_id;
    var storage_location_name = req.body.storage_location_name;
    var plant_code = req.body.plant_code;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "INSERT INTO tm_storage_location (STORAGE_LOCATION_ID,STORAGE_LOCATION_NAME,PLANT_CODE) \
                   VALUES ('"+storage_location_id+"','"+storage_location_name+"','"+plant_code+"')";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/update_storage_location', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var id = req.body.id;
    var storage_location_id = req.body.storage_location_id;
    var storage_location_name = req.body.storage_location_name;
    var plant_code = req.body.plant_code;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "UPDATE tm_storage_location \
                    SET STORAGE_LOCATION_ID='"+storage_location_id+"',STORAGE_LOCATION_NAME='"+storage_location_name+"',PLANT_CODE='"+plant_code+"' \
                    WHERE ID='"+id+"'";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/add_uom', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var uom_code = req.body.uom_code;
    var uom_name = req.body.uom_name;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "INSERT INTO tm_uom (UOM_CODE,UOM_NAME) VALUES ('"+uom_code+"','"+uom_name+"')";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/update_uom', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var uom_code = req.body.uom_code;
    var uom_name = req.body.uom_name;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "UPDATE tm_uom SET UOM_NAME='"+uom_name+"' WHERE UOM_CODE='"+uom_code+"'";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/add_valuation_class', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var valuation_class_id = req.body.valuation_class_id;
    var valuation_class_name = req.body.valuation_class_name;
    var material_type = req.body.material_type;
    var server = req.body.server;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "INSERT INTO tm_valuation_class (VALUATION_CLASS_ID,VALUATION_CLASS_NAME,MATERIAL_TYPE_CODE,SERVER_CODE) \
                    VALUES ('"+valuation_class_id+"','"+valuation_class_name+"','"+material_type+"','"+server+"')";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});



router.use('/update_valuation_class', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var id = req.body.id;
    var valuation_class_id = req.body.valuation_class_id;
    var valuation_class_name = req.body.valuation_class_name;
    var material_type = req.body.material_type;
    var server = req.body.server;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "UPDATE tm_valuation_class \
                    SET VALUATION_CLASS_NAME='"+valuation_class_name+"',\
                    MATERIAL_TYPE_CODE='"+material_type+"',\
                    SERVER_CODE='"+server+"' \
                    WHERE ID='"+id+"'";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});




router.use('/add_price_control', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var price_control_id = req.body.price_control_id;
    var price_control_desc = req.body.price_control_desc;
    var material_type = req.body.material_type;
    var server = req.body.server_code;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "INSERT INTO tm_price_control (PRICE_CONTROL_ID,PRICE_CONTROL_DESC,MATERIAL_TYPE_CODE,SERVER_CODE) \
                    VALUES ('"+price_control_id+"','"+price_control_desc+"','"+material_type+"','"+server+"')";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});



router.use('/update_price_control', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var id = req.body.id;
    var price_control_id = req.body.price_control_id;
    var price_control_desc = req.body.price_control_desc;
    var material_type = req.body.material_type;
    var server = req.body.server_code;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "UPDATE tm_price_control \
                    SET PRICE_CONTROL_ID='"+price_control_id+"',PRICE_CONTROL_DESC='"+price_control_desc+"',\
                    MATERIAL_TYPE_CODE='"+material_type+"',\
                    SERVER_CODE='"+server+"' \
                    WHERE ID='"+id+"'";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/add_lab_office', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var lab_office = req.body.lab_office;
    var external_group = req.body.external_group;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "INSERT INTO tm_lab_office (MATERIAL_GROUP,LAB_OFFICE) VALUES ('"+external_group+"','"+lab_office+"')";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Error Query");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/update_lab_office', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var lab_office = req.body.lab_office;
    var external_group = req.body.external_group;
    var id = req.body.id;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "UPDATE tm_lab_office SET LAB_OFFICE='"+lab_office+"',MATERIAL_GROUP='"+external_group+"' \
                    WHERE ID='"+id+"'";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/add_material_type', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var material_type_code = req.body.material_type_code;
    var material_type_name = req.body.material_type_name;
    var server_code = req.body.server;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "INSERT INTO tm_material_type (MATERIAL_TYPE_CODE,MATERIAL_TYPE_NAME,SERVER_CODE) VALUES ('"+material_type_code+"','"+material_type_name+"','"+server_code+"')";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/update_material_type', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var material_type_code = req.body.material_type_code;
    var material_type_name = req.body.material_type_name;
    var id = req.body.id;
    var server_code = req.body.server;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "UPDATE tm_material_type SET MATERIAL_TYPE_CODE='"+material_type_code+"',\
                    MATERIAL_TYPE_NAME='"+material_type_name+"',\
                    SERVER_CODE='"+server_code+"' WHERE ID='"+id+"'";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});



router.use('/add_material_group', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var material_group_id = req.body.material_group_id;
    var material_group_name = req.body.material_group_name;
    var material_group_desc = req.body.material_group_desc;
    var server_code = req.body.server;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "INSERT INTO tm_material_group (MATERIAL_GROUP_ID,MATERIAL_GROUP_NAME,MATERIAL_GROUP_DESC,SERVER_CODE)\
                    VALUES ('"+material_group_id+"','"+material_group_name+"','"+material_group_desc+"','"+server_code+"')";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/update_material_group', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var id = req.body.id;
    var material_group_id = req.body.material_group_id;
    var material_group_name = req.body.material_group_name;
    var material_group_desc = req.body.material_group_desc;
    var server_code = req.body.server;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "UPDATE tm_material_group SET MATERIAL_GROUP_NAME='"+material_group_name+"',\
                    MATERIAL_GROUP_ID='"+material_group_id+"',MATERIAL_GROUP_DESC='"+material_group_desc+"',\
                    SERVER_CODE='"+server_code+"' WHERE ID='"+id+"'";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});



router.use('/add_external_group', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var external_group_code = req.body.external_group_code;
    var external_group_name = req.body.external_group_name;
    var material_group_code = req.body.material_group_code;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "INSERT INTO tm_external_group (EXTERNAL_GROUP_CODE,EXTERNAL_GROUP_NAME,MATERIAL_GROUP_CODE) VALUES ('"+external_group_code+"','"+external_group_name+"','"+material_group_code+"')";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/update_external_group', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var external_group_code = req.body.external_group_code;
    var external_group_name = req.body.external_group_name;
    var material_group_code = req.body.material_group_code;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "UPDATE tm_external_group SET EXTERNAL_GROUP_NAME='"+external_group_name+"',\
                    MATERIAL_GROUP_CODE='"+material_group_code+"' \
                    WHERE EXTERNAL_GROUP_CODE='"+external_group_code+"'";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});



router.use('/add_inc', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var inc_code = req.body.inc_code;
    var inc_name = req.body.inc_name;
    var short_name = req.body.short_name;
    var colloquial_name = req.body.colloquial_name;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "INSERT INTO tm_inc (INC_CODE,INC_NAME,SHORT_NAME,COLLOQUIAL_NAME) VALUES ('"+inc_code+"','"+inc_name+"','"+short_name+"','"+colloquial_name+"')";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/update_inc', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var inc_code = req.body.inc_code;
    var inc_name = req.body.inc_name;
    var short_name = req.body.short_name;
    var colloquial_name = req.body.colloquial_name;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "UPDATE tm_inc SET INC_NAME='"+inc_name+"',\
                    SHORT_NAME='"+short_name+"',\
                    COLLOQUIAL_NAME='"+colloquial_name+"'\
                    WHERE INC_CODE='"+inc_code+"'";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});



router.use('/add_inc_attribute', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var attribute_code = req.body.attribute_code;
    var attribute_desc = req.body.attribute_desc;
    var explanation = req.body.explanation;
    var sequence = req.body.sequence;
    var inc_code = req.body.inc_code;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "INSERT INTO tm_inc_attribute (ATTRIBUTE_CODE,ATTRIBUTE_DESC,EXPLANATION,SEQUENCE,INC_CODE) \
                    VALUES ('"+attribute_code+"','"+attribute_desc+"','"+explanation+"','"+sequence+"','"+inc_code+"')";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Data Available");
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/update_inc_attribute', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var attribute_id = req.body.attribute_id;
    var attribute_code = req.body.attribute_code;
    var attribute_desc = req.body.attribute_desc;
    var explanation = req.body.explanation;
    var sequence = req.body.sequence;
    var inc_code = req.body.inc_code;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "UPDATE tm_inc_attribute SET ATTRIBUTE_CODE='"+attribute_code+"',\
                    ATTRIBUTE_DESC='"+attribute_desc+"',\
                    EXPLANATION='"+explanation+"',\
                    SEQUENCE='"+sequence+"',\
                    INC_CODE='"+inc_code+"'\
                    WHERE ATTRIBUTE_ID='"+attribute_id+"'";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send(sql);
                    }else{
                        res.send(true)
                    }
                })                 
            }
            connection.end();
        })
    }
});


router.use('/add_departement', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var departement_id = req.body.departement_id;
    var departement_name = req.body.departement_name;
    var type = req.body.type;
    var id = req.body.id;

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{
        if(type=="INSERT"){
            var sql = "INSERT INTO tm_departement (DEPARTEMENT_ID,DEPARTEMENT_DESC) VALUES ('"+departement_id+"','"+departement_name+"')";
        }else if(type="UPDATE"){
            var sql = "UPDATE tm_departement SET DEPARTEMENT_ID='"+departement_id+"',DEPARTEMENT_DESC='"+departement_name+"' \
                        WHERE ID='"+id+"'";
        }
        
        var check_query= "SELECT * FROM tm_departement WHERE DEPARTEMENT_ID='"+departement_id+"'";

        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{

                if(type=="INSERT"){
                    connection.query(check_query,function(err,rows,fields){
                        if(err){
                            res.send("Error check departement");
                        }else{
                            if(rows==''){
                                connection.query(sql,function(err,rows,fields){
                                    if(err){
                                        res.send("Error Query Departement");
                                    }else{
                                        res.send(true);
                                    }
                                })
                            }else{
                                res.send("Data Available");
                            }
                        }
                    })
                }else{
                    connection.query(sql,function(err,rows,fields){
                        if(err){
                            res.send("Error Query Departement");
                        }else{
                            res.send(true);
                        }
                    })
                }
                                 
            }
            connection.end();
        })
    }
});

router.use('/change_status', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var id = req.body.id;
    var status = req.body.status;
    var table = req.body.table;
    var field = req.body.field;
    

    if(status=='Y'){
        var message = "Data Successfully Reactivated";
    }else if(status=='N'){
        var message = "Data Successfully Deactivated";
    }

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "UPDATE "+table+" SET STATUS='"+status+"' WHERE "+field+"='"+id+"'";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Change Status Failed");
                    }else{
                        res.send(message);
                    }
                })                 
            }
            connection.end();
        })
    }
});

router.use('/delete', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var id = req.body.id;
    var table = req.body.table;
    var field = req.body.field;
    

    var message = "Data Successfully Deleted";
    

    if(req.method=='OPTIONS'){
        res.send(200);

    }else{

        var sql = "DELETE FROM "+table+"  WHERE "+field+"='"+id+"'";
                
        pool.getConnection(function(err, connection){ 
            if(err){
                connection.release();
                res.send("Error Connection");
            }else{
                connection.query(sql,function(err,rows,fields){
                    if(err){
                        res.send("Delete Data Failed");
                    }else{
                        res.send(message);
                    }
                })                 
            }
            connection.end();
        })
    }
});

module.exports = router;

