var express = require('express');
var router = express.Router();
var pool = require('./db_connection');
const jwt = require('jsonwebtoken');
var mysql = require('mysql');
var cors = require('cors')
var myobj = {};


router.get('/dropselect/:param/:key', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

        var param  = req.params.param;
        var key    = req.params.key;

        var sql = "SELECT * FROM " + param +" WHERE STATUS='Y' ORDER BY "+ key;
        pool.getConnection(function(err, connection){
            if(err){
               
                res.send("Error Connection");
            }else{
                pool.query(sql,function(err,rows,fields){
                    res.send(rows);
                }) 
            }
            connection.end();
        })
});

router.get('/dropdynamic/:param/:key/:id', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

        var param  = req.params.param;
        var key  = req.params.key;
        var id  = req.params.id;
         
        var sql = "SELECT * FROM " + param + " WHERE "+ key +"='" +id+"'";
        pool.getConnection(function(err, connection){
            if(err){
               
                res.send("Error Connection");
            }else{
                pool.query(sql,function(err,rows,fields){
                    res.send(rows);
                }) 
            }
            connection.end();
        })
});


router.use('/dropserver', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

     var param  = req.body.param;
     var key  = req.body.key;
     var id  = req.body.id;
     var server = req.body.server;
    
    var sql = "SELECT * FROM " + param + " WHERE STATUS='Y' AND "+ key +"='" +id+"' AND SERVER_CODE='"+server+"'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/material_group/:server', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server = req.params.server;

    var sql = "SELECT MATERIAL_GROUP_ID,CONCAT(MATERIAL_GROUP_ID,' - ',MATERIAL_GROUP_NAME) AS MATERIAL_GROUP_NAME FROM tm_material_group WHERE STATUS='Y' AND SERVER_CODE='"+server+"'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            });
        }
        connection.end();
    })
});


router.get('/external_group/:id', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var id = req.params.id;

    var sql = "SELECT EXTERNAL_GROUP_CODE,CONCAT(EXTERNAL_GROUP_CODE,' - ',EXTERNAL_GROUP_NAME) AS EXTERNAL_GROUP_NAME FROM tm_external_group WHERE STATUS='Y' AND MATERIAL_GROUP_CODE='"+id+"'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});

router.get('/plant/:id', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var id = req.params.id;

    var sql = "SELECT PLANT_CODE,CONCAT(PLANT_CODE,' - ',PLANT_NAME) AS PLANT_NAME FROM tm_plant WHERE STATUS='Y' AND COMPANY_ID='"+id+"'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});

router.get('/company/:id', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);
    var id = req.params.id;

    var sql = "SELECT * FROM tm_company WHERE STATUS='Y' AND SERVER_CODE='"+id+"'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/storage_location/:id', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);
    var id = req.params.id;

    var sql = "SELECT * FROM tm_storage_location WHERE STATUS='Y' AND PLANT_CODE='"+id+"'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});



router.get('/material_type/:id', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var id = req.params.id;

    var sql = "SELECT MATERIAL_TYPE_CODE,CONCAT(MATERIAL_TYPE_CODE,' - ',MATERIAL_TYPE_NAME) AS MATERIAL_TYPE_NAME FROM tm_material_type WHERE STATUS='Y' AND SERVER_CODE='"+id+"'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});

router.get('/uom', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT UOM_CODE,CONCAT(UOM_CODE,' - ',UOM_NAME) AS UOM_NAME FROM tm_uom WHERE STATUS='Y'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});

router.get('/departement', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT DEPARTEMENT_ID,CONCAT(DEPARTEMENT_ID,' - ',DEPARTEMENT_DESC) AS DEPARTEMENT_NAME FROM tm_departement WHERE STATUS='Y'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/user/:departement', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    departement = req.params.departement;

    var sql = "SELECT USER_ID,NAME FROM tm_user WHERE STATUS='Y' AND DEPARTEMENT_ID='"+departement+"' and ROLE_ID='APP1' ORDER BY NAME ASC"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});




router.get('/requester', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT USER_ID,CONCAT(NAME,' - ',DEPARTEMENT_ID) AS NAME FROM tm_user WHERE STATUS='Y' AND ROLE_ID='REQ' ORDER BY NAME ASC"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});



router.get('/approval1', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT USER_ID,CONCAT(NAME,' - ',DEPARTEMENT_ID) AS NAME FROM tm_user WHERE STATUS='Y' AND ROLE_ID='APP1' ORDER BY NAME ASC"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/approval2', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT USER_ID,NAME FROM tm_user WHERE STATUS='Y' AND ROLE_ID='APP2' ORDER BY NAME ASC"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/cataloguer', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT USER_ID,NAME FROM tm_user WHERE STATUS='Y' AND ROLE_ID='CAT' ORDER BY NAME ASC"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/server', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT * FROM tm_server WHERE STATUS='Y'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});

router.get('/role', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT * FROM tm_role WHERE STATUS='Y'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/price_control/:server/:material_type', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server = req.params.server;
    var material_type = req.params.material_type;

    var sql = "SELECT PRICE_CONTROL_ID,CONCAT(PRICE_CONTROL_ID,' - ',PRICE_CONTROL_DESC) AS PRICE_CONTROL_DESC \
               FROM tm_price_control WHERE STATUS='Y' AND SERVER_CODE='"+server+"' AND MATERIAL_TYPE_CODE='"+material_type+"'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});

router.get('/valuation_class/:server/:material_type', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server = req.params.server;
    var material_type = req.params.material_type;

    var sql = "SELECT VALUATION_CLASS_ID,CONCAT(VALUATION_CLASS_ID,' - ',VALUATION_CLASS_NAME) AS VALUATION_CLASS_NAME \
               FROM tm_valuation_class WHERE STATUS='Y' AND SERVER_CODE='"+server+"' AND MATERIAL_TYPE_CODE='"+material_type+"'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/profit_center/:server/:material_type', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server = req.params.server;
    var material_type = req.params.material_type;

    var sql = "SELECT PROFIT_CENTER_CODE,CONCAT(PROFIT_CENTER_CODE,' - ',PROFIT_CENTER_DESC) AS PROFIT_CENTER_DESC \
               FROM tm_profit_center WHERE STATUS='Y' AND SERVER_CODE='"+server+"' AND MATERIAL_TYPE='"+material_type+"'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/lab_office/:material_group', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var material_group = req.params.material_group;

    var sql = "SELECT LAB_OFFICE FROM tm_lab_office WHERE STATUS='Y' AND MATERIAL_GROUP='"+material_group+"'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});

router.get('/plant_detail/:material_number', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

        var material_number  = req.params.material_number;
        var sql = "SELECT a.PLANT,a.COMPANY_ID,b.PLANT_NAME,c.COMPANY_NAME FROM tm_initial_item a \
                    LEFT JOIN tm_plant b ON a.PLANT=b.PLANT_CODE \
                    LEFT JOIN tm_company c ON a.COMPANY_ID=c.COMPANY_ID \
                    WHERE a.MATERIAL_NUMBER='"+material_number+"' \
                    GROUP BY a.PLANT ORDER BY c.COMPANY_NAME ASC \
                   ";
        
        pool.getConnection(function(err, connection){
            if(err){
               res.send("Error Connection");
            }else{
                pool.query(sql,function(err,rows,fields){
                    res.send(rows);
                }) 
            }
            connection.end();
        })
});


router.get('/server_setting/:id', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var id = req.params.id;
    var sql = "SELECT SERVER_ROLE FROM tm_user WHERE USER_ID='"+id+"'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/company', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT * FROM tm_company WHERE STATUS='Y'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});

router.get('/plant', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT PLANT_CODE,CONCAT(PLANT_CODE,' - ',PLANT_NAME) as PLANT_NAME FROM tm_plant WHERE STATUS='Y'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/material_type', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT MATERIAL_TYPE_CODE,CONCAT(MATERIAL_TYPE_CODE,' - ',MATERIAL_TYPE_NAME) AS MATERIAL_TYPE_NAME FROM tm_material_type WHERE STATUS='Y'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/material_group', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT MATERIAL_GROUP_ID,CONCAT(MATERIAL_GROUP_ID,' - ',MATERIAL_GROUP_NAME) as MATERIAL_GROUP_NAME FROM tm_material_group WHERE STATUS='Y'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/external_group', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT EXTERNAL_GROUP_CODE,CONCAT(EXTERNAL_GROUP_CODE,' - ',EXTERNAL_GROUP_NAME) as EXTERNAL_GROUP_NAME FROM tm_external_group WHERE STATUS='Y'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});

router.get('/inc', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT INC_CODE,CONCAT(INC_CODE,' - ',INC_NAME) as INC_NAME FROM tm_inc WHERE STATUS='Y'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/inc_attribute/:id', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var id = req.params.id;
    var sql = "SELECT * FROM tm_inc_attribute WHERE STATUS='Y' AND INC_CODE='"+id+"' ORDER BY SEQUENCE ASC"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/company_dashboard/:id', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);
    var id = req.params.id;

    var sql = "SELECT CODE AS COMPANY_ID,NAME AS COMPANY_NAME FROM tm_null WHERE TYPE='company' \
               UNION \
               SELECT COMPANY_ID,COMPANY_NAME FROM tm_company WHERE STATUS='Y' AND SERVER_CODE='"+id+"'";
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/company_dashboard_all', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*'); 
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);
    
    var sql = "SELECT CODE AS COMPANY_ID,NAME AS COMPANY_NAME FROM tm_null WHERE TYPE='company' \
               UNION \
               SELECT COMPANY_ID,COMPANY_NAME FROM tm_company WHERE STATUS='Y'";
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});

router.get('/server_dashboard', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT CODE AS SERVER_CODE,NAME AS SERVER_NAME FROM tm_null WHERE TYPE='server' \
               UNION \
               SELECT SERVER_CODE,SERVER_NAME FROM tm_server WHERE STATUS='Y'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/departement_dashboard', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT CODE AS DEPARTEMENT_ID,NAME AS DEPARTEMENT_NAME FROM tm_null WHERE TYPE='departement' \
               UNION \
               SELECT DEPARTEMENT_ID,CONCAT(DEPARTEMENT_ID,' - ',DEPARTEMENT_DESC) \
               AS DEPARTEMENT_NAME FROM tm_departement WHERE STATUS='Y'"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.get('/item_status', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var sql = "SELECT CODE AS STATUS_ID,NAME AS STATUS_DESC FROM tm_null WHERE TYPE='status' \
               UNION \
               (SELECT STATUS_ID,STATUS_DESC FROM tm_status ORDER BY SEQUENCE ASC)"; 
    
    pool.getConnection(function(err, connection){
        if(err){
           
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});

module.exports = router;

