var express = require('express');
var router = express.Router();
var pool = require('./db_connection');
const jwt = require('jsonwebtoken');
var mysql = require('mysql');
var cors = require('cors')
var myobj = {};


router.use('/initial_material',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;
    var server = req.body.server;
    var additional = req.body.additional;
    
    if(server==''){
        where = ""; 
    }else{
        where = " and a.SERVER_CODE in ("+server+")";
    }

    var query = "select a.SERVER_CODE,a.MATERIAL_NUMBER,a.INC_CODE,a.INC_NAME,a.DESCRIPTION,a.COLLAQUAL_NAME,a.ADDITIONAL_INFORMATION, \
                    a.BRAND,PART_NO, \
                    GROUP_CONCAT(DISTINCT(b.COMPANY_NAME) ORDER BY b.COMPANY_NAME SEPARATOR ', ') AS COMPANY \
                    from tm_initial_item a \
                    left join tm_company b ON a.COMPANY_ID=b.COMPANY_ID \
                    WHERE (a.DESCRIPTION like '%"+ param +"%' \
                    or a.COLLAQUAL_NAME like '%"+ param +"%' \
                    or a.ADDITIONAL_INFORMATION like '%"+ param +"%' \
                    or a.BRAND like '%"+ param +"%' \
                    or a.PART_NO like '%"+ param +"%' \
                    or a.MATERIAL_NUMBER like '%"+ param +"%') \
                    "+where+"\
                    GROUP BY a.MATERIAL_NUMBER \
                    LIMIT 2000 \
                ";

    pool.getConnection(function(err,connection){
        if(err){
            res.send('Connection Error')
        }else{
            pool.query(query,function(err,rows,fields){
                res.send(rows);
            })
        }
        connection.end();
    })
});


router.use('/initial_material_partno',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true)

    var param = req.body.param;
    var server = req.body.server;

    if(server==''){
        where = ""; 
    }else{
        where = " and a.SERVER_CODE in ("+server+")";
    }

    var query = "select a.SERVER_CODE,a.MATERIAL_NUMBER,a.INC_CODE,a.INC_NAME,a.DESCRIPTION,a.COLLAQUAL_NAME,a.ADDITIONAL_INFORMATION, \
                    a.BRAND,PART_NO, \
                    GROUP_CONCAT(DISTINCT(b.COMPANY_NAME) ORDER BY b.COMPANY_NAME SEPARATOR ', ') AS COMPANY \
                    from tm_initial_item a \
                    left join tm_company b ON a.COMPANY_ID=b.COMPANY_ID \
                    WHERE a.PART_NO LIKE '%"+param+"%' \
                    "+where+"\
                    GROUP BY a.MATERIAL_NUMBER \
                    LIMIT 2000 \
                ";

    pool.getConnection(function(err,connection){
        if(err){
            res.send('Connection Error')
        }else{
            pool.query(query,function(err,rows,fields){
                res.send(rows);
            })
        }
        connection.end();
    })
});



router.use('/inc', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;
    if(param==''){
        var sql = "select * from tm_inc WHERE STATUS='Y' limit 250";
    }else{
        var sql = "select * from tm_inc where (INC_CODE like '%"+param+"%' \
               or INC_NAME like '%"+param+"%' or COLLOQUIAL_NAME like '%"+param+"%') and STATUS='Y' \
               limit 250";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});



router.use('/item_registration', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;
    var user_id = req.body.user_id;
    var status = req.body.status;
    var sqlstatus = ""; 
    
    var filter_server = req.body.filter_server;
    var filter_status = req.body.filter_status;
    var filter_startdate = req.body.filter_startdate;
    var filter_enddate = req.body.filter_enddate; 

    var where = "";
    if(filter_server){
        where += " AND a.SERVER_CODE='"+filter_server+"'";
    }

    if(filter_status){
        where += " AND a.ITEM_STATUS='"+filter_status+"'";
    }

    if(filter_startdate && filter_enddate){
        where += " AND a.REQUEST_DATE BETWEEN '"+filter_startdate+"' AND '"+filter_enddate+"'";
    }

    if(filter_startdate && !filter_enddate){
        where += " AND a.REQUEST_DATE='"+filter_startdate+"'";
    }

    if(!filter_startdate && filter_enddate){
        where += " AND a.REQUEST_DATE='"+filter_enddate+"'";
    }

    if(param=='' && where==''){
        var sql = "select a.*,CONCAT(b.PLANT_CODE,' - ',b.PLANT_NAME) as PLANT_NAME,c.UOM_NAME,d.STORAGE_LOCATION_NAME, \
                    DATE_FORMAT(a.SUBMIT_DATE, '%d-%m-%Y') as DATE, \
                    DATE_FORMAT(a.REQUEST_DATE, '%d-%m-%Y') as REQUEST_DATE, \
                    e.MATERIAL_TYPE_NAME,f.MATERIAL_GROUP_NAME,g.EXTERNAL_GROUP_NAME\
                    from tm_item_registration a \
                    left join tm_plant b ON a.PLANT=b.PLANT_CODE \
                    left join tm_uom c ON a.BASE_UOM=c.UOM_CODE \
                    left join tm_storage_location d ON a.STORAGE_LOCATION=d.STORAGE_LOCATION_ID and d.PLANT_CODE=a.PLANT \
                    left join tm_material_type e ON a.MATERIAL_TYPE=e.MATERIAL_TYPE_CODE and e.SERVER_CODE=a.SERVER_CODE  \
                    left join tm_material_group f ON a.MATERIAL_GROUP=f.MATERIAL_GROUP_ID and f.SERVER_CODE=a.SERVER_CODE   \
                    left join tm_external_group g ON a.EXTERNAL_GROUP=g.EXTERNAL_GROUP_CODE and g.MATERIAL_GROUP_CODE=a.MATERIAL_GROUP \
                    WHERE a.USER_INPUT='"+user_id+"' "+sqlstatus+" GROUP BY a.REQUEST_NO ORDER BY a.UPDATE_DATE DESC limit 250";

    }else{
        var sql = "select a.*,CONCAT(b.PLANT_CODE,' - ',b.PLANT_NAME) as PLANT_NAME,c.UOM_NAME,d.STORAGE_LOCATION_NAME, \
                    DATE_FORMAT(a.SUBMIT_DATE, '%d-%m-%Y') as DATE, \
                    DATE_FORMAT(a.REQUEST_DATE, '%d-%m-%Y') as REQUEST_DATE, \
                    e.MATERIAL_TYPE_NAME,f.MATERIAL_GROUP_NAME,g.EXTERNAL_GROUP_NAME\
                    from tm_item_registration a \
                    left join tm_plant b ON a.PLANT=b.PLANT_CODE \
                    left join tm_uom c ON a.BASE_UOM=c.UOM_CODE \
                    left join tm_storage_location d ON a.STORAGE_LOCATION=d.STORAGE_LOCATION_ID and d.PLANT_CODE=a.PLANT \
                    left join tm_material_type e ON a.MATERIAL_TYPE=e.MATERIAL_TYPE_CODE and e.SERVER_CODE=a.SERVER_CODE  \
                    left join tm_material_group f ON a.MATERIAL_GROUP=f.MATERIAL_GROUP_ID and f.SERVER_CODE=a.SERVER_CODE   \
                    left join tm_external_group g ON a.EXTERNAL_GROUP=g.EXTERNAL_GROUP_CODE and g.MATERIAL_GROUP_CODE=a.MATERIAL_GROUP \
                    WHERE (INC_CODE like '%"+param+"%' \
                    or a.INC like '%"+param+"%' or a.COLLOQUIAL_NAME like '%"+param+"%' or  a.DESCRIPTION like '%"+param+"%' \
                    or a.PART_NO like '%"+param+"%' or a.ADDITIONAL_INFORMATION like '%"+param+"%' or a.REQUEST_CODE like '%"+ param +"%')\
                    "+ where +" AND a.USER_INPUT='"+user_id+"' GROUP BY a.REQUEST_NO ORDER BY a.UPDATE_DATE DESC LIMIT 250";
    }
    
    pool.getConnection(function(err, connection){
        if(err){
            
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/check_partnumber', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server = req.body.server;
    var part_number = req.body.part_number;

    var sql = "select PART_NO from tm_initial_item where SERVER_CODE='"+server+"' and PART_NO='"+part_number+"'";
    
    pool.getConnection(function(err, connection){
        if(err){
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});



router.use('/item_approval', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;
    var user_id = req.body.user_id;
    var status = req.body.status;
    var departement_id = req.body.departement_id;
    var server = req.body.server;

    var filter_server = req.body.filter_server;
    var filter_startdate = req.body.filter_startdate;
    var filter_enddate = req.body.filter_enddate; 

    var where = "";
    if(filter_server){
        where += " AND a.SERVER_CODE='"+filter_server+"'";
    }

    if(filter_startdate && filter_enddate){
        where += " AND a.REQUEST_DATE BETWEEN '"+filter_startdate+"' AND '"+filter_enddate+"'";
    }

    if(filter_startdate && !filter_enddate){
        where += " AND a.REQUEST_DATE='"+filter_startdate+"'";
    }

    if(!filter_startdate && filter_enddate){
        where += " AND a.REQUEST_DATE='"+filter_enddate+"'";
    }


    if(param=='' && where==''){
        if(status=='APP1'){
            var sql = "select a.*,CONCAT(b.PLANT_CODE,' - ',b.PLANT_NAME) as PLANT_NAME,c.UOM_NAME,d.STORAGE_LOCATION_NAME, \
                        DATE_FORMAT(a.SUBMIT_DATE, '%d-%m-%Y') as DATE, \
                        DATE_FORMAT(a.REQUEST_DATE, '%d-%m-%Y') as REQUEST_DATE, \
                        e.MATERIAL_TYPE_NAME,f.MATERIAL_GROUP_NAME,g.EXTERNAL_GROUP_NAME\
                        from tm_item_registration a \
                        left join tm_plant b ON a.PLANT=b.PLANT_CODE \
                        left join tm_uom c ON a.BASE_UOM=c.UOM_CODE \
                        left join tm_storage_location d ON a.STORAGE_LOCATION=d.STORAGE_LOCATION_ID and d.PLANT_CODE=a.PLANT \
                        left join tm_material_type e ON a.MATERIAL_TYPE=e.MATERIAL_TYPE_CODE and e.SERVER_CODE=a.SERVER_CODE  \
                        left join tm_material_group f ON a.MATERIAL_GROUP=f.MATERIAL_GROUP_ID and f.SERVER_CODE=a.SERVER_CODE   \
                        left join tm_external_group g ON a.EXTERNAL_GROUP=g.EXTERNAL_GROUP_CODE and g.MATERIAL_GROUP_CODE=a.MATERIAL_GROUP \
                        WHERE a.ITEM_STATUS='"+status+"' \
                        AND a.DEPARTEMENT_ID='"+departement_id+"' GROUP BY a.REQUEST_NO ORDER BY a.UPDATE_DATE DESC limit 250";
                   
        }else if(status=='APP2'){
            var sql = "select a.*,CONCAT(b.PLANT_CODE,' - ',b.PLANT_NAME) as PLANT_NAME,c.UOM_NAME,d.STORAGE_LOCATION_NAME, \
                        DATE_FORMAT(a.SUBMIT_DATE, '%d-%m-%Y') as DATE, \
                        DATE_FORMAT(a.REQUEST_DATE, '%d-%m-%Y') as REQUEST_DATE, \
                        e.MATERIAL_TYPE_NAME,f.MATERIAL_GROUP_NAME,g.EXTERNAL_GROUP_NAME\
                        from tm_item_registration a \
                        left join tm_plant b ON a.PLANT=b.PLANT_CODE \
                        left join tm_uom c ON a.BASE_UOM=c.UOM_CODE \
                        left join tm_storage_location d ON a.STORAGE_LOCATION=d.STORAGE_LOCATION_ID and d.PLANT_CODE=a.PLANT \
                        left join tm_material_type e ON a.MATERIAL_TYPE=e.MATERIAL_TYPE_CODE and e.SERVER_CODE=a.SERVER_CODE  \
                        left join tm_material_group f ON a.MATERIAL_GROUP=f.MATERIAL_GROUP_ID and f.SERVER_CODE=a.SERVER_CODE   \
                        left join tm_external_group g ON a.EXTERNAL_GROUP=g.EXTERNAL_GROUP_CODE and g.MATERIAL_GROUP_CODE=a.MATERIAL_GROUP \
                        WHERE a.ITEM_STATUS='"+status+"' \
                        AND a.SERVER_CODE='"+server+"' GROUP BY a.REQUEST_NO ORDER BY a.UPDATE_DATE DESC limit 250";
        }

    }else{
        if(status=='APP1'){
            var sql = "select a.*,CONCAT(b.PLANT_CODE,' - ',b.PLANT_NAME) as PLANT_NAME,c.UOM_NAME,d.STORAGE_LOCATION_NAME, \
                        DATE_FORMAT(a.SUBMIT_DATE, '%d-%m-%Y') as DATE, \
                        DATE_FORMAT(a.REQUEST_DATE, '%d-%m-%Y') as REQUEST_DATE, \
                        e.MATERIAL_TYPE_NAME,f.MATERIAL_GROUP_NAME,g.EXTERNAL_GROUP_NAME\
                        from tm_item_registration a \
                        left join tm_plant b ON a.PLANT=b.PLANT_CODE \
                        left join tm_uom c ON a.BASE_UOM=c.UOM_CODE \
                        left join tm_storage_location d ON a.STORAGE_LOCATION=d.STORAGE_LOCATION_ID and d.PLANT_CODE=a.PLANT \
                        left join tm_material_type e ON a.MATERIAL_TYPE=e.MATERIAL_TYPE_CODE and e.SERVER_CODE=a.SERVER_CODE  \
                        left join tm_material_group f ON a.MATERIAL_GROUP=f.MATERIAL_GROUP_ID and f.SERVER_CODE=a.SERVER_CODE   \
                        left join tm_external_group g ON a.EXTERNAL_GROUP=g.EXTERNAL_GROUP_CODE and g.MATERIAL_GROUP_CODE=a.MATERIAL_GROUP \
                        WHERE (INC_CODE like '%"+param+"%' \
                        or a.INC like '%"+param+"%' or a.COLLOQUIAL_NAME like '%"+param+"%' or a.DESCRIPTION like '%"+param+"%' \
                        or a.PART_NO like '%"+param+"%' or a.ADDITIONAL_INFORMATION like '%"+param+"%' or a.REQUEST_CODE like '%"+ param +"%')\
                        "+ where +" AND a.DEPARTEMENT_ID='"+departement_id+"' AND a.ITEM_STATUS='"+status+"' GROUP BY a.REQUEST_NO ORDER BY a.UPDATE_DATE DESC limit 250";

        }else if(status=='APP2'){
            var sql = "select a.*,CONCAT(b.PLANT_CODE,' - ',b.PLANT_NAME) as PLANT_NAME,c.UOM_NAME,d.STORAGE_LOCATION_NAME, \
                        DATE_FORMAT(a.SUBMIT_DATE, '%d-%m-%Y') as DATE, \
                        DATE_FORMAT(a.REQUEST_DATE, '%d-%m-%Y') as REQUEST_DATE, \
                        e.MATERIAL_TYPE_NAME,f.MATERIAL_GROUP_NAME,g.EXTERNAL_GROUP_NAME\
                        from tm_item_registration a \
                        left join tm_plant b ON a.PLANT=b.PLANT_CODE \
                        left join tm_uom c ON a.BASE_UOM=c.UOM_CODE \
                        left join tm_storage_location d ON a.STORAGE_LOCATION=d.STORAGE_LOCATION_ID and d.PLANT_CODE=a.PLANT \
                        left join tm_material_type e ON a.MATERIAL_TYPE=e.MATERIAL_TYPE_CODE and e.SERVER_CODE=a.SERVER_CODE  \
                        left join tm_material_group f ON a.MATERIAL_GROUP=f.MATERIAL_GROUP_ID and f.SERVER_CODE=a.SERVER_CODE   \
                        left join tm_external_group g ON a.EXTERNAL_GROUP=g.EXTERNAL_GROUP_CODE and g.MATERIAL_GROUP_CODE=a.MATERIAL_GROUP \
                        WHERE (INC_CODE like '%"+param+"%' \
                        or a.INC like '%"+param+"%' or a.COLLOQUIAL_NAME like '%"+param+"%' or a.DESCRIPTION like '%"+param+"%' \
                        or a.PART_NO like '%"+param+"%' or a.ADDITIONAL_INFORMATION like '%"+param+"%' or a.REQUEST_CODE like '%"+ param +"%')\
                        "+ where +" AND a.SERVER_CODE='"+server+"' AND a.ITEM_STATUS='"+status+"' GROUP BY a.REQUEST_NO ORDER BY a.UPDATE_DATE DESC limit 250";
        }
    }
            
    pool.getConnection(function(err, connection){
        if(err){
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});


router.use('/item_catalog', async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var param = req.body.param;
    var user_id = req.body.user_id;
    var status = req.body.status;
    var departement_id = req.body.departement_id;
    var server = req.body.server;


    var filter_server = req.body.filter_server;
    var filter_status = req.body.filter_status;
    var filter_startdate = req.body.filter_startdate;
    var filter_enddate = req.body.filter_enddate; 

    var where = "";
    if(filter_server){
        where += " AND a.SERVER_CODE='"+filter_server+"'";
    }

    if(filter_status){
        where += " AND a.ITEM_STATUS='"+filter_status+"'";
    }

    if(filter_startdate && filter_enddate){
        where += " AND a.REQUEST_DATE BETWEEN '"+filter_startdate+"' AND '"+filter_enddate+"'";
    }

    if(filter_startdate && !filter_enddate){
        where += " AND a.REQUEST_DATE='"+filter_startdate+"'";
    }

    if(!filter_startdate && filter_enddate){
        where += " AND a.REQUEST_DATE='"+filter_enddate+"'";
    }

    if(param=='' && where==''){
        var sql = "select a.*,CONCAT(b.PLANT_CODE,' - ',b.PLANT_NAME) as PLANT_NAME,c.UOM_NAME,d.STORAGE_LOCATION_NAME, \
                    DATE_FORMAT(a.SUBMIT_DATE, '%d-%m-%Y') as DATE, \
                    DATE_FORMAT(a.REQUEST_DATE, '%d-%m-%Y') as REQUEST_DATE, \
                    e.MATERIAL_TYPE_NAME,f.MATERIAL_GROUP_NAME,g.EXTERNAL_GROUP_NAME,\
                    h.NAME,h.EMAIL \
                    from tm_item_registration a \
                    left join tm_plant b ON a.PLANT=b.PLANT_CODE \
                    left join tm_uom c ON a.BASE_UOM=c.UOM_CODE \
                    left join tm_storage_location d ON a.STORAGE_LOCATION=d.STORAGE_LOCATION_ID and d.PLANT_CODE=a.PLANT \
                    left join tm_material_type e ON a.MATERIAL_TYPE=e.MATERIAL_TYPE_CODE and e.SERVER_CODE=a.SERVER_CODE  \
                    left join tm_material_group f ON a.MATERIAL_GROUP=f.MATERIAL_GROUP_ID and f.SERVER_CODE=a.SERVER_CODE   \
                    left join tm_external_group g ON a.EXTERNAL_GROUP=g.EXTERNAL_GROUP_CODE and g.MATERIAL_GROUP_CODE=a.MATERIAL_GROUP \
                    left join tm_user h ON a.USER_INPUT=h.USER_ID \
                    WHERE a.ITEM_STATUS IN ('"+status+"','DRAFT_CAT','FAIL','REG') \
                    GROUP BY a.REQUEST_NO ORDER BY a.UPDATE_DATE DESC limit 250";
                   
    }else{
        var sql = "select a.*,CONCAT(b.PLANT_CODE,' - ',b.PLANT_NAME) as PLANT_NAME,c.UOM_NAME,d.STORAGE_LOCATION_NAME, \
                    DATE_FORMAT(a.SUBMIT_DATE, '%d-%m-%Y') as DATE, \
                    DATE_FORMAT(a.REQUEST_DATE, '%d-%m-%Y') as REQUEST_DATE, \
                    e.MATERIAL_TYPE_NAME,f.MATERIAL_GROUP_NAME,g.EXTERNAL_GROUP_NAME,\
                    h.NAME,h.EMAIL \
                    from tm_item_registration a \
                    left join tm_plant b ON a.PLANT=b.PLANT_CODE \
                    left join tm_uom c ON a.BASE_UOM=c.UOM_CODE \
                    left join tm_storage_location d ON a.STORAGE_LOCATION=d.STORAGE_LOCATION_ID and d.PLANT_CODE=a.PLANT \
                    left join tm_material_type e ON a.MATERIAL_TYPE=e.MATERIAL_TYPE_CODE and e.SERVER_CODE=a.SERVER_CODE  \
                    left join tm_material_group f ON a.MATERIAL_GROUP=f.MATERIAL_GROUP_ID and f.SERVER_CODE=a.SERVER_CODE   \
                    left join tm_external_group g ON a.EXTERNAL_GROUP=g.EXTERNAL_GROUP_CODE and g.MATERIAL_GROUP_CODE=a.MATERIAL_GROUP \
                    left join tm_user h ON a.USER_INPUT=h.USER_ID \
                    WHERE (INC_CODE like '%"+param+"%' \
                    or a.INC like '%"+param+"%' or a.COLLOQUIAL_NAME like '%"+param+"%' or a.DESCRIPTION like '%"+param+"%' \
                    or a.SERVER_CODE like '%"+param+"%' or a.REQUEST_BY like '%"+param+"%' or a.MATERIAL_NUMBER like '%"+param+"%' or a.PART_NO like '%"+param+"%' or a.ADDITIONAL_INFORMATION like '%"+param+"%' or a.REQUEST_CODE like '%"+ param +"%')\
                    "+ where +" AND a.ITEM_STATUS IN ('"+status+"','FAIL','DRAFT_CAT','REG')  GROUP BY a.REQUEST_NO ORDER BY a.UPDATE_DATE DESC limit 250";
        
    }
            
    pool.getConnection(function(err, connection){
        if(err){
            res.send("Error Connection");
        }else{
            pool.query(sql,function(err,rows,fields){
                res.send(rows);
            }) 
        }
        connection.end();
    })
});

module.exports = router;

