var express = require('express');
var router = express.Router();
var pool = require('./db_connection');
const jwt = require('jsonwebtoken');
var mysql = require('mysql');
var cors = require('cors')
var myobj = {};


router.use('/home',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var user_id = req.body.user_id;
    var role = req.body.role;
    var where = "";

    if(role=='REQ'){
        where = " WHERE a.USER_INPUT='"+user_id+"'";
    
    }else if(role=='APP1'){
        where = " WHERE a.USER_APPROVAL1='" + user_id + "'";
    
    }else if(role=='APP2'){
        where = " WHERE a.USER_APPROVAL2='" + user_id + "'";
    
    }else if(role=='CAT'){
        where = " WHERE a.ITEM_STATUS IN ('CAT','DRAFT_CAT','FAIL','REG')";
    }
    
    var query = 'SELECT a.ITEM_STATUS,b.STATUS_DESC,COUNT(a.request_no) AS TOTAL \
                 FROM tm_item_registration a LEFT JOIN tm_status b ON a.ITEM_STATUS=b.STATUS_ID \
                 '+ where +'\
                 GROUP BY a.ITEM_STATUS ORDER BY b.SEQUENCE ASC';

    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
        }else{
            pool.query(query,function(err,rows,fields){
                if(err){
                    res.send("Error Dashboard");
                }else{
                    res.send(rows);
                }
            })
        }
        connection.end();
    })
});

router.use('/approve_ratio',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server_code = req.body.server_code;
    var company_id = req.body.company_id;
    var departement_id = req.body.departement_id;
    var startdate = req.body.startdate;
    var enddate = req.body.enddate;
    var where = "";

    if(server_code){
        where += " AND SERVER_CODE='"+server_code+"'";
    }

    if(company_id){
        where += " AND COMPANY_ID='"+company_id+"'";
    }

    if(departement_id){
        where += " AND DEPARTMENT_ID='"+departement_id+"'";
    }

    if(startdate && enddate){
        where += " AND REQUEST_DATE BETWEEN '"+startdate+"' AND '"+enddate+"'";
    }else{
        
        if(startdate){
            where += " AND REQUEST_DATE='"+startdate+"'";
        }
    
        if(enddate){
            where += " AND REQUEST_DATE='"+enddate+"'";
        }
    }
   
    var query = "SELECT \
                (SELECT COUNT(REQUEST_NO) FROM tm_item_registration WHERE ITEM_STATUS IN ('DRAFT_CAT','REG') "+where+") AS WAITING_SAP,\
                (SELECT COUNT(REQUEST_NO) FROM tm_item_registration WHERE ITEM_STATUS='RJT' "+where+" ) AS REJECTED\
                FROM tm_item_registration LIMIT 1";

    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
        }else{
            pool.query(query,function(err,rows,fields){
                if(err){
                    res.send("Error Dashboard");
                }else{
                    res.send(rows);
                }
            })
        }
        connection.end();
    })
});



router.use('/sla_admin',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var request_no = req.body.request_no;
    var server = req.body.server;
    var company = req.body.company;
    var departement_id = req.body.departement_id;
    var startdate = req.body.startdate;
    var enddate = req.body.enddate;
    var status = req.body.status;

    var item_name = req.body.item_name;
    var requester = req.body.requester;
    var approval1 = req.body.approval1;
    var approval2 = req.body.approval2;
    var cataloguer = req.body.cataloguer;
    
    var where = "";

    if(request_no){
        where = " AND a.REQUEST_CODE='"+request_no+"'";   
    }else{
        
        if(departement_id){
            where += " AND a.DEPARTEMENT_ID='"+departement_id+"'";
        }

        if(server){
            where += " AND a.SERVER_CODE='"+server+"'";
        }

        if(status){
            where += " AND a.ITEM_STATUS='"+status+"'";
        }
        
        if(company){
            where += " AND a.COMPANY_ID='"+company+"'";
        }

        if(startdate && enddate){
            where += " AND a.REQUEST_DATE BETWEEN '"+startdate+"' AND '"+enddate+"'";
        }else{
            
            if(startdate){
                where += " AND a.REQUEST_DATE='"+startdate+"'";
            }
        
            if(enddate){
                where += " AND a.REQUEST_DATE='"+enddate+"'";
            }
        }


        if(item_name){
            where += " AND a.INC LIKE '%"+item_name+"%'";
        }

        if(requester){
            where += " AND a.USER_INPUT='"+requester+"'";
        }

        if(approval1){
            where += " AND a.USER_APPROVAL1='"+approval1+"'";
        }

        if(approval2){
            where += " AND a.USER_APPROVAL2='"+approval2+"'";
        }

        if(cataloguer){
            where += " AND a.USER_CATALOGUER='"+cataloguer+"'";
        }

    }
    
    var query = "SELECT \
                    a.REQUEST_NO,\
                    a.REQUEST_CODE,\
                    a.DEPARTEMENT_ID,\
                    a.REQUEST_BY,\
                    a.INC,\
                    a.TYPE_STOCK,\
                    a.ITEM_STATUS,\
                    a.NEED_INFO_DATE,\
                    a.NEED_INFO_USER,\
                    DATE_FORMAT(a.REJECT_DATE, '%d %M %Y - %H:%i') as REJECT_DATE,\
                    DATE_FORMAT(a.REQUEST_DATE, '%d %M %Y') as REQUEST_DATE,\
                    DATE_FORMAT(a.SUBMIT_DATE, '%d %M %Y') as SUBMIT_DATE,\
                    DATE_FORMAT(a.APPROVAL1_DATE, '%d %M %Y - %H:%i') as APPROVAL1_DATES,\
                    DATE_FORMAT(a.APPROVAL2_DATE, '%d %M %Y - %H:%i') as APPROVAL2_DATES,\
                    DATE_FORMAT(a.CATALOGUER_DATE, '%d %M %Y - %H:%i') as CATALOGUER_DATES,\
                    DATEDIFF(a.APPROVAL1_DATE, a.SUBMIT_DATE) AS 'APPROVAL1_DATE',\
                    DATEDIFF(a.APPROVAL2_DATE, a.APPROVAL1_DATE) AS 'APPROVAL2_DATE',\
                    (CASE \
                        WHEN a.TYPE_STOCK='Y' THEN DATEDIFF(a.CATALOGUER_DATE, a.APPROVAL2_DATE)\
                        WHEN a.TYPE_STOCK='N' THEN DATEDIFF(a.CATALOGUER_DATE, a.SUBMIT_DATE) END\
                    ) \
                    AS 'CATALOGUER_DATE',\
                    b.NAME AS 'REQUESTER',\
                    c.NAME AS 'APPROVAL1',\
                    d.NAME AS 'APPROVAL2',\
                    e.NAME AS 'CATALOGUER', \
                    f.STATUS_DESC, \
                    g.NAME AS 'REJECT_USER', \
                    g.ROLE_ID AS 'REJECT_USER_ROLE', \
                    (SELECT n.NAME FROM tx_departement_role m LEFT JOIN tm_user n ON m.USER_ID=n.USER_ID WHERE m.DEPARTEMENT_ID=a.DEPARTEMENT_ID AND m.STATUS='Y' LIMIT 1) AS 'MON_APPROVAL1',\
                    (SELECT n.NAME FROM tx_approval_server m LEFT JOIN tm_user n ON m.APPROVAL2=n.USER_ID WHERE m.SERVER=a.SERVER_CODE AND m.STATUS='Y' LIMIT 1) AS 'MON_APPROVAL2',\
                    (SELECT n.NAME FROM tx_approval_server m LEFT JOIN tm_user n ON m.CATALOGUER=n.USER_ID WHERE m.SERVER=a.SERVER_CODE AND m.STATUS='Y' LIMIT 1) AS 'MON_CATALOGUER',\
                    DATEDIFF(NOW(), a.SUBMIT_DATE) AS 'MON_APPROVAL1_DATE',\
                    DATEDIFF(NOW(), a.APPROVAL1_DATE) AS 'MON_APPROVAL2_DATE',\
                    (CASE \
                        WHEN a.TYPE_STOCK='Y' THEN DATEDIFF(NOW(), a.APPROVAL2_DATE)\
                        WHEN a.TYPE_STOCK='N' THEN DATEDIFF(NOW(), a.SUBMIT_DATE) END\
                    ) AS 'MON_CATALOGUER_DATE',\
                    DATEDIFF(a.REGISTER_SAP_DATE, a.CATALOGUER_DATE) AS 'REGISTER_SAP_DATE',\
                    DATE_FORMAT(a.REGISTER_SAP_DATE, '%d %M %Y - %H:%i') as REGISTER_SAP_DATES\
                    FROM tm_item_registration a \
                    LEFT JOIN tm_user b ON a.USER_INPUT=b.USER_ID \
                    LEFT JOIN tm_user c ON a.USER_APPROVAL1=c.USER_ID \
                    LEFT JOIN tm_user d ON a.USER_APPROVAL2=d.USER_ID \
                    LEFT JOIN tm_user e ON a.USER_CATALOGUER=e.USER_ID \
                    LEFT JOIN tm_status f ON a.ITEM_STATUS=f.STATUS_ID \
                    LEFT JOIN tm_user g ON a.REJECT_USER=g.USER_ID \
                    WHERE a.REQUEST_NO !='' "+where+" ORDER BY a.REQUEST_NO DESC";

    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
        }else{
            pool.query(query,function(err,rows,fields){
                if(err){
                    res.send("Error Query");
                }else{
                    res.send(rows);
                }
            })
        }
        connection.end();
    })
});


router.use('/sla_user',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var user_id = req.body.user_id;
    var request_no = req.body.request_no;
    var server = req.body.server;
    var item_name = req.body.item_name;
    var startdate = req.body.startdate;
    var enddate = req.body.enddate;
    
    var approval1 = req.body.approval1;
    var approval2 = req.body.approval2;
    var cataloguer = req.body.cataloguer;

    var where = " AND USER_INPUT='"+user_id+"'";

    if(request_no){
        where += " AND a.REQUEST_CODE='"+request_no+"'";

    }else{
        if(server){
            where += " AND a.SERVER_CODE='"+server+"'";
        }
        
        if(company){
            where += " AND a.COMPANY_ID='"+company+"'";
        }

        if(startdate && enddate){
            where += " AND a.REQUEST_DATE BETWEEN '"+startdate+"' AND '"+enddate+"'";
        }else{
            
            if(startdate){
                where += " AND a.REQUEST_DATE='"+startdate+"'";
            }
        
            if(enddate){
                where += " AND a.REQUEST_DATE='"+enddate+"'";
            }
        }


        if(item_name){
            where += " AND a.INC LIKE '%"+item_name+"%'";
        }

        if(approval1){
            where += " AND a.USER_APPROVAL1='"+approval1+"'";
        }

        if(approval2){
            where += " AND a.USER_APPROVAL2='"+approval2+"'";
        }

        if(cataloguer){
            where += " AND a.USER_CATALOGUER='"+cataloguer+"'";
        }
    }
    
    var query = "SELECT \
                    a.REQUEST_NO,\
                    a.REQUEST_CODE,\
                    a.DEPARTEMENT_ID,\
                    a.REQUEST_BY,\
                    a.INC,\
                    DATEDIFF(a.APPROVAL1_DATE, a.SUBMIT_DATE) AS 'APPROVAL1_DATE',\
                    DATEDIFF(a.APPROVAL2_DATE, a.APPROVAL1_DATE) AS 'APPROVAL2_DATE',\
                    DATEDIFF(a.CATALOGUER_DATE, a.APPROVAL2_DATE) AS 'CATALOGUER_DATE',\
                    b.NAME AS 'REQUESTER',\
                    c.NAME AS 'APPROVAL1',\
                    d.NAME AS 'APPROVAL2',\
                    e.NAME AS 'CATALOGUER', \
                    f.STATUS_DESC \
                    FROM tm_item_registration a \
                    LEFT JOIN tm_user b ON a.USER_INPUT=b.USER_ID \
                    LEFT JOIN tm_user c ON a.USER_APPROVAL1=c.USER_ID \
                    LEFT JOIN tm_user d ON a.USER_APPROVAL2=d.USER_ID \
                    LEFT JOIN tm_user e ON a.USER_CATALOGUER=e.USER_ID \
                    LEFT JOIN tm_status f ON a.ITEM_STATUS=f.STATUS_ID \
                    WHERE a.REQUEST_NO !='' "+where+"";

    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
        }else{
            pool.query(query,function(err,rows,fields){
                if(err){
                    res.send("Error Dashboard");
                }else{
                    res.send(rows);
                }
            })
        }
        connection.end();
    })
});




router.use('/top_requester1',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server = req.body.server;
    var company = req.body.company;
    var startdate = req.body.startdate;
    var enddate = req.body.enddate;
    var where = "";

    if(server){
        where += " AND SERVER_CODE='"+server+"'";
    }
    
    if(company){
        where += " AND COMPANY_ID='"+company+"'";
    }

    if(startdate && enddate){
        where += " AND REQUEST_DATE BETWEEN '"+startdate+"' AND '"+enddate+"'";
    }else{
        
        if(startdate){
            where += " AND REQUEST_DATE='"+startdate+"'";
        }
    
        if(enddate){
            where += " AND REQUEST_DATE='"+enddate+"'";
        }
    }
    
    var query = "SELECT COUNT(REQUEST_NO) as TOTAL,DEPARTEMENT_ID as SHORTNAME FROM tm_item_registration \
                 WHERE REQUEST_NO !='' "+where+" \
                 GROUP BY DEPARTEMENT_ID";

    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
        }else{
            pool.query(query,function(err,rows,fields){
                if(err){
                    res.send("Error Dashboard");
                }else{
                    res.send(rows);
                }
            })
        }
        connection.end();
    })
});


router.use('/top_requester2',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server = req.body.server;
    var company = req.body.company;
    var startdate = req.body.startdate;
    var enddate = req.body.enddate;
    var where = "";

    if(server){
        where += " AND a.SERVER_CODE='"+server+"'";
    }
    
    if(company){
        where += " AND a.COMPANY_ID='"+company+"'";
    }

    if(startdate && enddate){
        where += " AND a.REQUEST_DATE BETWEEN '"+startdate+"' AND '"+enddate+"'";
    }else{

        if(startdate){
            where += " AND a.REQUEST_DATE='"+startdate+"'";
        }
    
        if(enddate){
            where += " AND a.REQUEST_DATE='"+enddate+"'";
        }
    }
    

    var query = "SELECT COUNT(a.REQUEST_NO) as TOTAL,b.SHORTNAME FROM tm_item_registration a \
                 LEFT JOIN tm_company b ON a.COMPANY_ID=b.COMPANY_ID \
                 WHERE a.REQUEST_NO !='' "+where+"\
                 GROUP BY a.COMPANY_ID";
    
    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
        }else{
            pool.query(query,function(err,rows,fields){
                if(err){
                    res.send("Error Dashboard");
                }else{
                    res.send(rows);
                }
            })
        }
        connection.end();
    })
});



router.use('/done_approve1',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server = req.body.server;
    var company = req.body.company;
    var startdate = req.body.startdate;
    var enddate = req.body.enddate;

    var where = "";

    if(server){
        where += " AND SERVER_CODE='"+server+"'";
    }
    
    if(company){
        where += " AND COMPANY_ID='"+company+"'";
    }

    if(startdate && enddate){
        where += " AND REQUEST_DATE BETWEEN '"+startdate+"' AND '"+enddate+"'";
    }else{
        
        if(startdate){
            where += " AND REQUEST_DATE='"+startdate+"'";
        }
    
        if(enddate){
            where += " AND REQUEST_DATE='"+enddate+"'";
        }
    }

    var query = "SELECT \
                (SELECT COUNT(REQUEST_NO) FROM tm_item_registration \
                WHERE ITEM_STATUS IN ('DRAFT_CAT','REG','FAIL') "+where+") AS DONE,\
                (SELECT COUNT(REQUEST_NO) FROM tm_item_registration \
                WHERE ITEM_STATUS='CAT' "+where+") AS ONREVIEW \
                FROM tm_item_registration LIMIT 1";
    
    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
        }else{
            pool.query(query,function(err,rows,fields){
                if(err){
                    res.send("Error Dashboard");
                }else{
                    res.send(rows);
                }
            })
        }
        connection.end();
    })
});

module.exports = router;



router.use('/done_approve2',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server = req.body.server;
    var company = req.body.company;
    var startdate = req.body.startdate;
    var enddate = req.body.enddate;

    var where = "";

    if(server){
        where += " AND SERVER_CODE='"+server+"'";
    }
    
    if(company){
        where += " AND COMPANY_ID='"+company+"'";
    }

    if(startdate && enddate){
        where += " AND REQUEST_DATE BETWEEN '"+startdate+"' AND '"+enddate+"'";
    }else{
        
        if(startdate){
            where += " AND REQUEST_DATE='"+startdate+"'";
        }
    
        if(enddate){
            where += " AND REQUEST_DATE='"+enddate+"'";
        }
    }
    
    var query = "SELECT \
                (SELECT COUNT(REQUEST_NO) FROM tm_item_registration \
                WHERE ITEM_STATUS='REG' "+where+") AS REG, \
                (SELECT COUNT(REQUEST_NO) FROM tm_item_registration \
                WHERE ITEM_STATUS='DRAFT_CAT' "+where+") AS DRAFT_CAT,\
                (SELECT COUNT(REQUEST_NO) FROM tm_item_registration \
                WHERE ITEM_STATUS='FAIL' "+where+") AS RJT \
                FROM tm_item_registration LIMIT 1";
    
    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
        }else{
            pool.query(query,function(err,rows,fields){
                if(err){
                    res.send("Error Dashboard");
                }else{
                    res.send(rows);
                }
            })
        }
        connection.end();
    })
});


router.use('/done_approve3',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var server = req.body.server;
    var company = req.body.company;
    var startdate = req.body.startdate;
    var enddate = req.body.enddate;

    var where = "";
    var where2 = "";

    if(server){
        where2 += " AND a.SERVER_CODE='"+server+"'";
    }
    
    if(company){
        where2 += " AND a.COMPANY_ID='"+company+"'";
    }

    if(startdate && enddate){
        where += " AND REQUEST_DATE BETWEEN '"+startdate+"' AND '"+enddate+"'";
    }else{
        
        if(startdate){
            where += " AND REQUEST_DATE='"+startdate+"'";
        }
    
        if(enddate){
            where += " AND REQUEST_DATE='"+enddate+"'";
        }
    }
    
    var query = "SELECT \
                b.SHORTNAME,\
                 (SELECT COUNT(REQUEST_NO) FROM tm_item_registration \
                 WHERE ITEM_STATUS IN ('DRAFT_CAT','REG','FAIL') AND COMPANY_ID=a.COMPANY_ID "+where+") AS REG,\
                 (SELECT COUNT(REQUEST_NO) FROM tm_item_registration \
                 WHERE ITEM_STATUS='CAT' AND COMPANY_ID=a.COMPANY_ID "+where+") AS DRAFT_CAT \
                 FROM tm_item_registration a \
                 LEFT JOIN tm_company b ON a.COMPANY_ID=b.COMPANY_ID \
                 WHERE a.REQUEST_NO !='' "+where2+" GROUP BY a.COMPANY_ID HAVING reg > 0 or draft_cat > 0";
    
    pool.getConnection(function(err,connection){
        if(err){
            res.send("Error Connection");
        }else{
            pool.query(query,function(err,rows,fields){
                if(err){
                    res.send(query);
                }else{
                    res.send(rows);
                }
            })
        }
        connection.end();
    })
});

module.exports = router;

