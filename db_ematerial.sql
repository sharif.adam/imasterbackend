/*
SQLyog Community v13.1.1 (64 bit)
MySQL - 10.1.38-MariaDB : Database - db_ematerial
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_ematerial` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_ematerial`;

/*Table structure for table `tm_item_registration` */

DROP TABLE IF EXISTS `tm_item_registration`;

CREATE TABLE `tm_item_registration` (
  `REQUEST_NO` int(10) NOT NULL AUTO_INCREMENT,
  `REQUEST_BY` varchar(100) NOT NULL,
  `REQUEST_DATE` date NOT NULL,
  `MATERIAL_NUMBER` varchar(15) DEFAULT NULL,
  `REQUEST_CODE` varchar(30) DEFAULT NULL,
  `INC_CODE` varchar(30) NOT NULL,
  `INC` varchar(250) DEFAULT NULL,
  `DESCRIPTION` text NOT NULL,
  `ADDITIONAL_INFORMATION` text,
  `COLLOQUIAL_NAME` varchar(200) DEFAULT NULL,
  `BRAND` varchar(30) DEFAULT NULL,
  `PART_NO` varchar(30) NOT NULL,
  `BASE_UOM` varchar(30) DEFAULT NULL,
  `MATERIAL_GROUP` varchar(30) DEFAULT NULL,
  `EXTERNAL_GROUP` varchar(30) DEFAULT NULL,
  `COMPANY_ID` varchar(20) DEFAULT NULL,
  `COMPANY_NAME` varchar(200) NOT NULL,
  `PLANT` varchar(30) DEFAULT NULL,
  `STORAGE_LOCATION` varchar(30) DEFAULT NULL,
  `AVERAGE_USAGE` varchar(10) DEFAULT NULL,
  `TYPE_STOCK` char(5) NOT NULL,
  `SAFETY_STOCK` varchar(10) DEFAULT NULL,
  `CRITICALLITY` varchar(30) DEFAULT NULL,
  `MATERIAL_TYPE` varchar(30) DEFAULT NULL,
  `VALUATION_CLASS` varchar(30) DEFAULT NULL,
  `PRICE_CONTROL` varchar(30) DEFAULT NULL,
  `PROFIT_CENTER` varchar(30) DEFAULT NULL,
  `LAB_OFFICE` varchar(30) DEFAULT NULL,
  `ATTACHMENT` varchar(200) DEFAULT NULL,
  `SERVER_CODE` varchar(20) NOT NULL,
  `ITEM_STATUS` char(10) NOT NULL COMMENT 'DRAFT,',
  `CATALOGUER` varchar(20) DEFAULT NULL,
  `TYPE_INPUT` char(10) DEFAULT NULL COMMENT 'NEW(Material Baru), EXT(Extended)',
  `MESSAGE` text,
  `USER_INPUT` int(10) DEFAULT NULL,
  `DEPARTEMENT_ID` varchar(20) DEFAULT NULL,
  `SUBMIT_DATE` datetime DEFAULT NULL,
  `APPROVAL1_DATE` datetime DEFAULT NULL,
  `APPROVAL2_DATE` datetime DEFAULT NULL,
  `CATALOGUER_DATE` datetime DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `USER_APPROVAL1` varchar(30) DEFAULT NULL,
  `USER_APPROVAL2` varchar(30) DEFAULT NULL,
  `USER_CATALOGUER` varchar(30) DEFAULT NULL,
  `REGISTER_SAP_DATE` datetime DEFAULT NULL,
  `INC_ATTRIBUTE` text,
  `REQUESTER_DESCRIPTION` varchar(40) DEFAULT NULL,
  `REQUESTER_ADDITIONAL_INFO` text,
  `REQUESTER_JSON_INC` text,
  `NEED_INFO_DATE` datetime DEFAULT NULL,
  `NEED_INFO_USER` varchar(30) DEFAULT NULL,
  `REJECT_DATE` datetime DEFAULT NULL,
  `REJECT_USER` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`REQUEST_NO`,`PART_NO`),
  KEY `INDEX_MATERIAL` (`MATERIAL_GROUP`) USING HASH,
  KEY `tm_user` (`USER_INPUT`),
  KEY `uom` (`BASE_UOM`),
  KEY `company` (`COMPANY_ID`),
  KEY `external_group` (`EXTERNAL_GROUP`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `tm_item_registration` */

insert  into `tm_item_registration`(`REQUEST_NO`,`REQUEST_BY`,`REQUEST_DATE`,`MATERIAL_NUMBER`,`REQUEST_CODE`,`INC_CODE`,`INC`,`DESCRIPTION`,`ADDITIONAL_INFORMATION`,`COLLOQUIAL_NAME`,`BRAND`,`PART_NO`,`BASE_UOM`,`MATERIAL_GROUP`,`EXTERNAL_GROUP`,`COMPANY_ID`,`COMPANY_NAME`,`PLANT`,`STORAGE_LOCATION`,`AVERAGE_USAGE`,`TYPE_STOCK`,`SAFETY_STOCK`,`CRITICALLITY`,`MATERIAL_TYPE`,`VALUATION_CLASS`,`PRICE_CONTROL`,`PROFIT_CENTER`,`LAB_OFFICE`,`ATTACHMENT`,`SERVER_CODE`,`ITEM_STATUS`,`CATALOGUER`,`TYPE_INPUT`,`MESSAGE`,`USER_INPUT`,`DEPARTEMENT_ID`,`SUBMIT_DATE`,`APPROVAL1_DATE`,`APPROVAL2_DATE`,`CATALOGUER_DATE`,`UPDATE_DATE`,`USER_APPROVAL1`,`USER_APPROVAL2`,`USER_CATALOGUER`,`REGISTER_SAP_DATE`,`INC_ATTRIBUTE`,`REQUESTER_DESCRIPTION`,`REQUESTER_ADDITIONAL_INFO`,`REQUESTER_JSON_INC`,`NEED_INFO_DATE`,`NEED_INFO_USER`,`REJECT_DATE`,`REJECT_USER`) values 
(1,'anisanan','2020-03-11','901001','110320215306','00001','ELECTRON TUBE','ELECTRON TUBE;123','ELECTRON TUBE;123;123','Electron Tube','123','123','CAR','5800','5820','3000','PT. BERAU COAL','B400','4002','','N','','','ZFG','3010','S','null','E','33-1 p97BugR0XeVxY__7CqL9fA.png','BC','FAIL',NULL,'NEW','',33,'AUDIT-BCE','2020-03-11 21:53:06',NULL,NULL,'2020-03-11 21:57:02','2020-03-11 21:57:02',NULL,NULL,'64','2020-03-11 22:01:35','[{\"index\":0,\"code\":\"SH_NAME\",\"desc\":\"SHORT NAME\",\"val\":\"ELECTRON TUBE\"},{\"index\":1,\"code\":\"TUBE_FUNCT\",\"desc\":\"TUBE FUNCTION\",\"val\":\"123\"},{\"index\":\"add\",\"code\":\"ADDITIONAL_TEXT\",\"desc\":\"ADDITIONAL TEXT\",\"val\":\"123\"}]',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(3,'erssadm','2020-03-11','12','110320221707','00001','ELECTRON TUBE','ELECTRON TUBE;12;12','ELECTRON TUBE;12;12','Electron Tube','123','12','BOX','5200','5210','1000','PT. BERAU COAL ENERGY','A100','WHSE','12','Y','12','Critical','ZFG','3010','S','null','H','64-1200px-.NET_Core_Logo.svg.png','BC','CAT',NULL,'NEW','',41,'ERG-BCE','2020-03-11 22:17:17','2020-03-11 22:18:54','2020-03-11 22:42:53','2020-03-11 22:21:39','2020-03-11 22:42:53','42','76','64','2020-03-11 22:21:48','[{\"index\":0,\"code\":\"SH_NAME\",\"desc\":\"SHORT NAME\",\"val\":\"ELECTRON TUBE\"},{\"index\":1,\"code\":\"TUBE_FUNCT\",\"desc\":\"TUBE FUNCTION\",\"val\":\"12\"},{\"index\":2,\"code\":\"TYPE_NUMBER\",\"desc\":\"TYPE NUMBER\",\"val\":\"12\"}]',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(5,'erssadm','2020-03-11','1230091','110320225207','00023','CONE-ROLLERS,TAPERED ROLLER BEARING','CONE-ROLLERS,TAPERED ROLLER BEARING;123;','CONE-ROLLERS,TAPERED ROLLER BEARING;123;121;121','123','121','121111','PMI','5800','5820','1000','PT. BERAU COAL ENERGY','A100','WHSE','12','Y','12','Critical','ZFG','3010','V','null','E','','BC','DRAFT_CAT',NULL,'NEW','',41,'ERG-BCE','2020-03-11 22:52:31','2020-03-11 22:52:55','2020-03-11 22:53:13','2020-03-24 12:06:43','2020-03-24 12:06:43','42','76','1',NULL,'[{\"index\":0,\"code\":\"SH_NAME\",\"desc\":\"SHORT NAME\",\"val\":\"CONE-ROLLERS,TAPERED ROLLER BEARING\"},{\"index\":1,\"code\":\"BRG_BORE\",\"desc\":\"BEARING BORE\",\"val\":\"123\"},{\"index\":2,\"code\":\"BRG_ID\",\"desc\":\"BEARING ID\",\"val\":\"121\"},{\"index\":3,\"code\":\"BRG_OUTSIDE_DIA\",\"desc\":\"BEARING OUTSIDE DIAMETER\",\"val\":\"121\"}]','CONE-ROLLERS,TAPERED ROLLER BEARING;123;','CONE-ROLLERS,TAPERED ROLLER BEARING;123;121;121','[{\"index\":0,\"code\":\"SH_NAME\",\"desc\":\"SHORT NAME\",\"val\":\"CONE-ROLLERS,TAPERED ROLLER BEARING\"},{\"index\":1,\"code\":\"BRG_BORE\",\"desc\":\"BEARING BORE\",\"val\":\"123\"},{\"index\":2,\"code\":\"BRG_ID\",\"desc\":\"BEARING ID\",\"val\":\"121\"},{\"index\":3,\"code\":\"BRG_OUTSIDE_DIA\",\"desc\":\"BEARING OUTSIDE DIAMETER\",\"val\":\"121\"}]',NULL,NULL,NULL,NULL),
(6,'erssadm','2020-03-11','13000010','110320230204','00007','CAPACITOR,FIXED,CERAMIC DIELECTRIC','CAPACITOR,FIXED,CERAMIC DIELECTRIC;2121;','CAPACITOR,FIXED,CERAMIC DIELECTRIC;2121;123','111','1231','1121','CCM','5200','5220','2100','PT. MUTIARA TANJUNG LESTARI','C100','1002','12','Y','11','Insurance','ZOS','5075','V','null','H','','BC','REG',NULL,'NEW','',41,'ERG-BCE','2020-03-11 23:02:10','2020-03-11 23:02:36','2020-03-11 23:02:52','2020-03-11 23:03:22','2020-03-11 23:03:22','42','76','64','2020-03-11 23:03:33','[{\"index\":0,\"code\":\"SH_NAME\",\"desc\":\"SHORT NAME\",\"val\":\"CAPACITOR,FIXED,CERAMIC DIELECTRIC\"},{\"index\":3,\"code\":\"MATL\",\"desc\":\"MATERIAL\",\"val\":\"2121\"},{\"index\":4,\"code\":\"ADD_FEAT\",\"desc\":\"ADDITIONAL FEATURE\",\"val\":\"123\"}]',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(7,'erssadm','2020-03-11','1009000112','110320230559','00001','ELECTRON TUBE','ELECTRON TUBE;123;12;121','ELECTRON TUBE;123;12;121','113','12132','121','CAN','5800','5820','1000','PT. BERAU COAL ENERGY','A100','WHSE','','N','','','ZRM','1020','V','null','E','','BC','FAIL',NULL,'NEW','',41,'ERG-BCE','2020-03-11 23:06:00',NULL,NULL,'2020-03-11 23:09:33','2020-03-11 23:09:33',NULL,NULL,'64','2020-03-24 12:04:38','[{\"index\":0,\"code\":\"SH_NAME\",\"desc\":\"SHORT NAME\",\"val\":\"ELECTRON TUBE\"},{\"index\":1,\"code\":\"TUBE_FUNCT\",\"desc\":\"TUBE FUNCTION\",\"val\":\"123\"},{\"index\":2,\"code\":\"TYPE_NUMBER\",\"desc\":\"TYPE NUMBER\",\"val\":\"12\"},{\"index\":3,\"code\":\"TUBE_MATL\",\"desc\":\"TUBE MATERIAL\",\"val\":\"121\"}]',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(8,'erssadm','2020-03-24',NULL,'240320115716','00004','CAPACITOR,FIXED,GLASS DIELECTRIC','CAPACITOR,FIXED,GLASS DIELECTRIC;1231;11','CAPACITOR,FIXED,GLASS DIELECTRIC;1231;11231;123','1231','1123','12321','000','7000','7021','2100','PT. MUTIARA TANJUNG LESTARI','C100','1002','10','Y','12','Critical','ZOS',NULL,NULL,NULL,NULL,'','BC','CAT',NULL,'NEW','',41,'ERG-BCE','2020-03-24 11:58:41','2020-03-24 11:59:37','2020-03-24 12:01:02',NULL,'2020-03-24 12:01:02','42','76',NULL,NULL,'[{\"index\":0,\"code\":\"SH_NAME\",\"desc\":\"SHORT NAME\",\"val\":\"CAPACITOR,FIXED,GLASS DIELECTRIC\"},{\"index\":1,\"code\":\"DESIGN_TYPE\",\"desc\":\"DESIGN TYPE\",\"val\":\"1231\"},{\"index\":2,\"code\":\"DIMENSION_SIZE\",\"desc\":\"DIMENSION SIZE\",\"val\":\"11231\"},{\"index\":3,\"code\":\"MATL\",\"desc\":\"MATERIAL\",\"val\":\"123\"}]',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(9,'erssadm','2020-03-24',NULL,'240320122211','00005','CAPACITOR,FIXED,MICA DIELECTRIC','CAPACITOR,FIXED,MICA DIELECTRIC;123;112','CAPACITOR,FIXED,MICA DIELECTRIC;123;112','1231','121','1123','BAG','','','2200','PT. PELAYARAN SANDITIA PERKASA MARITIM','D100','1003','12','Y','11','Critical','ZRM',NULL,NULL,NULL,NULL,'','BC','CAT',NULL,'NEW','',41,'ERG-BCE','2020-03-24 12:22:17','2020-03-24 12:24:02','2020-03-24 12:30:34',NULL,'2020-03-24 12:30:34','42','76',NULL,NULL,'[{\"index\":0,\"code\":\"SH_NAME\",\"desc\":\"SHORT NAME\",\"val\":\"CAPACITOR,FIXED,MICA DIELECTRIC\"},{\"index\":1,\"code\":\"DESIGN_TYPE\",\"desc\":\"DESIGN TYPE\",\"val\":\"123\"},{\"index\":2,\"code\":\"DIMENSION_SIZE\",\"desc\":\"DIMENSION SIZE\",\"val\":\"112\"}]',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(10,'erssadm','2020-03-30',NULL,'300320092743','00003','CAPACITOR,FIXED,PAPER DIELECTRIC','CAPACITOR,FIXED,PAPER DIELECTRIC;123;121','CAPACITOR,FIXED,PAPER DIELECTRIC;123;12131','123','12121','2121','BOM','5200','5220','2100','PT. MUTIARA TANJUNG LESTARI','C100','1003','','N','','','ZNS',NULL,NULL,NULL,NULL,'','BC','CAT',NULL,'NEW','',41,'ERG-BCE','2020-03-30 09:27:44',NULL,NULL,NULL,'2020-03-30 09:27:44',NULL,NULL,NULL,NULL,'[{\"index\":0,\"code\":\"SH_NAME\",\"desc\":\"SHORT NAME\",\"val\":\"CAPACITOR,FIXED,PAPER DIELECTRIC\"},{\"index\":1,\"code\":\"DESIGN_TYPE\",\"desc\":\"DESIGN TYPE\",\"val\":\"123\"},{\"index\":2,\"code\":\"DIMENSION_SIZE\",\"desc\":\"DIMENSION SIZE\",\"val\":\"12131\"}]',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(11,'erssadm','2020-03-30',NULL,'300320092857','00008','CAPACITOR,FIXED,ELECTROLYTIC','CAPACITOR,FIXED,ELECTROLYTIC;1212;12121','CAPACITOR,FIXED,ELECTROLYTIC;1212;12121','1212','2121','121212121','CCM','6600','6610','2100','PT. MUTIARA TANJUNG LESTARI','C100','1003','','N','','','ZRM',NULL,NULL,NULL,NULL,'','BC','CAT',NULL,'NEW','',41,'ERG-BCE','2020-03-30 09:29:03',NULL,NULL,NULL,'2020-03-30 09:29:03',NULL,NULL,NULL,NULL,'[{\"index\":0,\"code\":\"SH_NAME\",\"desc\":\"SHORT NAME\",\"val\":\"CAPACITOR,FIXED,ELECTROLYTIC\"},{\"index\":1,\"code\":\"DESIGN_TYPE\",\"desc\":\"DESIGN TYPE\",\"val\":\"1212\"},{\"index\":2,\"code\":\"DIMENSION_SIZE\",\"desc\":\"DIMENSION SIZE\",\"val\":\"12121\"}]',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(12,'datacon','2020-03-30',NULL,'300320094508','00029','RING,BEARING,OUTER','RING,BEARING,OUTER;121;2121;212','RING,BEARING,OUTER;121;2121;212','121','2121','2121009','000','7000','7021','1000','PT. BERAU COAL ENERGY','A100','WHSE','','N','','','ZNS',NULL,NULL,NULL,NULL,'','BC','CAT',NULL,'NEW','',35,'EMS-BCE','2020-03-30 09:45:19',NULL,NULL,NULL,'2020-03-30 09:45:19',NULL,NULL,NULL,NULL,'[{\"index\":0,\"code\":\"SH_NAME\",\"desc\":\"SHORT NAME\",\"val\":\"RING,BEARING,OUTER\"},{\"index\":1,\"code\":\"RING_ID\",\"desc\":\"RING ID\",\"val\":\"121\"},{\"index\":2,\"code\":\"RING_INSIDE_DIA\",\"desc\":\"RING ID\",\"val\":\"2121\"},{\"index\":3,\"code\":\"RING_OD\",\"desc\":\"RING OD\",\"val\":\"212\"}]',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(13,'datacon','2020-03-30',NULL,'300320095037','00135','SWITCH,KNIFE','SWITCH,KNIFE;121;21212','SWITCH,KNIFE;121;21212','11','2121','DE32','CAR','5200','5220','2100','PT. MUTIARA TANJUNG LESTARI','C100','1003','21','Y','12','Critical','ZNS',NULL,NULL,NULL,NULL,'','BC','APP1',NULL,'NEW','',35,'EMS-BCE','2020-03-30 09:50:43',NULL,NULL,NULL,'2020-03-30 09:50:43',NULL,NULL,NULL,NULL,'[{\"index\":0,\"code\":\"SH_NAME\",\"desc\":\"SHORT NAME\",\"val\":\"SWITCH,KNIFE\"},{\"index\":1,\"code\":\"CURRENT_RTG\",\"desc\":\"CURRENT RATING\",\"val\":\"121\"},{\"index\":2,\"code\":\"VOLT_RTG\",\"desc\":\"VOLTAGE RATING\",\"val\":\"21212\"}]',NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `tm_user` */

DROP TABLE IF EXISTS `tm_user`;

CREATE TABLE `tm_user` (
  `USER_ID` int(10) NOT NULL AUTO_INCREMENT,
  `USER_NAME` varchar(100) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `DEPARTEMENT_ID` char(30) NOT NULL,
  `ROLE_ID` char(30) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL,
  `SERVER_CODE` varchar(50) DEFAULT 'GEMS',
  `SERVER_ROLE` varchar(50) DEFAULT NULL,
  `STATUS` char(10) DEFAULT 'Y',
  PRIMARY KEY (`USER_ID`,`EMAIL`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;

/*Data for the table `tm_user` */

insert  into `tm_user`(`USER_ID`,`USER_NAME`,`NAME`,`DEPARTEMENT_ID`,`ROLE_ID`,`EMAIL`,`PASSWORD`,`SERVER_CODE`,`SERVER_ROLE`,`STATUS`) values 
(1,'addin','Anugrah Addin','PROC-GEMS','CAT','anugra.idi@sinarmasmining.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(2,'tjekjen','Tjek Jen','HL-GEMS','APP2','tjek.jen@sinarmasmining.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(3,'freda','Fredawati Sihombing','HRGA-GEMS','REQ','fredawati.sihombing@sinarmasmining.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(4,'norhalimah','Norhalimah','CCEM-GEMS','REQ','Admin.ccem@borneo-indobara.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(5,'nurulhal','Nurul Halifah','IT-GEMS','REQ','nurul.halifah@borneo-indobara.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(6,'arifardi','Arif Ardiyanto','SGE-GEMS','REQ','arif.ardiyanto@borneo-indobara.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(7,'nadyamun','Nadya Muntaza','SPO-GEMS','REQ','nadya.muntaza@borneo-indobara.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(8,'rinanda','Rinanda Kinanti','CPPP-GEMS','REQ','rinanda.kinanti@sinarmasmining.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(9,'sitimasi','Siti Masitah','CCC-GEMS','REQ','siti.masitah@borneo-indobara.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(10,'fernando','Fernando Saputra','PORTMIN-GEMS','REQ','fernando.saputra@sinarmasmining.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(11,'nadianor','Nadia Normakiah','CIM-GEMS','REQ','Nadia.normakiah@borneo-indobara.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(12,'nurharis','Nurharis Maulana','CIM-GEMS','REQ','nurharis.maulana@borneo-indobara.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(13,'ghazali','A.Ghazali Rahman','HRGA-GEMS','REQ','admin.ga@borneo-indobara.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(14,'natalia','Natalita','PORT-GEMS','REQ','natalita.natalita@borneo-indobara.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(15,'abuyamin','Abu Yamin ','OSH-GEMS','REQ','abuyamin279@gmail.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(16,'hendrayus','Hendra Yusuf ','DC-GEMS','REQ','yusufhendra72@gmail.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(17,'fakhriihsan','M.Fakhri Ihsan ','LOG-GEMS','REQ','mfakhriihsan@gmail.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(18,'harusbudi','Haris Budi Prasetyo ','DCBSH-GEMS','REQ','haris_logmal@yahoo.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(19,'sarwan','Sarwan ','LOGSTF-GEMS','REQ','sarwan_biduri@yahoo.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(20,'adimas','Adi Maswardi ','PLNTTL-GEMS','REQ','adimaswardi@gmail.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(21,'alisafrani','Ali Safrani','CCEMPED-GEMS','APP1','ali.safrani@sinarmasmining.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(22,'renirach','Reni Rachmad','CCEMPED-GEMS','REQ','reni.rachmad@sinarmasmining.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(23,'mahdalena','Mahdalena','HSE-GEMS','REQ','Mahdalena@sinarmasmining.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(24,'adminsmkp','Admin SMKP','HSE-GEMS','REQ','Admin.smkp@borneo-indobara.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(25,'raisaros','Raisa Rosyadi','HSE-GEMS','REQ','Raissa.Rosyadi@borneo-indobara.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(26,'novisepta','Novi Septa','GRB-GEMS','REQ','produksi@sinarmasmining.com','2ece9cd58727cfefffd3f2efd5b8a48b','GEMS','[\"GEMS\"]','Y'),
(27,'suryani','Suryani','OPER-BKES','REQ','suryani.suryani@bkes.co.id;','2ece9cd58727cfefffd3f2efd5b8a48b','BKES','[\"BKES\"]','Y'),
(28,'cicipuji','Cici Puji Lestari','OPER-BKES','REQ','adminsite.mal@bkes.co.id; ','2ece9cd58727cfefffd3f2efd5b8a48b','BKES','[\"BKES\"]','Y'),
(29,'apriliasaf','Aprilia Safitri','HRGA-BKES','REQ','mal.hrga@bkes.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BKES','[\"BKES\"]','Y'),
(30,'fericaveni','Ferica Veni Dita','CSR-BKES','REQ','ferica.dita@sinarmasmining.com','2ece9cd58727cfefffd3f2efd5b8a48b','BKES','[\"BKES\"]','Y'),
(31,'suhendriwi','Suhendri Wijaya','IT-BKES','REQ','itsupport.mal@bkes.co.id; ','2ece9cd58727cfefffd3f2efd5b8a48b','BKES','[\"BKES\"]','Y'),
(32,'gregorius','Gregorius Ade Setiawan','HRGA-BKES','REQ','ga-setiawan@bkes.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BKES','[\"BKES\"]','Y'),
(33,'anisanan','Anisa Nanhidayah','AUDIT-BCE','REQ','anisa.nanhidayah@beraucoalenergy.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(34,'admsyscom','ADM System Compliance','COMPL-BCE','REQ','adm.systemcompliance@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(35,'datacon','Data Controller Energy','EMS-BCE','REQ','datacontroller.energy@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(36,'tedyindra','Tedy Indra S','EMS-BCE','APP1','tedy.indra@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(37,'admenvrho','Adm Enviro HO','ENVR-BCE','REQ','adm.enviroho@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(38,'admenvred','Adm Enviro Edcr','ENVR-BCE','REQ','adm.enviroedcr@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(39,'interenvr','Internship Enviro','ENVR-BCE','REQ','internship.enviroho@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(40,'hifzilkirmi','Hifzil Kirmi','ENVR-BCE','APP1','hifzil.kirmi@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(41,'erssadm','ER & SS Admin','ERG-BCE','REQ','erss.admin@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(42,'andihenry','Andi Henry Achmad','ERG-BCE','APP1','andi.henry@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(43,'fimadm','FIM Admin','FIM-BCE','REQ','adm-utility@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(44,'arham','Arham','FPM-BCE','REQ','arham@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(45,'dwichandra','Dwi Chandra','FPM-BCE','REQ','dwi.chandra@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(46,'haekaladi','Haekal Adityo','FPM-BCE','REQ','haekal.adityo@beraucoalenergy.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(47,'restuyanuar','Restu Yanuar','FPM-BCE','REQ','restu.yanuar@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(48,'zulfakur','Zulfa Kurniawan','FPM-BCE','APP1','zulfa@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(49,'robertohuta','Roberto Hutagalung','FPM-BCE','REQ','roberto@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(50,'doniabdi','Doni Abdillah','FPM-BCE','REQ','doni.abdillah@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(51,'puguhsup','Puguh Suprianto','FPM-BCE','REQ','puguh.suprianto@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(52,'sainon','Sainon','GEO-BCE','REQ','sainon@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(53,'hijriansyah','Hijriansyah','GEO-BCE','REQ','hijriansyah@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(54,'nikolaus','Nikolaus Candra Lado','GS-BCE','REQ','nikolaus.candra@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(55,'gaadmserv','Ga Admin Service','GS-BCE','REQ','gaadmin.service@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(56,'nawarmelin','Nawar Melinda','GSJKT-BCE','REQ','nawar.melinda@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(57,'admgsmtl','Admin GS MTL','GSMTL-BCE','REQ','admin.gs@mtl.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(58,'martabaya','Marta Baya','GSMTL-BCE','REQ','marta.baya@mtl.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(59,'srikumala','Sri Kumalayanti','GSMTL-BCE','REQ','sri.kumalayanti@mtl.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(60,'dedikusnadi','Dedi Kusnadi','GSMTL-BCE','APP1','dedi.kusnadi@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(61,'nurulhik','Nurul Hikmah','HR-BCE','REQ','nurulhikmah@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(62,'achmadabel','Achmad Abel','HR-BCE','REQ','achmad.abel@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(63,'admhse','Admin HSE C&T','HSECT-BCE','REQ','admin.hsect@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(64,'haltontaslim','Halton N. Taslim','INV-BCE','CAT','halton.taslim@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(65,'novitaros','Novita Rosanti','IT-BCE','REQ','novita.rosanti@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(66,'adminit','Admin IT & MIS','IT-BCE','REQ','admin.itmis@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(67,'itasset','IT Asset (Muhammad Wiriyanata)','IT-BCE','REQ','It.asset@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(68,'yulinar','Yulinar Permatasari','IT-BCE','REQ','yulinar.permatasari@sinarmasmining.com','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(69,'septiper','Septi Permadi','IT-BCE','REQ','septi.permadi@sinarmasmining.com','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(70,'deddyira','Deddy Irawan','IT-BCE','REQ','deddy.irawan@sinarmasmining.com','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(71,'denny','Denny','IT-BCE','REQ','denny.denny@sinarmasmining.com','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(72,'ismailsas','Ismail Sastra','KB-BCE','REQ','ismail.sastra@beraucoalenergy.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(73,'adminkb','Admin KB','KB-BCE','REQ','admin.kb@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(74,'wrhho','Warehouse HO','LOG-BCE','REQ','warehouse.ho@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(75,'kaharman','Kaharman','LOG-BCE','REQ','kaharman@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(76,'lambokpanj','Lambok ER Panjaitan','LOG-BCE','APP2','lambok.panjaitan@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(77,'adityaris','Aditya Rismana','LOG-BCE','APP1','aditya.rismana@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(78,'agussetia','Agus Setia Budi','LOGMTL-BCE','REQ','agus.setiabudi@mtl.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(79,'lilikp','Lilik P','MTCRD-BCE','REQ','maintenance.road@mtl.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(80,'admmpc','Admin MPC Dept','MINECLO-BCE','REQ','adm-mpc@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(81,'minetechprm','Minetech PRM','MINETCH-BCE','REQ','minetech.prm@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(82,'herlina','Herlina','PLNTMTL-BCE','REQ','adm.plantmaintenance@mtl.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(83,'juliatipil','Juliati Pilipus','PLNTMTL-BCE','REQ','planadm.rental@mtl.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(84,'ratihrama','Ratih Ramadhaniati','PLNTMTL-BCE','REQ','admprod.rental@mtl.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(85,'igedeagus','I Gede Agus Suryadinata','PLNTMTL-BCE','APP1','igedeagus.suryadinata@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(86,'evisafika','Evi Safika Putri','PRJ-BCE','REQ','evi.safika@beraucoalenergy.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(87,'lukaspatan','Lukas Patandung','PRJ-BCE','REQ','lukas.patandung@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(88,'reoandre','Reo Andreas','PRJ-BCE','APP1','reo.andreas@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(89,'adminpr','Admin PR','PRJ-BCE','REQ','admin.pr@beraucoal.co.id','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y'),
(90,'admin','admin','PRJ-BCE','ADM','admin@sinarmasmining.com','2ece9cd58727cfefffd3f2efd5b8a48b','BC','[\"BC\"]','Y');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
